package br.com.assai.horus.integracao;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.assai.horus.iis.integration.IntegrationIIS;
import br.com.assai.horus.iis.integration.dto.TipoIntegracao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PedidoTVDIntegracao extends IntegrationIIS { 
	
	@Value("${horus.application.iis.validacaoURI.path}")
	private String validacaoURI;
	
	@Value("${horus.application.iis.integracaoURI.path}")
	private String integracaoURI;
	
	@Value("${horus.application.balancer.host}")
	private String hostBalancer;
	
	@Value("${horus.application.iis.integracao.emporium.URI.path}")
	private String integracaoEmpURI;
	
	@PostConstruct
	public void postConstructor() {
		log.info("hostBalancer = {}", this.hostBalancer);
		log.info("validationURI = {}", this.validacaoURI);
		log.info("integracaoURI = {}", this.integracaoURI);
		log.info("integracaoEmpURI = {}", this.integracaoEmpURI);
	}
	
	    @Override
		protected String validacaoURI() {
			return hostBalancer.concat(validacaoURI);
		}

		@Override
		protected  String integracaoURI() {
			return hostBalancer.concat(integracaoURI);
		}

		@Override
		protected TipoIntegracao tipoIntegracao() {
			return TipoIntegracao.INTEGRACAO_PEDIDO_TVD_CAPA;
		}
		
		public  String integracaoEmpURI() {
			return integracaoEmpURI;
		}

}
