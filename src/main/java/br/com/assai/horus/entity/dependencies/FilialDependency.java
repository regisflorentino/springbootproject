package br.com.assai.horus.entity.dependencies;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.assai.horus.entity.ParamentroTipoVenda;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "CAM_FILIAL")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilialDependency implements Model {

    private static final long serialVersionUID = 1817437483799967291L;

    @Id
    @Column(name = "ID_FILIAL", insertable=false, updatable=false)
    private Long id;
    
    @Column(name = "NM_FILIAL", insertable=false, updatable=false)
    private String nome;
    
    @Column(name = "NM_UF", insertable=false, updatable=false)
    private String uf;
    
    @ManyToOne(optional=true,  fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_REGIONAL", insertable=false, updatable=false)
    private RegionalDependency regional;
    
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_FILIAL")
    private List<FilialUsuarioDependency> usuariosPermitidos;

    @OneToOne
    @JoinColumn(name="ID_FILIAL")
    private ParamentroTipoVenda parametroTipoVenda;
    
    public FilialDependency() {
        super();
    }
    
    public FilialDependency(Long id) {
        this.id = id; 
    }
    
}
