package br.com.assai.horus.entity.prepedido;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.NotAudited;

import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.rule.IdentifiableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Table(name = "TVD_PREPEDIDO_ITEM")
@EqualsAndHashCode(of = {"idPrepedido", "idProduto", "idEmbalagem", "idProdutoFilialPreco"})
//@SequenceGenerator(name = "TVD_PREPEDIDO_ITEM_SEQ", sequenceName = "TVD_PREPEDIDO_ITEM_SEQ", allocationSize = 1)
public class ItemPrepedido implements IdentifiableEntity {

    private static final long serialVersionUID = -2260005139040005917L;

	@Id
    @Column(name="ID_PREPEDIDO_ITEM")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TVD_PREPEDIDO_ITEM_SEQ")
    private Long id;
   
    @Column(name="ID_PREPEDIDO_CAPA")
    private Long idPrepedido;
    
    @Column(name="NO_ITEM") 
    private Integer numeroItem;
    
    @Column(name="ID_PRODUTO")
    private Long idProduto;
    
    @Column(name="ID_EMBALAGEM")
    private Long idEmbalagem;
    
    @Column(name="ID_PRODUTO_FILIAL_PRECO")
    private Long idProdutoFilialPreco;
    
    @Column(name="VL_PRECO_VIGENTE")
    private BigDecimal valorPrecoVigente;
    
    @Column(name="QT_NEGOCIADA") 
    private BigDecimal qtdNegociada;
    
    @Column(name="VL_PRECO_NEGOCIADO") 
    private BigDecimal precoNegociadoUnit;

    @Column(name="VL_PRECO_NEGOCIADO_EMBL") 
    private BigDecimal precoNegociadoEmb;

    @Column(name="VL_IMPOSTO") 
    private BigDecimal valorImposto;
    
    @Column(name="VL_DESPESA") 
    private BigDecimal valorDespesa;
    
    @Column(name="VL_VERBA") 
    private BigDecimal valorVerba;
    
    @Column(name="PC_MARGEM_NEGOCIADA")
    private BigDecimal margem;

    @Column(name="VL_TOTAL_PRODUTO")
    private BigDecimal valorTotalProduto;

    @Column(name="VL_DESCONTO") 
    private BigDecimal valorDesconto;
    
    @Column(name="FG_NEGOCIACAO_PONTUAL")
    private Boolean negociacaoPontual;

    @Column(name="ST_STATUS_SEPARACAO")
    private String statusSeparacao;
    
    @Column(name="QT_SEPARACAO") 
    private BigDecimal qtdSeparacao;
    
    @Column(name="QT_RETIRADA") 
    private BigDecimal qtdRetirada;

    @Column(name="QT_DEVOLVIDA") 
    private BigDecimal qtdDevolvida;

    
}
