package br.com.assai.horus.entity.dependencies;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.NotAudited;

import br.com.assai.horus.entity.Usuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "CAM_CLIENTE")
@Data
@EqualsAndHashCode(of = {"id"})
public class ClienteDependency {
	
	@Id
    @Column(name="ID_CLIENTE")
    private Long id;
    
    @Column(name="FG_ATIVO")
    private String ativo;
    
    @OneToOne()
    @JoinColumn(name="ID_PESSOA")
    private PessoaDependency pessoa;
    
    @NotAudited
    @Column(name="DT_CADASTRO", updatable=true, insertable=true)
    private Date dataCadastro;
    
    @OneToOne
    @JoinColumn(name="ID_FORMA_PAGAMENTO", updatable=true, insertable=true)
    private FormaPagamentoDependency formaPagamento;

    @OneToOne
    @JoinColumn(name="ID_PRAZO_MAX_PAGAMENTO", updatable=true, insertable=true)
    private PrazoMaximoPagamentoDependency prazoMaximoPagamento;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=true, insertable=true)
    private Usuario usuarioCadastro;
    
    @NotAudited
    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @NotAudited
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;

}
