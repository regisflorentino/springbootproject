package br.com.assai.horus.entity.dependencies;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "ADM_USUARIO_FILIAL")
public class FilialUsuarioDependency {
    
    @Id
    @Column(name="ID_USUARIO_FILIAL", insertable=false, updatable=false)
    private Long id;
    
    @Column(name="ID_FILIAL", insertable=false, updatable=false)
    private Long idFilial;

    @Column(name="ID_USUARIO", insertable=false, updatable=false)
    private Long idUsuario;

}
