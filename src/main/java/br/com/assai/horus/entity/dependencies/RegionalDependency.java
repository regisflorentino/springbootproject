package br.com.assai.horus.entity.dependencies;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@ToString(exclude = {"filiais"})
@Table(name="REGIONAL")
@JsonIgnoreProperties(value = {"filiais"},ignoreUnknown = true)
public class RegionalDependency implements Model {

    private static final long serialVersionUID = 443963737694472269L;

    @Id
    @Column(name = "ID_REGIONAL", insertable=false, updatable=false)
    private Long id;

    @Column(name = "NOME", insertable=false, updatable=false)
    private String nome;    
    
    @Column(name = "COD", insertable=false, updatable=false)
    private Long codigo;

    @Column(name = "ORDEM", insertable=false, updatable=false)
    private Long ordem;
    
    
    @OneToMany(mappedBy = "regional", fetch=FetchType.LAZY)
    private List<FilialDependency> filiais;
    
}
