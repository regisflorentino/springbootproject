package br.com.assai.horus.entity.pedido;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

import br.com.assai.horus.datamodel.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;



@Entity
@Table(name = "TVD_PEDIDO_NOTA")
@Data
@EqualsAndHashCode(of = {"id"})
@SequenceGenerator(name = "TVD_PEDIDO_NOTA_SEQ", sequenceName = "TVD_PEDIDO_NOTA_SEQ", allocationSize = 1)
public class PedidoNota implements Model, Persistable<Long>  {
	
	
	private static final long serialVersionUID = -713812405269032307L;

	@Id
    @Column(name="ID_PEDIDO_NOTA")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TVD_PEDIDO_NOTA_SEQ")
	private Long id ;
	
	@Column(name="ID_PRODUTO")
	private Long idProduto;
	
	@Column(name="NO_NOTA")
	private Long nroNota;
	
	@Column(name="NO_SERIE_NOTA")
	private Long nroSerieNota;
	
	@Column(name="ID_PEDIDO_CAPA")
	private Long idPedidoCapa;
	
	@Column(name="TP_NOTA")
	private String tpNota;
	
	@Column(name="CH_ACESSO_NOTA")
	private String chAcessoNota;
	
	@Column(name="VL_NFE")
	private Long vlNfe;
	
	@Column(name="NO_BOLETO")
	private Long nroBoleto;
	
	@Column(name="VL_BOLETO")
	private Long vlBoleto;
	
	@Column(name="NO_BANCO")
	private Long noBanco;
	
	@Column(name="NO_AGENCIA")
	private String  noAgencia;
	
	@Column(name="NO_CONTA_CORRENTE")
	private String noContaCorrente;
	
	@Column(name="NO_CHEQUE")
	private String noCheque;
	
	@Column(name="VL_CHEQUE")
	private Long vlCheque;
	
	@Column(name="QT_PRODUTO_NOTA")
	private Integer qtdProdutoNota ;
	
	@Column(name="VL_UNIT_PROD_NOTA")
	private Long vlUnitProdutoNota ;
	
	@Column(name="VL_TOTAL_PROD_NOTA")
	private Long  vlTotalProdNota;
	
	@Column(name="ID_USUARIO_CADASTRO")
	private Long idUsuarioCadastro;
	
	@Column(name="DT_CADASTRO")
	private Date dtCadastro;
	
	@Column(name="ID_TRANSACAO")
	private Long idTransacao;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}

}
