package br.com.assai.horus.entity.dependencies;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.NotAudited;

import br.com.assai.horus.entity.Usuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "CAM_PESSOA_ENDERECO")
@Data
@EqualsAndHashCode(of = {"id"})
public class PessoaEnderecoDependency {
	
	@Id
    @Column(name="ID_ENDERECO")
    private Long id;
    
    @ManyToOne(optional=true,  fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_PESSOA", insertable=false, updatable=false)   
    private PessoaDependency pessoa;
    
    @Column(name="TP_ENDERECO")
    private String tipoEndereco;
    
    @Column(name="DS_LOGRADOURO")
    private String logradouro;
    
    @Column(name="NO_ENDERECO")
    private String numeroEndereco;
    
    @Column(name="DS_COMPLEMENTO")
    private String descricaoComplemento;
    
    @Column(name="NM_BAIRRO")
    private String nomeBairro;
    
    @Column(name="NM_CIDADE")
    private String nomeCidade;
    
    @Column(name="NM_PAIS")
    private String nomePais;
    
    @Column(name="SG_UF")
    private String siglaUF;
    
    @Column(name="CD_IBGE")
    private String codigoIbge;
    
    @Column(name="NO_CEP")
    private String numeroCep;
    
    @Column(name="NO_INSCRICAOMUNICIPAL")
    private String numeroInscricaoMunicipal;
    
    @NotAudited
    @Column(name="DT_CADASTRO", updatable=false, insertable=true)
    private Date dataCadastro;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=false, insertable=true)
    private Usuario usuarioCadastro;
    
    @NotAudited
    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @NotAudited
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;

}
