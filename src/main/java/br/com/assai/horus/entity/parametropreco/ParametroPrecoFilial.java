package br.com.assai.horus.entity.parametropreco;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

import org.springframework.data.domain.Persistable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@JsonIgnoreProperties(ignoreUnknown=true)
@Entity
@Table(name = "TVD_PARAM_PRECO_FILIAL")
@EqualsAndHashCode(of = {"filial"})
@SequenceGenerator(name = "PARAM_PRECO_FILIAL_SEQ", sequenceName = "PARAM_PRECO_FILIAL_SEQ", allocationSize = 1)
public class ParametroPrecoFilial implements Model, Persistable<Long>, PercentualParametroPreco {

    private static final long serialVersionUID = 7590255499782244709L;
                    
    @Id
    @Column(name="ID_PARAM_PRECO_FILIAL")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAM_PRECO_FILIAL_SEQ")
    private Long id;
        
    @Column(name="ID_PARAM_PRECO")
    private Long idParametroPreco;

    @Column(name="ID_FILIAL")
    private Long idFilial;

    @Column(name="PC_REDUCAO")
    @DecimalMin(value="0", message="O percentual de redução na filial não pode ser inferior a zero")
    @DecimalMax(value="99.99", message="O percentual de redução na filial não pode ser superior a 99.99%")
    private BigDecimal precoReducao;

    @Column(name="FG_SEM_REDUCAO")
    private Boolean semReducao;
    
    @Column(name="PC_ACRESCIMO")
    @DecimalMin(value="0", message="O percentual de acréscimo na filial não pode ser inferior a zero")
    private BigDecimal precoAcrescimo;

    @Column(name="FG_SEM_ACRESCIMO")
    private Boolean semAcrescimo;
    
    public ParametroPrecoFilial() {
        super();
    }
    
    public ParametroPrecoFilial(Long id) {
        this.id = id;
    }
    
    @Override
    public boolean isNew() {
        return id == null;
    }
}
