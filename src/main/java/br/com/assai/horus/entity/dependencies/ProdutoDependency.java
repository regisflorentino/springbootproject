package br.com.assai.horus.entity.dependencies;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CAM_PRODUTO")
@EqualsAndHashCode()
@ToString(of = { "codigo", "descricaoCompleta", "descricaoReduzida"})
@JsonIgnoreProperties(value = {"estruturaMercadologica"}, ignoreUnknown = true)
public class ProdutoDependency implements Model {
    
    private static final long serialVersionUID = 3846648460529380207L;

    public static final String FINALIDADE_REVENDA = "R";
    public static final Long[] INVALID_COMPRADOR_CATEGORIA_ID = {1L, 11L, 15L};
    
    @Id
    @Column(name = "ID_PRODUTO")
    private Long id;

    @Column(name = "CD_PRODUTO")
    private Long codigo;

    @Column(name = "DS_COMPLETA")
    private String descricaoCompleta;

    @Column(name = "DS_REDUZIDA")
    private String descricaoReduzida;
    
    @Column(name = "DS_BASICA")
    private String descricaoBasica;

    @OneToOne
    @JoinColumn(name = "ID_NIVEL")
    private EstruturaMercadologicaDependency estruturaMercadologica;
    
    @Column(name = "FG_ATIVO")
    private boolean ativo;
    
    @Column(name = "CD_FINALIDADE")
    private String finalidade;   

    @Column(name = "ID_COMP_CATEGORIA")
    private Long compradorCategoriaId;


}