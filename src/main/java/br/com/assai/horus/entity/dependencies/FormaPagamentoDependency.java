package br.com.assai.horus.entity.dependencies;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "CAM_FORMA_PAGAMENTO")
@Data
@EqualsAndHashCode(of = {"id"})
public class FormaPagamentoDependency {

    @Id
    @Column(name="ID_FORMA_PAGAMENTO")
    private Long id;

    @Column(name="NM_FORMA_PAGAMENTO")
    private String nome;
    
}
