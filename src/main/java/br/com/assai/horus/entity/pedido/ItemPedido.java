package br.com.assai.horus.entity.pedido;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

import br.com.assai.horus.contextdomain.ContextDomainType;
import br.com.assai.horus.contextdomain.IContextDomainChild;
import br.com.assai.horus.contextdomain.entity.TransacaoContextDomainChildComponent;
import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.entity.prepedido.ItemPrepedido;
import br.com.assai.horus.rule.IdentifiableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@Entity
@Table(name = "TVD_PEDIDO_ITEM")
@EqualsAndHashCode(of = {"id"})
@SequenceGenerator(name = "TVD_PEDIDO_ITEM_SEQ", sequenceName = "TVD_PEDIDO_ITEM_SEQ", allocationSize = 1)
public class ItemPedido implements Model, Persistable<Long>, IContextDomainChild, IdentifiableEntity {

	private static final long serialVersionUID = -2239686020770493297L;

	@Id
    @Column(name="ID_PEDIDO_ITEM")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TVD_PEDIDO_ITEM_SEQ")
    private Long id;
	
    @Column(name = "ID_PEDIDO_CAPA")
    private Long idPedido;
    
    @Column(name = "NO_ITEM", insertable=true, updatable=false)
    private Integer numeroItem;

    @Column(name = "ID_PRODUTO")
    private Long idProduto;
    
    @Column(name="ID_EMBALAGEM")
    private Long idEmbalagem;    
    
    @Column(name="ID_PRODUTO_FILIAL_PRECO")
    private Long idProdutoFilialPreco;

    @Column(name="VL_PRECO_VIGENTE")
    private BigDecimal valorPrecoVigente;

    @Column(name="QT_NEGOCIADA")
    private BigDecimal qtdNegociada;
    
    @Column(name="VL_PRECO_NEGOCIADO")
    private BigDecimal precoNegociadoUnit;

    @Column(name="VL_PRECO_NEGOCIADO_EMBL") 
    private BigDecimal precoNegociadoEmb;
    
    @Column(name="VL_IMPOSTO")
    private BigDecimal valorImposto;
    
    @Column(name="VL_DESPESA")
    private BigDecimal valorDespesa;
    
    @Column(name="VL_VERBA")
    private BigDecimal valorVerba;
    
    @Column(name="PC_MARGEM_NEGOCIADA")
    private BigDecimal margem;
    
    @Column(name="VL_TOTAL_PRODUTO")
    private BigDecimal valorTotalProduto;
    
    @Column(name="VL_DESCONTO")
    private BigDecimal valorDesconto;
    
    @Column(name="FG_NEGOCIACAO_PONTUAL")
    private Boolean negociacaoPontual;
    
    @Column(name="ST_STATUS_SEPARACAO")
    private String statusSeparacao;
    
    @Column(name="QT_SEPARACAO")
    private BigDecimal qtdSeparacao;
    
    @Column(name="QT_RETIRADA")
    private BigDecimal qtdRetirada;
    
    @Column(name="QT_DEVOLVIDA")
    private BigDecimal qtdDevolvida;
    
	@Embedded
	private TransacaoContextDomainChildComponent transacao;
    
    public ItemPedido() {
    	super();
    }

    public ItemPedido(Long idPedido, ItemPrepedido itemPrepedido) {
    	super();
    	this.setIdPedido(idPedido);
    	this.setIdEmbalagem         (itemPrepedido.getIdEmbalagem         ());
    	this.setIdProduto           (itemPrepedido.getIdProduto           ());
    	this.setIdProdutoFilialPreco(itemPrepedido.getIdProdutoFilialPreco());
    	this.setNumeroItem          (itemPrepedido.getNumeroItem          ());
    	this.setMargem              (itemPrepedido.getMargem              ());
    	this.setNegociacaoPontual   (itemPrepedido.getNegociacaoPontual   ());
    	this.setPrecoNegociadoEmb   (itemPrepedido.getPrecoNegociadoEmb   ());
    	this.setPrecoNegociadoUnit  (itemPrepedido.getPrecoNegociadoUnit  ());
    	this.setQtdDevolvida        (itemPrepedido.getQtdDevolvida        ());
    	this.setQtdNegociada        (itemPrepedido.getQtdNegociada        ());
    	this.setQtdRetirada         (itemPrepedido.getQtdRetirada         ());
    	this.setQtdSeparacao        (itemPrepedido.getQtdSeparacao        ());
    	this.setStatusSeparacao     (itemPrepedido.getStatusSeparacao     ());
    	this.setValorDesconto       (itemPrepedido.getValorDesconto       ());
    	this.setValorDespesa        (itemPrepedido.getValorDespesa        ());
    	this.setValorImposto        (itemPrepedido.getValorImposto        ());
    	this.setValorPrecoVigente   (itemPrepedido.getValorPrecoVigente   ());
    	this.setValorTotalProduto   (itemPrepedido.getValorTotalProduto   ());
    	this.setValorVerba          (itemPrepedido.getValorVerba          ());
    }

    
    @Override
    public boolean isNew() {
        return id == null;
    }

	@Override
	public ContextDomainType getContextDomain() {
		return ContextDomainType.TVD_PEDIDO_ITEM;
	}

	@Override
	public void setTransacao(TransacaoContextDomainChildComponent transacao) {
		this.transacao = transacao;
		
	}

	@Override
	public TransacaoContextDomainChildComponent getTransacao() {
		return transacao;
	}

	/**
	 * Retorna uma implementação de IdentifiableEntity 
	 */
	@Override
	public IdentifiableEntity getParentId() {
		return new PedidoIdentifiableEntityImpl(idPedido);
	}
	
}
