package br.com.assai.horus.entity.dependencies;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.NotAudited;

import br.com.assai.horus.entity.Usuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "CAM_PESSOA")
@Data
@EqualsAndHashCode(of = {"id"})
public class PessoaDependency {
	
    @Id
    @Column(name="ID_PESSOA")
    private Long id;

    /** Fisica -> Nome - Juridica -> Razao Social */
    @Column(name="DESCR")
    private String descricao;

    /** CPF/CNPJ */
    @Column(name="NO_CNPJ_CPF")
    private String cpfCnpj;
    
    @Column(name="NM_RAZAO_SOCIAL")
    private String nomeRazaoSocial;
    
    @Column(name="NM_FANTASIA")
    private String nomeFantasia;
    
    @Column(name="TP_SEXO")
    private String sexo;

    @Column(name="DT_FUNDACAO_NASCIMENTO", updatable=false, insertable=true)
    @Temporal(TemporalType.DATE)
    private Date fundacaoNascimento;
    
    @Column(name="NO_INSCRICAO_ESTADUAL")
    private String numeroInscricaoEstadual;
    
    @Column(name="NO_RG")
    private String numeroRG;
    
    @Column(name="SG_ORGAO_EXPEDIDOR")
    private String orgaoExpedidor;
    
    @Column(name="ID_UF_ORGAO_EXPEDIDOR")
    private String ufOrgaoExpedidor;
    
    @Column(name="TP_SITUACAO_TRIBUTARIA")
    private String situacaoTributaria;
    
    @Column(name="ID_CNAE")
    private String cnae;
    
    @Column(name="NM_EMAIL_NFE")
    private String emailNfe;
    
    @Column(name="FG_HABILITADO_SINTEGRA")
    private Boolean habilitadoSintegra;
    
    @Column(name="FG_ORGAO_PUBLICO")
    private Boolean orgaoPublico;
    
    @Column(name="FG_PRODUTOR_RURAL")
    private Boolean produtorRural;
    
    @Column(name="FG_CONTRIBUINTE_ICMS")
    private Boolean contribuinteIcms;
    
    @Column(name="FG_SUSPENSO_PIS_COFINS")
    private Boolean suspensoPisCofins;
    
    @Column(name="FG_MICRO_EMPRESA")
    private Boolean microEmpresa;
    
    @Column(name="FG_SUBSTITUTO_TRIBUTARIO")
    private Boolean substitutoTributario;
    
    @Column(name="FG_CONTRIBUINTE_IPI")
    private Boolean contribuinteIpi;
    
    @Column(name="NO_SUFRAMA")
    private String numeroSuframa;
    
    @Column(name="ID_ATIVIDADE")
    private String atividade;
    
    @Column(name="FG_CLIENTE")
    private Boolean cliente;
    
    @Column(name="FG_FORNECEDOR")
    private Boolean fornecedor;
    
    @Column(name="FG_TRANSPORTADORA")
    private Boolean transportadora;
    
    @Column(name="FG_MOTORISTA")
    private Boolean motorista;   
    
    @OneToMany(mappedBy = "pessoa", fetch=FetchType.LAZY)
    private List<PessoaEnderecoDependency> enderecos;
    
    @NotAudited
    @Column(name="DT_CADASTRO", updatable=false, insertable=true)
    private Date dataCadastro;
    
    @NotAudited
    @OneToOne
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=false, insertable=true)
    private Usuario usuarioCadastro;
    
    @NotAudited
    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;
    
    @NotAudited
    @OneToOne
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @NotAudited
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;

}
