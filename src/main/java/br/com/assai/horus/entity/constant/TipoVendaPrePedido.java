package br.com.assai.horus.entity.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TipoVendaPrePedido {

    INTERNA(1L),
    INTERESTADUAL(2L),
    EXPORTACAO(3L);
	
	@Getter
	private Long id;
	
	public static TipoVendaPrePedido findById(Long id) {
		for(TipoVendaPrePedido index : TipoVendaPrePedido.values()) {
			if(index.getId().equals(id)) {
				return index;
			}
		}
		return null;
	}
	
}
