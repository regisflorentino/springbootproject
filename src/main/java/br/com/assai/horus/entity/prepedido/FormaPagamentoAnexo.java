package br.com.assai.horus.entity.prepedido;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.rule.IdentifiableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "TVD_FORMAPAGAMENTO_ANEXO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@ToString(exclude={"formaPagamento"})
@SequenceGenerator(name = "TVD_ANEXO_SEQ", sequenceName = "TVD_ANEXO_SEQ", allocationSize = 1)
public class FormaPagamentoAnexo implements Model, IdentifiableEntity{
	
	private static final long serialVersionUID = -2809750328828887121L;

    @Id
    @Column(name="ID_ANEXO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TVD_ANEXO_SEQ")
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_FORMA_PAGAMENTO")
    @JsonManagedReference
	private FormaPagamento formaPagamento;
	
    @Column(name="NM_ANEXO")
    private String nomeAnexo;
    
    @Lob
    @Column(name="ANEXO")
    private byte[] arquivo;
    
    @NotAudited
    @Column(name="DT_CADASTRO", updatable=false, insertable=true)
    private Date dataCadastro;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=false, insertable=true)
    private Usuario usuarioCadastro;
    
    @NotAudited
    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;
    
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @NotAudited
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;    

}
