package br.com.assai.horus.entity.prepedido;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.rule.IdentifiableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TVD_FORMA_PAGAMENTO")
@Data
@Builder
@EqualsAndHashCode(of = {"id"})
@NoArgsConstructor
@AllArgsConstructor
public class FormaPagamento implements Model, IdentifiableEntity {
	
	private static final long serialVersionUID = -4741139707310201033L;

    @Id
    @Column(name="ID_FORMA_PAGAMENTO")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FORMAPAGAMENTO_SEQ")
    private Long id;
	
    @Column(name="NM_FORMA_PAGAMENTO")
    private String nome;	

    @Column(name="CD_FORMA_PAGAMENTO")
    private Long codigo;
    
    @Column(name = "ID_PREPEDIDO_CAPA")
    private Long idPrepedido;
    
    @Column(name = "ID_PEDIDO_CAPA")
    private Long idPedido;
    
    @Column(name = "NM_PRAZO")
    private Integer prazo;
    
    @Column(name = "NM_DEPOSITO")
    private String numeroDeposito;

}
