package br.com.assai.horus.entity.dependencies;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "CAM_PRAZO_PAGAMENTO")
@Data
@EqualsAndHashCode(of = {"id"})
public class PrazoMaximoPagamentoDependency {

    @Id
    @Column(name="ID_PRAZO_PAGAMENTO")
    private Long id;

    @Column(name="NO_DIAS_PAGAMENTO")
    private Integer prazoMaximos;

    
}
