package br.com.assai.horus.entity.prepedido;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.NotAudited;

import br.com.assai.horus.contextdomain.annotation.ContextDomainIgnoreField;
import br.com.assai.horus.contextdomain.entity.TransacaoContextDomainParentComponent;
import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.engineworkflow.annotation.TableDependency;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.constant.FormaRetirada;
import br.com.assai.horus.entity.constant.TipoFaturamento;
import br.com.assai.horus.entity.constant.TipoOrigemEnum;
import br.com.assai.horus.entity.constant.TipoVendaPrePedido;
import br.com.assai.horus.entity.engineworkflow.WorkflowEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TVD_PREPEDIDO_CAPA")
public class Prepedido extends WorkflowEntity implements Model {
	
	private static final long serialVersionUID = 7282003432613885261L;
	
    @Id
    @Column(name="ID_PREPEDIDO_CAPA")
    private Long id;

    @Column(name="ID_FILIAL")
    private Long idFilial;
    
    @Column(name = "ID_OPERACAO")
    private Long idOperacao;
    
    @Enumerated(EnumType.STRING)
    @Column(name="TP_FATURAMENTO")
    private TipoFaturamento tipoFaturamento;

    @Enumerated(EnumType.STRING)
    @Column(name = "NM_TIPO_VENDA")
    private TipoVendaPrePedido tipoVenda;
   
    @Column(name="ID_STATUS")
    private Long idStatus;

    @Column(name="ID_CLIENTE")
    private Long idCliente;
    
    @Column(name="FG_CLIENTE_IDENTIFICADO")
    private boolean clienteNaoIdentificado;
    
    @Column(name="NO_CPF_CNPJ_CUPOM")
    private String numeroCpfCnpjCupom;
    
    @Column(name="ID_ENDERECO")
    private Long idEndereco;
    
    @Column(name="FG_SEPARACAO")
    private int separacao;
    
    @ContextDomainIgnoreField
    @TableDependency(isList=true)
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_PREPEDIDO_CAPA")
    private List<FormaPagamento> formasPagamento;

    @Column(name="NO_PRAZO_PAGAMENTO")
    private Long numeroPrazoPagamento;
    
    @Enumerated(EnumType.STRING)
    @Column(name="TP_FORMA_RETIRADA")
    private FormaRetirada formaRetirada;
    
    @Enumerated(EnumType.STRING)
    @Column(name="TP_ORIGEM")
    private TipoOrigemEnum tipoOrigem;
    
    @Column(name="DT_PREVISTA_RETIRADA")
    private Date dataPrevistaRetirada;
    
    @Column(name="NO_RGCPF_RETIRADA")
    private String numeroRgCpfRetirada;

    @Column(name="ID_TRANSPORTADORA")
    private Long idTransportadora;
    
    @Column(name="NM_MOTORISTA")
    private String nomeMotorista;

    @Column(name="DS_TELEFONE_MOTORISTA")
    private String telefoneMotorista;
    
    @Column(name="NM_VEICULO")
    private String nomeVeiculo;
    
    @Column(name="NM_PLACA")
    private String placa;
    
    @Column(name="DT_GERACAO_PEDIDO")
    private Date dataGeracaoPedido;
   
    @Column(name="QT_TOTAL_ITENS")
    private Long qtdTotalItens;
    
    @Column(name="VL_TOTAL_DESCONTO")
    private BigDecimal valorTotalDesconto;
    
    @Column(name="PC_TOTAL_MARGEM")
    private BigDecimal percentutalTotalMargem;
    
    @Column(name="VL_TOTAL_PREPEDIDO")
    private BigDecimal valorTotalPrepedido;
    
    @Column(name="DS_OBSERVACAO_PEDIDO")
    private String observacaoPedido;
    
    @Column(name="DS_OBSERVACAO_NOTA")
    private String observacaoNota;
    
    @Column(name="ID_PEDIDO")
    private Long idPedido;
    
    @Column(name="ID_PEDIDO_CLIENTE")
    private String idPedidoCliente;
    
    @NotAudited
    @Column(name="DT_EXCLUSAO")
    private Date dataExclusao;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_EXCLUSAO")
    private Usuario usuarioExclusao;
    
    @Column(name="ID_VENDEDOR")
    private Long idVendedor;
    
    @Version
    @NotAudited
    @Column(name="VERSAO")
    private Long versao;
    
    @Embedded
    private TransacaoContextDomainParentComponent transacao;

    
    public void clearFormasPagamento() {
    	createFormasPagamento();
    	this.formasPagamento.clear();
    }

	private void createFormasPagamento() {
		if (this.formasPagamento == null) {
    		this.formasPagamento = new ArrayList<>();
    	}
	}
	
    
    public void addFormaPagamento(FormaPagamento formaPagamento) {
    	createFormasPagamento();
    	if (formaPagamento != null) {
    		this.formasPagamento.add(formaPagamento);
    	}
    }

    
//    public void clearItens() {
//    	createItens();
//    	this.itens.clear();
//    }
//
//	private void createItens() {
//		if (this.itens == null) {
//    		this.itens = new ArrayList<>();
//    	}
//	}
	
    
//    public void addItem(ItemPrepedido item) {
//    	createItens();
//    	if (item != null) {
//    		this.itens.add(item);
//    	}
//    }
    
    
    
    
}