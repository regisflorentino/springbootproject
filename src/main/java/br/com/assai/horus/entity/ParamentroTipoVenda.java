package br.com.assai.horus.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.NotAudited;
import org.springframework.data.domain.Persistable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.Data;

@Data
@Entity
@Table(name = "TVD_PARAM_TIPO_VENDA")
//@EqualsAndHashCode(of = {"uf", "idFilial"})
@SequenceGenerator(name = "TVD_PARAM_TIPO_VENDA_SEQ", sequenceName = "TVD_PARAM_TIPO_VENDA_SEQ", allocationSize = 1)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParamentroTipoVenda implements Model, Persistable<Long> {

    private static final long serialVersionUID = -5277448897595582060L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TVD_PARAM_TIPO_VENDA_SEQ")
    @Column(name="ID_PARAM_TIPO_VENDA")
    private Long id;
    
    @Column(name="ID_FILIAL")
    private Long idFilial;
        
    @Column(name="FG_VND_INTERNA")
    private Boolean vendaInterna;
    
    @Column(name="FG_VND_INTERNA_PDV")
    private Boolean vendaInternaPdv;
    
    @Column(name="FG_VND_INTERESTADUAL")
    private Boolean vendaInterstadual;
    
    @Column(name="FG_VND_INTEREST_PDV")
    private Boolean vendaInterstadualPdv;
    
    @Column(name="FG_VND_EXPORTACAO")
    private Boolean vendaExportacao;
    
    @Column(name="FG_EXPORT_PDV")
    private Boolean vendaExportacaoPdv;
    
    @NotAudited
    @Column(name="DT_CADASTRO", updatable=false, insertable=true)
    private Date dataCadastro;

    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=false, insertable=true)
    private Usuario usuarioCadastro;
    
    @NotAudited
    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @NotAudited
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;
    
    @NotAudited
    @Column(name="VERSAO")
    @Version
    private Long versao;

    @Override
    public boolean isNew() {
        return (id == null);
    }

}
