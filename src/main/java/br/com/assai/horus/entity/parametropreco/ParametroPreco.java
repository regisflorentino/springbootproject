package br.com.assai.horus.entity.parametropreco;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.domain.Persistable;

import br.com.assai.horus.constraints.Constraint;
import br.com.assai.horus.constraints.Validators;
import br.com.assai.horus.contextdomain.ContextDomainType;
import br.com.assai.horus.contextdomain.entity.TransacaoContextDomainParentComponent;
import br.com.assai.horus.datamodel.EntityAuditable;
import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.dependencies.RegionalDependency;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Entity
@Table(name = "TVD_PARAM_PRECO")
@EqualsAndHashCode(of = {"nomeGrupo"})
@JsonIgnoreProperties(ignoreUnknown=true)
@Data
@SequenceGenerator(name = "PARAM_PRECO_SEQ", sequenceName = "PARAM_PRECO_SEQ", allocationSize = 1)
public class ParametroPreco implements Model, Persistable<Long>, EntityAuditable<Long> {

    private static final long serialVersionUID = 3838530388788445441L;

    @Id
    @Column(name="ID_PARAM_PRECO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAM_PRECO_SEQ")
    private Long id;
    
    @Column(name="ID_REGIONAL")
    private Long idRegional;

    @NotNull(message="TLVM02.FORM.VALIDATE.GRUPO_NOT_NULL")
    @Size(min=1, max=20, message="TLVM02.FORM.VALIDATE.GRUPO_MAX_20_DIGITOS")
    @Column(name="NM_GRUPO")
    private String nomeGrupo;
    
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=false, insertable=true)
    private Usuario usuarioCadastro;

    @Column(name="DT_CADASTRO", updatable=false, insertable=true)
    private Date dataCadastro;
    
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;

    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;    
    
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;    

    // @Embedded
    // private TransacaoContextDomainParentComponent transacao;
    
    @Override
    public boolean isNew() {
        return id == null;
    }

}
