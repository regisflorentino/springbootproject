package br.com.assai.horus.entity.parametropreco;

import java.math.BigDecimal;

public interface PercentualParametroPreco {
    
    BigDecimal getPrecoReducao();
    void setPrecoReducao(BigDecimal precoReducao);
    Boolean getSemReducao();
    void setSemReducao(Boolean semReducao);
    BigDecimal getPrecoAcrescimo();
    void setPrecoAcrescimo(BigDecimal precoAcrescimo);
    Boolean getSemAcrescimo();
    void setSemAcrescimo(Boolean semAcrescimo);

}
