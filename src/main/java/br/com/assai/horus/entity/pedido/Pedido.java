package br.com.assai.horus.entity.pedido;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.envers.NotAudited;

import com.ibm.icu.util.Calendar;

import br.com.assai.horus.context.domain.annotation.ContextDomainIgnoreField;
import br.com.assai.horus.context.domain.entity.TransacaoContextDomainParentComponent;
import br.com.assai.horus.context.domain.rule.IContextDomainParent;
import br.com.assai.horus.context.domain.rule.IdentifiableEntity;
import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.constant.FormaRetirada;
import br.com.assai.horus.entity.constant.TipoFaturamento;
import br.com.assai.horus.entity.constant.TipoVendaPrePedido;
import br.com.assai.horus.entity.prepedido.Prepedido;
import br.com.assai.horus.entity.prepedido.StatusTelevendas;
import lombok.Data;


@Data
@Entity
@Table(name = "TVD_PEDIDO_CAPA")
public class Pedido implements Model, IdentifiableEntity<Long>, IContextDomainParent  {

	private static final long serialVersionUID = -221894226158039465L;
	
	@Id
    @Column(name="ID_PEDIDO_CAPA")
    private Long id;
	
    @Column(name = "ID_PREPEDIDO_CAPA")
    private Long idPrepedido;
    
    @Column(name = "ID_OPERACAO", insertable=true, updatable=false)
    private Long idOperacao;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "NM_TIPO_VENDA", insertable=true, updatable=false)
    private TipoVendaPrePedido tipoVenda;
    
    @Enumerated(EnumType.STRING)
    @Column(name="TP_FATURAMENTO")
    private TipoFaturamento tipoFaturamento;
    
    @Column(name="ID_CLIENTE", insertable=true, updatable=false)
    private Long idCliente;
   
    @Column(name="FG_CLIENTE_IDENTIFICADO")
    private Boolean clienteNaoIdentificado;
    
    @Column(name="NO_CPF_CNPJ_CUPOM")
    private String numeroCpfCnpjCupom;
    
    @Column(name="ID_ENDERECO", insertable=true, updatable=false)
    private Long idEndereco;

    @Column(name="ID_STATUS")
    private Long idStatus;
    
    @Column(name="NO_PRAZO_PAGAMENTO")
    private Long numeroPrazoPagamento;
    
    @Column(name="NO_PEDIDO_CLIENTE")
    private String idPedidoCliente;
    
    @Enumerated(EnumType.STRING)
    @Column(name="TP_FORMA_RETIRADA")
    private FormaRetirada formaRetirada;
    
    @Column(name="DT_PREVISTA_RETIRADA", updatable=false, insertable=true)
    private Date dataPrevistaRetirada;

    @Column(name="NO_RGCPF_RETIRADA")
    private String numeroRgCpfRetirada;  
     
    @Column(name="ID_TRANSPORTADORA")
    private Long idTransportadora;
    
    @Column(name="NM_MOTORISTA")
    private String nomeMotorista;
    
    @Column(name="DS_TELEFONE_MOTORISTA")
    private String telefoneMotorista;
    
    @Column(name="NM_VEICULO")
    private String nomeVeiculo;
    
    @Column(name="NM_PLACA")
    private String placa;
    
    @Column(name="DT_GERACAO_PEDIDO")
    private Date dataGeracaoPedido;
    
    @Column(name="ID_FILIAL")
    private Long idFilial;    

    @Column(name="QT_TOTAL_ITENS")
    private Long qtdTotalItens;
    
    @Column(name="VL_TOTAL_DESCONTO")
    private BigDecimal valorTotalDesconto;
    
    @Column(name="PC_TOTAL_MARGEM")
    private BigDecimal percentutalTotalMargem;
    
    @Column(name="VL_TOTAL_PEDIDO")
    private BigDecimal valorTotalPedido;
    
    @Column(name="DT_FATURAMENTO")
    private Date dataFaturamento;
    
    @Column(name="DS_OBSERVACAO_PEDIDO")
    private String observacaoPedido;
    
    @Column(name="DS_OBSERVACAO_NOTA")
    private String observacaoNota;
    
    @NotAudited
    @Column(name="DT_EXCLUSAO")
    private Date dataExclusao;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_EXCLUSAO")
    private Usuario usuarioExlusao;
      
    @Column(name="ID_VENDEDOR")
    private Long idVendedor;
    
    @Column(name="FG_SEPARACAO")
    private int separacao;
    
    @ContextDomainIgnoreField
    @OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name="ID_PEDIDO_CAPA")
    private List<ItemPedido> itens;
    
    @NotAudited
    @Column(name="VERSAO")
    @Version
    private Long versao;

    @Embedded
    private TransacaoContextDomainParentComponent transacao;
    
    public Pedido() {
    	super();
    }
  
    public Pedido(Long idPedido, Prepedido prepedido) {
    	super();
    	this.setId(idPedido);
    	this.setClienteNaoIdentificado(prepedido.isClienteNaoIdentificado());
    	this.setDataGeracaoPedido     (Calendar.getInstance().getTime());
    	this.setDataPrevistaRetirada  (prepedido.getDataPrevistaRetirada  ());
    	this.setFormaRetirada         (prepedido.getFormaRetirada         ()); 
    	this.setSeparacao                 (prepedido.getSeparacao());
    	this.setIdCliente             (prepedido.getIdCliente             ());
    	this.setIdEndereco            (prepedido.getIdEndereco            ());
    	this.setIdFilial              (prepedido.getIdFilial              ());
    	this.setIdOperacao            (prepedido.getIdOperacao            ());
    	this.setIdPrepedido           (prepedido.getId                    ());
    	this.setIdStatus              (StatusTelevendas.NAO_FATURADO);
    	this.setIdTransportadora      (prepedido.getIdTransportadora      ());
    	this.setIdVendedor            (prepedido.getIdVendedor            ());
    	this.setNomeMotorista         (prepedido.getNomeMotorista         ());
    	this.setNomeVeiculo           (prepedido.getNomeVeiculo           ());
    	this.setNumeroCpfCnpjCupom    (prepedido.getNumeroCpfCnpjCupom    ());
    	this.setNumeroPrazoPagamento  (prepedido.getNumeroPrazoPagamento  ());
    	this.setNumeroRgCpfRetirada   (prepedido.getNumeroRgCpfRetirada   ());
    	this.setObservacaoNota        (prepedido.getObservacaoNota        ());
    	this.setObservacaoPedido      (prepedido.getObservacaoPedido      ());
    	this.setPercentutalTotalMargem(prepedido.getPercentutalTotalMargem());
    	this.setPlaca                 (prepedido.getPlaca                 ());
    	this.setQtdTotalItens         (prepedido.getQtdTotalItens         ());
    	this.setTelefoneMotorista     (prepedido.getTelefoneMotorista     ());
    	this.setTipoFaturamento       (prepedido.getTipoFaturamento       ());
    	this.setTipoVenda             (prepedido.getTipoVenda             ());
    	this.setValorTotalDesconto    (prepedido.getValorTotalDesconto    ());
    	this.setValorTotalPedido      (prepedido.getValorTotalPrepedido   ());
    	this.setIdPedidoCliente(prepedido.getIdPedidoCliente());
    }

}
