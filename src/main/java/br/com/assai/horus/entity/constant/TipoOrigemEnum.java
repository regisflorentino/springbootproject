package br.com.assai.horus.entity.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TipoOrigemEnum {

	E("E"), 
	M("M");

	@Getter
	private String sigla;
	
	public static TipoOrigemEnum searchBySigla(String sigla) {
		for (TipoOrigemEnum tipo: TipoOrigemEnum.values()) {
			if(tipo.getSigla().equals(sigla)) {
				return tipo;
			}
		}
		return null;
	}
	
}
