package br.com.assai.horus.entity.parametropreco;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.data.domain.Persistable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
@Entity
@Table(name = "TVD_PARAM_PRECO_PRODUTO")
@SequenceGenerator(name = "PARAM_PRECO_PRODUTO_SEQ", sequenceName = "PARAM_PRECO_PRODUTO_SEQ", allocationSize = 1)
public class ParametroPrecoProduto implements Model, Persistable<Long>, PercentualParametroPreco {
    
    private static final long serialVersionUID = 5791717574373298129L;

    @Id
    @Column(name="ID_PARAM_PRECO_PRODUTO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAM_PRECO_PRODUTO_SEQ")
    private Long id;
    
    @Column(name="ID_PARAM_PRECO_FILIAL")
    private Long idParamPrecoFilial;    
    
    @Column(name="ID_NIVEL")
    private Long idNivel;

    @Column(name="ID_PRODUTO")
    private Long idProduto;

    @DecimalMin(value="0", message="O percentual de redução na estrutura mercadológica não pode ser inferior a zero")
    @DecimalMax(value="99.99", message="O percentual de redução na estrutura mercadológica não pode ser superior a 99.99%")
    @Column(name="PC_REDUCAO")
    private BigDecimal precoReducao;
    
    @Column(name="FG_SEM_REDUCAO")
    private Boolean semReducao;

    @DecimalMin(value="0", message="O percentual de acréscimo na estrutura mercadológica não pode ser inferior a zero")
    @Column(name="PC_ACRESCIMO")
    private BigDecimal precoAcrescimo;

    @Column(name="FG_SEM_ACRESCIMO")
    private Boolean semAcrescimo;

    @Override
    public boolean isNew() {
        return id == null;
    }
}
