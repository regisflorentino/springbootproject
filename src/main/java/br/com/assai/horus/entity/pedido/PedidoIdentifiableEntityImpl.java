package br.com.assai.horus.entity.pedido;

import javax.persistence.Column;

import br.com.assai.horus.rule.IdentifiableEntity;

/**
 * 
 * Implementação de IdentifiableEntity para retornar o id do pedido para o Context Domais,
 * sem esta classe o relacionamento entre a classe filha e a classe pai teria de ser bidirecional.
 * Devido a implementação do ContextDomain esta classe tem de possuir um atributo com o nome de id
 * e ser anotado com o @Column do JPA aponstando para o campo id da tabela. 
 * 
 * @author ps014841
 *
 */

public class PedidoIdentifiableEntityImpl implements IdentifiableEntity {

	private static final long serialVersionUID = -6161460741786619741L;
	
	@Column(name="ID_PEDIDO_CAPA")
	private Long id;
	
	public PedidoIdentifiableEntityImpl(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

}
