package br.com.assai.horus.entity.dependencies;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "CAM_VENDEDOR")
public class VendedorDependency implements Serializable {

    private static final long serialVersionUID = 1817437483799967291L;

    @Id
    @Column(name = "ID_VENDEDOR", insertable=false, updatable=false)
    private Long id;
    
    @Column(name = "ID_USUARIO", insertable=false, updatable=false)
    private Long usuario;
    
    public VendedorDependency() {
        super();
    }
    
    public VendedorDependency(Long id) {
        this.id = id; 
    }
    
}
