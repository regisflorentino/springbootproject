package br.com.assai.horus.entity.prepedido;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.NotAudited;

import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.entity.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TVD_STATUS")
@EqualsAndHashCode(of = {"id"})
public class StatusTelevendas implements Model {
	
	private static final long serialVersionUID = -8046223185572892451L;

	public static final Long EM_ANDAMENTO = 1L;
	public static final Long CANCELADO = 2L;
	public static final Long REPROVADO_PGTO = 7L;
	public static final Long REPROVADO_PRECO = 8L;
	public static final Long REPROVADO_PRAZO = 9L;
	public static final Long FINALIZADO = 11L; 
	public static final long NAO_FATURADO = 12;
	public static final long ANÁLISE_WORKFLOW = 20L;
	public static final long PROCESSAMENTO= 21L; 
	
	@Id
	@Column(name="ID_STATUS")
	private Long id;
	
	@Column(name="NM_STATUS")
	private String status;
	
    @NotAudited
    @Column(name="DT_CADASTRO", updatable=false, insertable=true)
    private Date dataCadastro;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=false, insertable=true)
    private Usuario usuarioCadastro;
    
    @NotAudited
    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @NotAudited
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;
    
    
    public static boolean permiteExclusaoPrepedido(Long id) {
    	List<Long> listaStatusPermitemExclucao = new ArrayList<>();
    	listaStatusPermitemExclucao.add(StatusTelevendas.EM_ANDAMENTO);
    	listaStatusPermitemExclucao.add(StatusTelevendas.REPROVADO_PGTO);
    	listaStatusPermitemExclucao.add(StatusTelevendas.REPROVADO_PRECO);
    	listaStatusPermitemExclucao.add(StatusTelevendas.REPROVADO_PRAZO);
    	listaStatusPermitemExclucao.add(StatusTelevendas.FINALIZADO);

    	return listaStatusPermitemExclucao.contains(id);
    }

}
