package br.com.assai.horus.entity.prepedido;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.NotAudited;

import br.com.assai.horus.datamodel.Model;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.rule.IdentifiableEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "TVD_LIMITE_CREDITO")
@Data
@EqualsAndHashCode(of = {"id"})
@SequenceGenerator(name = "TVD_LIMITE_CREDITO_SEQ", sequenceName = "TVD_LIMITE_CREDITO_SEQ", allocationSize = 1)
public class LimiteCreditoCliente implements Model, IdentifiableEntity {
    
    private static final long serialVersionUID = 3277219378059394610L;

    @Id
    @Column(name="ID_LIMITE_CREDITO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TVD_LIMITE_CREDITO_SEQ")
    private Long id;
   
    @Column(name="ID_PREPEDIDO_CAPA")
    private Long idPrepedido;
    
    @Column(name="ID_PESSOA")
    private Long idPessoa;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DT_CONSULTA_LIMITE")
    private Date dataConsultaLimite;
    
    @Column(name="ID_ASSOCIACAO_CLIENTE")
    private Long idAssociacao;
    
    @Column(name="DS_ASSOCIACAO")
    private String associacao;
    
    @Column(name="ST_SITUACAO")
    private String situacao;
    
    @Column(name="VL_LIMITE_CREDITO")
    private BigDecimal valorLimiteCredito;
    
    @Column(name="VL_TITULO_ABERTO")
    private BigDecimal valorTituloAberto;
    
    @Column(name="VL_FATURAMENTO_DIA")
    private BigDecimal valorFaturamentoDia;
    
    @Column(name="VL_DISPONIVEL") 
    private BigDecimal valorDisponivel;
    
    @Column(name="CD_PRAZO_MAXIMO") 
    private Long prazoMaximo;
    
    @Column(name="DT_CADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;
    
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO")
    private Usuario usuarioCadastro;
    
    @Column(name="DT_ALTERACAO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAlteracao;
    
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;

}
