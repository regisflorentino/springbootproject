package br.com.assai.horus.entity.pedido;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

import br.com.assai.horus.datamodel.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;



@Entity
@Table(name = "TVD_PEDIDO_CUPOM")
@Data
@EqualsAndHashCode(of = {"id"})
@SequenceGenerator(name = "TVD_PEDIDO_CUPOM_SEQ", sequenceName = "TVD_PEDIDO_CUPOM_SEQ", allocationSize = 1)
public class PedidoCupom implements Model, Persistable<Long>  {
	
	
	private static final long serialVersionUID = 7179319101336129835L;
	
	@Id
    @Column(name="ID_PEDIDO_CUPOM")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TVD_PEDIDO_CUPOM_SEQ")
	private Long id ;
	
	@Column(name = "ID_PEDIDO_CAPA", insertable=true, updatable=false)
	private Long idPedidoCapa;
	
	@Column(name="NM_FINALIZADORA")
	private String nmFinalizadora;
	
	@Column(name="VL_FINALIZADORA")
	private Long vlFinalizadora;
	
	@Column(name="COD_CUPOM")
	private String codCupom;
	
	@Column(name="NO_PDV")
	private Long nroPdv;
	
	@Column(name="OP_PDV")
	private String operacaoPdv;
	
	@Column(name="VL_CUPOM")
	private Double vlCupom;
	
	@Column(name="ID_USUARIO_ CADASTR0")
	private Long idUsuarioCadastro;
	

	@Column(name="DT_CADASTRO")
	private Date dtCadastro;

	@Column(name="ID_USUARIO_ALTERACAO")
	private Long idUsuarioAlteracao;
	
	
	@Column(name="DT_ALTERACAO")
	private Date dtAlteracao ;
	
	
	@Column(name="ID_TRANSACAO")
	private Long idTransacao;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}

}
