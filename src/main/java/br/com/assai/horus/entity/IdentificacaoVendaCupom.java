package br.com.assai.horus.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.NotAudited;
import org.springframework.data.domain.Persistable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Table(name = "tvd_identificacao_venda_cupom")
@SequenceGenerator(name = "identificacao_venda_cupom_SEQ", sequenceName = "identificacao_venda_cupom_SEQ", allocationSize = 1)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IdentificacaoVendaCupom implements Model, Persistable<Long> {

    private static final long serialVersionUID = -5277448897595582060L;

    @Id
    @Column(name="id_identf_vnd_cupom")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "identificacao_venda_cupom_SEQ")
    private Long id;
    
    @Column(name="id_filial")
    private Long idFilial;
    
    @Column(name="vl_max_vnd_cupom")
    @DecimalMin(value="0", inclusive=false, message="FISM09.FORM.LIST.VALIDACAO.VALOR_MAXIMO_NOT_ZERO.BASE")
    private BigDecimal valorMaximoVendaCupom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dt_ini_vig_valor")
    private Date dataIniVigenciaValor;
    
    @Column(name="fg_obrig_identif")
    private Boolean obrigatorioIdentificacao;

    @Column(name="VR_OBRIGATORIA_IDENT")
    private BigDecimal valorObrigatorioIdentificacao;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dt_ini_vig_identific")
    private Date dataIniVigenciaIdentificacao;
    
    @NotAudited
    @Column(name="DT_CADASTRO", updatable=false, insertable=true)
    private Date dataCadastro;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_CADASTRO", updatable=false, insertable=true)
    private Usuario usuarioCadastro;
    
    @NotAudited
    @Column(name="DT_ALTERACAO")
    private Date dataAlteracao;
    
    @NotAudited
    @OneToOne()
    @JoinColumn(name="ID_USUARIO_ALTERACAO")
    private Usuario usuarioAlteracao;
    
    @NotAudited
    @Column(name="ID_TRANSACAO")
    private Long idTransacao;
    
    @Override
    public boolean isNew() {
        return (id == null);
    }

}
