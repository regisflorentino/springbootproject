package br.com.assai.horus.entity.dependencies;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.datamodel.Model;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Builder
@Table(name = "CAM_ESTRUTURA_MERCADOLOGICA")
@ToString(exclude = {"nivelPai", "subniveis"})
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler", "subniveis", "produtos"}, ignoreUnknown = true)
public class EstruturaMercadologicaDependency implements Model {
    
    private static final long serialVersionUID = 8329077109137965908L;

    @Id
    @Column(name="ID_NIVEL")
    private Long id;
    
    @Column(name="CD_NIVEL")
    private String codigo;

    @Column(name="NM_NIVEL")
    private String nome;
    
    @Column(name="DS_NIVEL")
    private String descricao;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_NIVEL_PAI")
    private EstruturaMercadologicaDependency nivelPai;
    
    @OneToMany(mappedBy="nivelPai", fetch=FetchType.LAZY)
    private List<EstruturaMercadologicaDependency> subniveis;

    @Column(name="NO_ORDEM")
    private Integer ordem;
    
    @Column(name="FG_ATIVO")
    private Boolean ativo;

    @OneToMany(mappedBy="estruturaMercadologica", fetch= FetchType.LAZY) 
    private List<ProdutoDependency> produtos;
    
}