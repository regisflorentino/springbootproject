package br.com.assai.horus.entity.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum FormaRetirada {

	TRANSPORTADORA(1L), 
	VEICULO_PROPRIO(2L);

	@Getter
	private Long id;
	
	public static FormaRetirada searchById(Long id) {
		for (FormaRetirada formaRetirada : FormaRetirada.values()) {
			if(formaRetirada.getId().equals(id)) {
				return formaRetirada;
			}
		}
		return null;
	}
	
}
