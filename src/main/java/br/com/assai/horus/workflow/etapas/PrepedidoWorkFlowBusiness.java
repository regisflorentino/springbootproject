package br.com.assai.horus.workflow.etapas;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.assai.horus.context.domain.annotation.TransactionalContext;
import br.com.assai.horus.converter.response.PrepedidoConverter;
import br.com.assai.horus.engineworkflow.definition.WorkflowPersistenceService;
import br.com.assai.horus.engineworkflow.exception.HorusEngineWorkflowPersistenceException;
import br.com.assai.horus.entity.prepedido.Prepedido;
import br.com.assai.horus.entity.prepedido.StatusTelevendas;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.rule.IdentifiableEntity;
import br.com.assai.horus.service.prepedido.PrepedidoBusinessImpl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("TELEVENDAS_PREPEDIDO_WORKFLOW_BEAN")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrepedidoWorkFlowBusiness implements WorkflowPersistenceService<PrepedidoResponse> {
     
	@Autowired private PrepedidoBusinessImpl business;
	@Autowired private PrepedidoConverter prepedidoConveter;
	
    @Override
    @Transactional(rollbackFor = Exception.class)
    public IdentifiableEntity mergeWorkflow(PrepedidoResponse response) throws HorusEngineWorkflowPersistenceException {
    	try {
    		log.info("PrepedidoWorkFlowBusiness mergeWorkflow()  do Prepedido atraves do workflow - iniciando");
    		log.info("PrepedidoWorkFlowBusiness mergeWorkflow() - obtendo o idPedido gerado previamente.");
    		response.setIdPedido(business.getIdPedidoByIdPrepedido(response.getId()));
    		log.info("ENTITY = " + response);
    		Prepedido model = prepedidoConveter.convertToModel(response);
    		
    		if(model.getDataGeracaoPedido() == null) {
    			model.setDataGeracaoPedido(DateTime.now().toDate());
			}
    		
    		response.setIdStatus( StatusTelevendas.FINALIZADO);
    		response = business.merge( response);
    		log.info("ENTITY SAVED = " + response);
    		log.info("PrepedidoWorkFlowBusiness mergeWorkflow()  do Prepedido atraves do workflow - finalizado com sucesso !");
    		return response;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Merge do Prepedido atraves do workflow Error  = " +  e.getMessage());
			throw new HorusEngineWorkflowPersistenceException("Fase de Verifcacao de Entidade antes do merge "  +  e.getMessage(), response.getId(), "LABELS.ERROR.TESTE");
		}
    }
}
