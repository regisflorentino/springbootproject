package br.com.assai.horus.enums;

public enum TipoVendaStatusType {
	
	FULL(2L), EMPTY(1L), PARTIAL(0L);

	private Long valor;
	
	TipoVendaStatusType(Long valor){
		this.valor = valor;
	}

	public Long getValor() {
		return valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}	

}
