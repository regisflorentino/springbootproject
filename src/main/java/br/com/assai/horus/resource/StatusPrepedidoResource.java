package br.com.assai.horus.resource;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.assai.horus.response.prepedido.StatusPrepedidoResponse;
import br.com.assai.horus.service.prepedido.StatusPrepedidoBusiness;

@RestController
@RequestMapping("/statusdoprepedido")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class StatusPrepedidoResource extends EntityResource<StatusPrepedidoResponse, StatusPrepedidoBusiness> {

	private static final long serialVersionUID = -3852868933400056644L;

}
