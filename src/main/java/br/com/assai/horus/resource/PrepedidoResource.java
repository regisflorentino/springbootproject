package br.com.assai.horus.resource;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.prepedido.PrepedidoRepository;
import br.com.assai.horus.response.prepedido.CupomResponse;
import br.com.assai.horus.response.prepedido.FormaPagamentoAnexoResponse;
import br.com.assai.horus.response.prepedido.FormaPagamentoResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;
import br.com.assai.horus.response.prepedido.LimiteCreditoClienteResponse;
import br.com.assai.horus.response.prepedido.NotaResponse;
import br.com.assai.horus.response.prepedido.PrazosPagamentoBoletoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoWorkflowResponse;
import br.com.assai.horus.service.formapagamento.FormaPagamentoBusiness;
import br.com.assai.horus.service.prepedido.PrepedidoBusiness;

@RestController
@RequestMapping("/prepedido")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrepedidoResource extends EntityResource<PrepedidoResponse, PrepedidoBusiness> {

    private static final long serialVersionUID = -4214710823552727958L;
    
    @Autowired
    private FormaPagamentoBusiness formaPagamentoBusiness;
    
    /**
     * Obtem um novo numero de prepedido
     * @return Numero do prepedido
     */
    @GetMapping("/next/id")
    public Long getNextId() {
        return this.getBusiness().getNextId();
    }
    
    
    /**
     * Obtem um novo numero de pedido
     * @return Numero do pedido
     */
    @GetMapping("/next/idpedido")
    public Long getNextIdPedido() {
        return this.getBusiness().getAllocatedIdPedido();
    }
    
    /**
     * Atualiza o status do Workflow
     * @param idWorkflow  Identificador do WKF
     * @param idStatus  Status
     * @return
     * @throws MapperRegistryException 
     */
    @PostMapping("/statusworkflow/{idWorkflow}/idStatus/{idStatus}")
    public boolean  updateStatusWkf(@PathVariable("idWorkflow") Long idWorkflow , @PathVariable("idStatus") Long idStatus) throws MapperRegistryException {
    	boolean sucess = true;
    	try {
    		this.getBusiness().updateStatusWorkflow(idWorkflow , idStatus); 
    	}catch (Exception e) {
    		return sucess = false;
		}
        return sucess;
    	}
    /**
     * Atualiza o predido add o idPEdido no Contexto de Workflow
     * @param idPrepedido 
     * @param idPedido  
     * @return
     * @throws MapperRegistryException 
     */
    @PostMapping("/updatePrepedido/{idPrepedido}")
    public long  updatePrepedido(@PathVariable("idPrepedido") Long idPrepedido ) throws MapperRegistryException {
    	long idPedido = this.getBusiness().getAllocatedIdPedido();
    	Long idPedidoByIdPrepedido = this.getBusiness().getIdPedidoByIdPrepedido(idPrepedido);
		if(idPedidoByIdPrepedido != null) {
    		this.getBusiness().deleteAllocatedIdPedido(idPedido);
    		return  idPedidoByIdPrepedido;
    	}
		
    	try {
    		this.getBusiness().updatePrepedido(idPrepedido , idPedido);
    	}catch (Exception e) {
    		return  0;
		}
        return idPedido;
    	}
 
	/**
     * Efetua download de um anexo
     * @param idAnexo Identificador do anexo
     * @return
     */
    @GetMapping("/formapagamento/anexo/{idAnexo}/download")
    public @ResponseBody ResponseEntity<ByteArrayResource> download(@PathVariable("idAnexo") Long idAnexo) {
		byte[] pdf = this.getBusiness().getFileInPDF(idAnexo);
		if (pdf == null) {
			throw new IllegalStateException("Arquivo não encontrado!");
		}
		MediaType mediaType = new MediaType("application", "pdf");
        return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "file.pdf")
                        .contentType(mediaType)
                        .contentLength(pdf.length) 
                        .body(new ByteArrayResource(pdf));
    }
    
    /**
     * Efetua upload de um arquivo vinculado a uma forma de pagamento do prepedido
     * @param idFormaPagamento IDentificador da forma de pagamento
     * @param file Arquivo a ser anexado
     * @return ?
     * @throws IOException
     * @throws MapperRegistryException 
     */
    @PostMapping("/formapagamento/{idFormaPagamento}/anexo/upload")
    public FormaPagamentoAnexoResponse uploadFile(@PathVariable("idFormaPagamento") Long idFormaPagamento, @RequestParam("file") MultipartFile file) throws IOException, MapperRegistryException {
		Path path = Paths.get(StringUtils.replace(StringUtils.trimToEmpty(file.getOriginalFilename()), "\\", "/"));
    	return this.getBusiness().saveFormaPagamentoAnexo(idFormaPagamento, path.getFileName().toString(), file.getBytes());
    }

    /**
     * Exclui um anexo vinculado a uma forma de pagamento do prepedido
     * @param idFormaPagamento Identificador da forma de pagamento
     * @param idsSelected ?
     * @return ?
     * @throws MapperRegistryException 
     */
    @DeleteMapping(value = "/formapagamento/{idFormaPagamento}/anexo")
    public List<FormaPagamentoAnexoResponse> deleteFormaPagamentoAnexo(@PathVariable("idFormaPagamento") Long idFormaPagamento,
    												   @RequestBody(required = false) List<Long> idsSelected) throws MapperRegistryException {
    	
    	List<FormaPagamentoAnexoResponse> result = null;
        if ( idsSelected != null && ! idsSelected.isEmpty()) {
        	result = getBusiness().deleteFormaPagamentoAnexo(idFormaPagamento, idsSelected);
        } 
        return result; 
    }
    
    /**
     * Obtem limite de credito para um prepedido
     * @param id Identificador do prepedido
     * @return
     * @throws MapperRegistryException 
     */
    @GetMapping("/limitecredito")
    public LimiteCreditoClienteResponse getLimiteCreditoByIdPredido(@RequestParam(required=true) Long id) throws MapperRegistryException {
        return this.getBusiness().getLimiteCredidoIdPredido(id);
    }
    
    @GetMapping("/workflows")
    public List<PrepedidoWorkflowResponse> getPrepedidoWorkflows(@RequestParam(required=false) String status) throws MapperRegistryException {
        return this.getBusiness().getPrepedidoWorkflows(status);
    }
    
    /**
     * Exclui prepedido
     * @param id Identificador do pedido
     * @return Prepedido excluido
     */
    @DeleteMapping("/deleteAll")
    public void excluir(@RequestParam(value="ids", required=false) List<Long> ids) {
    	try {
    		this.getBusiness().removeAllPrepedidos(ids);
		} catch (Exception e) {
			throw e;
		}
    }
    
    /**
     * Clona prepedido
     * @param id Identificador do pedido
     * @return Prepedido clonado
     */
    @PostMapping("/vendedor/{vendedorId}/clonar/{id}")
    public Long clonar(@PathVariable("id") Long id, @PathVariable("vendedorId") Long vendedorId) {
		return this.getBusiness().clonePrePedido(id, vendedorId).getId();
    }
    
    /**
     * Cancelar  prepedido
     * @param id Identificador do pedido
     * @return Prepedido excluido
     */
    @DeleteMapping("/cancelAll")
    public void canceAll(@RequestParam(value="ids", required=false) List<Long> ids) {
    	try {
    		this.getBusiness().cancelAllPrepedidos(ids);
		} catch (Exception e) {
			throw e;
		}
    }
    
    @PostMapping("/cancel/{id}")
    public void cancelar(@PathVariable("id") Long id) {
		this.getBusiness().cancelarPrepedido(id);
    }
    
    @PostMapping("/reprove")
    public void reprovar(@RequestParam("id") Long id, @RequestParam("idStatus") Long idStatus) {
		this.getBusiness().reprovarPrepedido(id, idStatus);
    }
    
    /**
     * Obtem formas de pagamento vinculados a um prepedido
     * @param id
     * @return
     * @throws MapperRegistryException 
     */
    @GetMapping("/formaspagamento")
    public List<FormaPagamentoResponse> getFormasPagamentoByIdPredido(@RequestParam(required=true) Long id) throws MapperRegistryException {
        return this.getBusiness().getFormasPagamentoByIdPredido(id);
    }
    
    /**
     * Obtem anexos vinculados a uma forma pagamento
     * @param id
     * @return forma pagamentos anexos
     */
    @GetMapping("/anexos")
    public List<FormaPagamentoAnexo> getAnexos(@RequestParam(required=true) Long id) {
    	return getBusiness().getFormasPagamentoAnexoByIdFormaPagamento(id);
    }
    
    /**
     * Obtem itens vinculados a um prepedido
     * @param id
     * @return
     */
    @GetMapping("/itens")
    public List<ItemPrepedidoResponse> getItensByIdPredido(@RequestParam(required=true) Long id) {
        return this.getBusiness().getItensPrepedidoByIdPredido(id);
    }
    
    /**
     * Salva um prepedido  
     * @param prepedido
     * @return Prepedido salvo
     * @throws MapperRegistryException 
     */
    @PostMapping(value="/save", consumes = {org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE, org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
    public PrepedidoResponse save(@RequestBody(required=true) PrepedidoResponse prepedido) throws MapperRegistryException {
        PrepedidoResponse merge = this.getBusiness().merge(prepedido);
        if(merge.getIdPedido() == null) {
        	long idPedido = this.getBusiness().getAllocatedIdPedido();
        	merge.setIdPedido(idPedido);
        	return this.getBusiness().merge(merge);
        }
        return merge;
    }
    
    @PostMapping(value="/margem", consumes = {org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE, org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
    public ItemPrepedidoResponse verificaRangemargem(@RequestBody(required=true) ItemPrepedidoResponse item, @RequestParam(required=true) Long idFilial) throws MapperRegistryException {
    	return getBusiness().verificaRangeMargem(idFilial, item);
    }
    
    /**
     * Obtem um novo numero de prepedido
     * @return Numero do prepedido
     */
    @GetMapping("/next/item/id")
    public Long getPrepedidoItemNextId() {
        return this.getBusiness().getItemNextId();
    }
    
    /**
     * Obtem um novo range de numero de prepedido
     * @param lenght quantidade de ids que irão retornar
     * @return Numero do prepedido
     */
    @GetMapping("/next/item/ids")
    public Set<Long> getPrepedidoItemNextId(@RequestParam(name="lenght") Long tamanho) {
    	Set<Long> ids = new HashSet<>();
    	for (int i = 0; i < tamanho; i++) {
			ids.add(this.getBusiness().getItemNextId());
		}
        return ids;
    }
    
    
    /**
     * Obtem um novo numero de prepedido
     * @return Numero do prepedido
     */
    @GetMapping("/next/formaPagamento/id")
    public Long getFormaPagamentoNextId() {
        return this.formaPagamentoBusiness.nextValSequence();
    }
    


    /**
     * Filtra prepedidos
     * @param idFilial Identificador da filial
     * @param dataInicial Data de inicio para o filtro
     * @param dataFinal Data de termino do filtro
     * @param flgExcluidos Indicador de registros excluidos
     * @return
     */
	@GetMapping("/")
    public List<PrepedidoResponse> findByFilter(@RequestParam(value="idFilial", required=false) Long idFilial,
        @RequestParam(value="dataInicial", required=false) Date dataInicial, 
        @RequestParam(value="dataFinal", required=false) Date dataFinal,
        @RequestParam(value="flgExcluidos", required=true) boolean flgExcluidos,
        @RequestParam(value="idProduto", required=false) List<Long> produtos
		) {
		if(dataFinal != null) {
			dataFinal.setHours(23);
			dataFinal.setMinutes(59);
			dataFinal.setSeconds(59);
		}
            return this.getBusiness().findByFilter(idFilial, dataInicial, dataFinal, produtos, flgExcluidos);
    }

    /**
     * Obtem um novo numero de prepedido
     * @return Numero do prepedido
     */
    @GetMapping("/count")
    public Long listCount(@RequestParam(value="idFilial", required=true) Long idFilial,
            @RequestParam(value="dataInicial", required=false) Date dataInicial, 
            @RequestParam(value="dataFinal", required=false) Date dataFinal,
            @RequestParam(value="flgExcluidos", required=false) boolean flgExcluidos,
            @RequestParam(value="idsProduto", required=false) List<Long> idsProduto) {
        return this.getBusiness().countByFilter(idFilial, dataInicial, dataFinal, idsProduto, flgExcluidos);
    }
    
    /**
     * Retorna produto com bloquio de preco gerente
     * @param idFilial
     * @param idProduto
     * @return ProdutoDependencyResponse
     */
    @GetMapping("/bloqueioGerente")
    public Long getProdutoBloqueadoPrecoGerente(
    		@RequestParam(value="idFilial",required=true) Long idFilial,
    		@RequestParam(value="idProd",required=true) Long idProd) {
        return this.getBusiness().getBloqueioPrecoGerente(idFilial, idProd);
    }
    
    @GetMapping("pagamentos/cupom/{idPedidoCapa}")
    public List<CupomResponse> findFaturamentosCupom(@PathVariable(value = "idPedidoCapa", required = true) Long idPedidoCapa) {
		return this.getBusiness().findFaturamentosCupom(idPedidoCapa);
	}
    
	@GetMapping("pagamentos/nota/{idPedidoCapa}")
	public NotaResponse findFaturamentosNota(@PathVariable(value = "idPedidoCapa", required = true) Long idPedidoCapa) {
		return this.getBusiness().findFaturamentosNota(idPedidoCapa);
	}
	
	@GetMapping("/prazo/pagamento/boleto")
	public List<PrazosPagamentoBoletoResponse> getPrazosPagamentoBoleto() {
		return  this.getBusiness().getPrazosPagamentoBoleto();
	}
	
	@GetMapping("/pedidos")
    public List<PrepedidoRepository.ResumoPedido> getPedidoByIdCliente(@RequestParam(required=true) Long idCliente) {
        return this.getBusiness().getPedidosByIdCliente(idCliente);
    }
	
    /**
     * Busca a data de validade do pedido parametrizada no módulo ADM
     * @return periodoValidadePedidoParam
     */
    @GetMapping("periodovalidadepedido")
    public int getPeriodoValidadePedidoParam() {
        return this.getBusiness().getPeriodoValidadePedidoParam();
    }
    
    /**
     * Retorna os ultimos  prepedidos cadastrados para  atualizar a listagem automaticamente
     * @param idFilial Identificador da filial
     * @param idPrepedidoLast  Id do ultimo prepedido gerado
     * @return
     */
	@GetMapping("/prepedidosrefresh")
    public List<PrepedidoResponse> prepedidosRefresh(@RequestParam(value="idFilial", required=false) Long idFilial,@RequestParam(value="idPrepedidoLast", required=false) Long idPrepedidoLast) {
            return this.getBusiness().prepedidosRefresh(idFilial,idPrepedidoLast);
    }
	
}