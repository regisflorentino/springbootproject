package br.com.assai.horus.resource;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.assai.horus.response.dependency.ProdutoDependencyResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;
import br.com.assai.horus.service.produto.ProdutoBusiness;

@RestController
@RequestMapping("/produto")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProdutoResource implements Serializable {
    
    private static final long serialVersionUID = 2391184470948858350L;
    
    @Autowired private ProdutoBusiness produtoBusiness;
    
    /**
     * Através do nome ou do id do produto traz uma lista de produtos válidos para televendas
     * 
     * @param descricaoCompleta Nome ou id do produto
     * @param idFilial id da filial em que o produto está sendo comercializado, informado para checar se o produto está habilidado na
     * filial 
     * 
     * @return Lista de produtos válidos
     */
    @GetMapping("/validos")
    public List<ProdutoDependencyResponse> findValidProdutosToTelevendasByDescricaoCompleta(
           @RequestParam("descricaoCompleta") String descricaoCompleta,
           @RequestParam(value="idFilial", required=true) Long idFilial) {   
        return produtoBusiness.findValidProdutosToTelevendasByDescricaoCompleta(descricaoCompleta, idFilial);
    }
    
    /**
     * Obtem os produtos ativos, com estoque e com codigo de barra ativo conforme filtro
     * @param idProduto Identificador do produto
     * @param idFilial Identificador da filial
     * @return Lista de itens de produtos validos para um prepedido
     */
    @GetMapping("/validos/preco/estoque")
    public List<ItemPrepedidoResponse> getProdutoValidoComPrecoAndEstoqueInFilial(@RequestParam(value="idProduto", required=true) Long idProduto, 
                                                                                  @RequestParam(value="idFilial", required=true) Long idFilial,
                                                                                  @RequestParam(value="idPromocao", required=false) Long idPromocao) {
        return produtoBusiness.findDadosItemPrePedidoDeProdutoValidoComPrecoAndEstoqueInFilial(idProduto, idFilial, idPromocao);
    }

}
