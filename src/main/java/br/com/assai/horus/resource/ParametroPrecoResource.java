package br.com.assai.horus.resource;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.assai.horus.response.parametropreco.ParametroPrecoResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoFilialResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoProdutoResponse;
import br.com.assai.horus.service.parametropreco.ParametroPrecoBusiness;;

@RestController
@RequestMapping("/parametrospreco")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParametroPrecoResource extends EntityResource<ParametroPrecoResponse, ParametroPrecoBusiness> {
    
    private static final long serialVersionUID = 6943979488023713058L;
    
    @Override
    @GetMapping(value="/")
    public List<ParametroPrecoResponse> findAll() {
        return this.getBusiness().listAll();
    }
                             
    @GetMapping(value="/filiais")
    public List<ParametroPrecoFilialResponse> listFiliais() {
        return this.getBusiness().listParametroPrecoFiliais(null);
    }
    
    @GetMapping(value="/filiais/{idParametroPreco}")
    public List<ParametroPrecoFilialResponse> findFiliais(@PathVariable("idParametroPreco") Long idParametroPreco) {
        return this.getBusiness().listParametroPrecoFiliais(idParametroPreco);
    }

    @GetMapping(value="/filial")
    public List<ParametroPrecoFilialResponse> findFilial(@RequestParam("idFilial") Long idFilial) {
        return this.getBusiness().verifyParametroPrecoFilial(idFilial);
    }

    @GetMapping(value="/produtos")
    public List<ParametroPrecoProdutoResponse> findProdutos(@RequestParam("idParametroPrecoFilial") Long idParametroPrecoFilial) {
        return this.getBusiness().listParametroPrecoProdutos(idParametroPrecoFilial);
    }

    @PreAuthorize("hasAuthority('TLVM02_01')")
    @DeleteMapping(value = "/deleteAll")
    public ResponseEntity<?> delete(@RequestParam("ids") List<Long> ids) throws JsonParseException, JsonMappingException, IOException {
        if ( ids != null && ! ids.isEmpty()) {
            getBusiness().delete(ids);
        } 
        return new ResponseEntity<>(null, HttpStatus.OK); 
    }
        
	@PostMapping(value = "")
	public ResponseEntity<ParametroPrecoResponse> merge(@RequestBody final ParametroPrecoResponse entity) {
        ParametroPrecoResponse parametroPreco = getBusiness().save(entity);
        
        if(Objects.nonNull(parametroPreco)) {
        	return new ResponseEntity<>(parametroPreco, HttpStatus.OK);
        }
        return new ResponseEntity<>(parametroPreco, HttpStatus.CONFLICT);
        
	}
	
	 @GetMapping(value="/prodbyid")
	    public List<ParametroPrecoProdutoResponse> findProdutosById(@RequestParam("idFilial") Long idFilial, @RequestParam("idProd") Long idProd) {
	        return this.getBusiness().listParametroPrecoProdutosByIdProd(idFilial, idProd);
	    }
}
