package br.com.assai.horus.resource;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.response.ParamentroTipoVendaResponse;
import br.com.assai.horus.service.parametrotipovenda.ParamentroTipoVendaBusiness;

@RestController
@RequestMapping("/tipoVenda/parametros")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParamentroTipoVendaResource extends EntityResource<ParamentroTipoVendaResponse, ParamentroTipoVendaBusiness> {

    private static final long serialVersionUID = 1768646027733450195L;

    @GetMapping(value="/filial")
    private ParamentroTipoVendaResponse getByFilial(@RequestParam(value="idFilial", required=true) Long idFilial) throws MapperRegistryException {
        return getBusiness().findByIdFilial(idFilial);
    }    

    @GetMapping(value="")
    public List<?> listAll() {
        return this.getBusiness().listAll();
    }
        
    @PostMapping(value="/", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public void saveAll(@RequestBody List<ParamentroTipoVendaResponse> parametros) throws MapperRegistryException {
        getBusiness().saveAll(parametros);
	}
    
}
