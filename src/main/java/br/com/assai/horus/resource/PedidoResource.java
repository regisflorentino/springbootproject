package br.com.assai.horus.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.assai.horus.converter.response.FormaPagamentoConverter;
import br.com.assai.horus.converter.response.ItemPedidoConverter;
import br.com.assai.horus.converter.response.PedidoConverter;
import br.com.assai.horus.entity.constant.FormaRetirada;
import br.com.assai.horus.entity.pedido.ItemPedido;
import br.com.assai.horus.entity.pedido.Pedido;
import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.formapagamento.FormaPagamentoRepository;
import br.com.assai.horus.repository.pedido.ItemPedidoRespository;
import br.com.assai.horus.response.pedido.PedidoResponse;
import br.com.assai.horus.response.pedido.mapa.separacao.ItemPedidoMapaSeparacaoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.service.pedido.PedidoBusiness;
import br.com.assai.horus.service.pedido.PedidoBusinessImpl;

@RestController
@RequestMapping("/pedidos")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PedidoResource extends EntityResource<PedidoResponse, PedidoBusiness> {

	private static final long serialVersionUID = -5403019125235221074L;
	
	@Autowired
	private PedidoConverter pedidoConverter;
	@Autowired
	private FormaPagamentoRepository formaPagtoRepo;
	@Autowired
	private FormaPagamentoConverter formaConverter;
	@Autowired
	private ItemPedidoConverter itemConverter;
	@Autowired
	private ItemPedidoRespository itemRepo;
	@Autowired
	private PedidoBusinessImpl pedidoBusiness;
	

    /**
     * Filtra prepedidos
     * @param idFilial Identificador da filial
     * @param dataInicial Data de inicio para o filtro
     * @param dataFinal Data de termino do filtro
     * @param flgExcluidos Indicador de registros excluidos
     * @return
     */
	@GetMapping("/")
    public List<PedidoResponse> findByFilter(@RequestParam(value="idFilial", required=true) Long idFilial,
										        @RequestParam(value="dataInicial", required=false) Date dataInicial, 
										        @RequestParam(value="dataFinal", required=false) Date dataFinal,
										        @RequestParam(value="flgExcluidos", required=true) boolean flgExcluidos,
										        @RequestParam(value="idProduto", required=false) List<Long> produtos) {
		return this.getBusiness().findByFilter(idFilial, dataInicial, dataFinal, produtos, flgExcluidos);
    }
	
	@GetMapping("/findById")
	@Override
    public PedidoResponse findById(@RequestParam(value="id", required=false) Long idPedido) {
		try {
			Pedido findById = this.getBusiness().findById(idPedido).orElse(null);
			List<FormaPagamento> formasPgto = formaPagtoRepo.findByIdPedido(idPedido);
			List<ItemPedido> itens = itemRepo.findByidPedido(idPedido);
			PedidoResponse pedidoRespose = pedidoConverter.convertToResponse(findById);
			pedidoRespose.setFormasPagamento(formaConverter.convertListModelToListResponse(formasPgto));
			pedidoRespose.setItens(itemConverter.listModelToListResponse(itens));
			pedidoRespose.setNomeVendedor(pedidoBusiness.getNomeVendedor(pedidoRespose.getIdVendedor()));
			return pedidoRespose;
		} catch (MapperRegistryException e) {
			e.printStackTrace();
			return null;
		}
    }
	
	  /**
     * Salva um pedido  
     * @param prepedido
     * @return Pedido salvo
     * @throws MapperRegistryException 
     */
    @PostMapping(value="/save", consumes = {org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE, org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
    public Pedido save(@RequestBody(required=true) PrepedidoResponse prepedido) throws MapperRegistryException {
    	Optional<Pedido> optional = getBusiness().findById(prepedido.getId());
    	optional.get().setObservacaoNota(prepedido.getObservacaoNota());
    	optional.get().setObservacaoPedido(prepedido.getObservacaoPedido());
    	optional.get().setDataPrevistaRetirada(prepedido.getDataPrevistaRetirada());
    	optional.get().setFormaRetirada(FormaRetirada.searchById(prepedido.getFormaRetirada()));
    	optional.get().setIdTransportadora(prepedido.getIdTransportadora());
    	optional.get().setIdStatus(prepedido.getIdStatus());
    	optional.get().setNomeMotorista(prepedido.getNomeMotorista());
    	optional.get().setNomeVeiculo(prepedido.getNomeVeiculo());
    	optional.get().setNumeroRgCpfRetirada(prepedido.getNumeroRgCpfRetirada());
    	optional.get().setPlaca(prepedido.getPlaca());
    	optional.get().setTelefoneMotorista(prepedido.getTelefoneMotorista());
    	return this.getBusiness().merge(optional.get()); 
    	
    }
    
    @PostMapping(value="/cancel")
    public void cancelarPedido(@RequestParam(value="idsPedidos", required=false) List<Long> idsPedidos) throws MapperRegistryException {
    	this.pedidoBusiness.cancelarPedido(idsPedidos);
    }
    
    @PostMapping(value="/integrar")
    public Pedido integrarEmporium(@RequestParam(required=true) Long idPedido) throws MapperRegistryException {
        if(this.pedidoBusiness.enviarRequisicao(idPedido)) {
        	 Optional<Pedido> optional = getBusiness().findById(idPedido);
             optional.get().setIdStatus(15L);
         	return this.getBusiness().merge(optional.get());
        }else {
        	 Optional<Pedido> optional = getBusiness().findById(idPedido);
             optional.get().setIdStatus(14L);
         	  this.getBusiness().merge(optional.get());
         	 return null;
        } 
       
    }
    
    @GetMapping("/mapaseparacao")
    public  List<ItemPedidoMapaSeparacaoResponse> mapaSepaaracao(@RequestParam(required=true) Long idPedido) throws MapperRegistryException {
         return this.pedidoBusiness.mapaSeparacao(idPedido);
     }
    
    @GetMapping("/termoretirada")
    public  List<ItemPedidoMapaSeparacaoResponse> termoRetirada(@RequestParam(required=true) Long idPedido) throws MapperRegistryException {
         return this.pedidoBusiness.termoRetirada(idPedido);
     }
	
}
