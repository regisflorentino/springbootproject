package br.com.assai.horus.resource;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.response.IdentificacaoVendaCupomResponse;
import br.com.assai.horus.service.idenficacaoVendaCupom.IdentificacaoVendaCupomBusiness;


@RestController
@RequestMapping("/identificacaoVendaCupom")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IdentificacaoVendaCupomResource extends EntityResource<IdentificacaoVendaCupomResponse, IdentificacaoVendaCupomBusiness> {

    private static final long serialVersionUID = 1768646027733450195L;
    
    @Override
    @GetMapping(value="/")
    public List<IdentificacaoVendaCupomResponse> findAll() {
        return this.getBusiness().listAll();
    }
    
    @GetMapping(value="/filial")
    public IdentificacaoVendaCupomResponse findByFilial(@RequestParam("idFilial") Long idFilial) {
        return this.getBusiness().findByFilial(idFilial);
    }

    @PostMapping(value="/")
    public List<IdentificacaoVendaCupomResponse> mergeList(@RequestBody(required = true) List<IdentificacaoVendaCupomResponse> values) throws MapperRegistryException {
        this.getBusiness().mergeList(values);
        return this.getBusiness().listAll();
    }  
}
