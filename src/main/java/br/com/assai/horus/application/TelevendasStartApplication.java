package br.com.assai.horus.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

public class TelevendasStartApplication extends HorusApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelevendasStartApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("InitAplication");
		log.info("Server Started");
	}

	
    @Bean
    @Autowired
    public ApplicationContextProvider getApplicationContextProvider(ApplicationContext applicationContext) {
        ApplicationContextProvider result = new ApplicationContextProvider();
        result.setApplicationContext(applicationContext);
        return result;
    }	

}
