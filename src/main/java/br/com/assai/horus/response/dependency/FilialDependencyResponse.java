package br.com.assai.horus.response.dependency;

import br.com.assai.horus.response.ParamentroTipoVendaResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.annotation.MapperProperty;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilialDependencyResponse implements HorusDataResponse {
    
	private static final long serialVersionUID = 7695695896324885239L;

	private Long id;
    private String nome;
    private String uf;
    
    @MapperProperty("regional.id")
    private Long idRegional;
    
    private String nomeExibicao;
    
    private ParamentroTipoVendaResponse parametroTipoVenda;

}
