package br.com.assai.horus.response.dependency;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.annotation.MapperProperty;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProdutoDependencyResponse implements HorusDataResponse {

	private static final long serialVersionUID = 506316901486878975L;
	private Long id;
    private Long codigo;
    private String descricaoCompleta;
    private String descricaoReduzida;
    private String descricaoBasica;
    
    @MapperProperty("estruturaMercadologica.id")
    private Long idEstruturaMercadologica;
    
    private int estoque;
  
    public String getNomeExibicao() {
        return String.valueOf(codigo) + "-" + descricaoCompleta;
    }    
    
}
