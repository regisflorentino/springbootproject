package br.com.assai.horus.response.prepedido;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrazosPagamentoBoletoResponse implements HorusDataResponse{
	
	private static final long serialVersionUID = -3927079537772178940L;
	
	private Integer idPrazoPagamento;
    
    private String prazoPagamento;
    
    private int nrodiavencto;
  
}
