package br.com.assai.horus.response.parametropreco;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.annotation.MapperProperty;
import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.ParametroPrecoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ResponseConverter(ParametroPrecoConverter.class)
public class ParametroPrecoResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = 4899284241782320138L;

    private Long id;
    
    private Long idRegional;
    
    private String nomeRegional;
    
    private String nomeGrupo;
    
    private Date dataCadastro;

    private Date dataAlteracao;
    
    @MapperProperty("usuarioCadastro.id")
    private Long idUsuarioCadastro;
    
    @MapperProperty("usuarioCadastro.nome")
    private String nomeUsuarioCadastro;

    @MapperProperty("usuarioAlteracao.id")
    private Long idUsuarioAlteracao;
    
    @MapperProperty("usuarioAlteracao.nome")
    private String nomeUsuarioAlteracao;

    private String qtdFiliais;
    
    private String nomeFilial;
    
    private List<ParametroPrecoFilialResponse> filiais;
    
}