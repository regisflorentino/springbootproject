package br.com.assai.horus.response.dependency;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClienteDependencyResponse implements HorusDataResponse {

    private static final long serialVersionUID = -4353147382290951452L;
    
    private Long id;
    private Boolean ativo;
    private PessoaDependencyResponse pessoa;
    private Date dataCadastro;
    private Long idUsuarioCadastro;
    private String nomeUsuarioCadastro;
    private Date dataAlteracao;
    private Long idUsuarioAlteracao;
    private String nomeUsuarioAlteracao;
    private Long idTransacao;
    
    private FormaPagamentoDependencyResponse formaPagamento;
    private PrazoMaximoPagamentoDependencyResponse prazoMaximoPagamento;
    
    public boolean isPermitidoPagarEmCheque() {
        return formaPagamento != null  && formaPagamento.getId() == FormaPagamentoDependencyResponse.ID_CHEQUE;
    }
    
    public boolean isPermitidoPagarEmBoleto() {
        return formaPagamento != null  && formaPagamento.getId() == FormaPagamentoDependencyResponse.ID_BOLETO;
    }    
    
}
