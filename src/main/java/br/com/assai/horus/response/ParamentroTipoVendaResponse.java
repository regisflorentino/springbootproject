package br.com.assai.horus.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.annotation.MapperProperty;
import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.ParamentroTipoVendaConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ResponseConverter(ParamentroTipoVendaConverter.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParamentroTipoVendaResponse implements HorusDataResponse{
    
    private static final long serialVersionUID = 8414723822778243162L;
    
    private Long id;
    private Long idFilial;
    private Long idRegional;
    private String nomeRegional;
    private String nomeFilial;
    private Date dataCadastro;
    private Date dataAlteracao;

    @MapperProperty("usuarioCadastro.id")
    private Long idUsuarioCadastro;

    @MapperProperty("usuarioCadastro.nome")
    private String nomeUsuarioCadastro;
    
    @MapperProperty("usuarioAlteracao.id")
    private Long idUsuarioAlteracao;

    @MapperProperty("usuarioAlteracao.nome")
    private String nomeUsuarioAlteracao;
    
    private Boolean vendaInterna;
    private Boolean vendaInternaPdv;
    private Boolean vendaInterstadual;
    private Boolean vendaInterstadualPdv;
    private Boolean vendaExportacao;
    private Boolean vendaExportacaoPdv;
    private Long recordChanged;
    private Long versao;
    
}
