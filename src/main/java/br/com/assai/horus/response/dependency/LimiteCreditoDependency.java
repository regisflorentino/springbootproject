package br.com.assai.horus.response.dependency;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TVD_LIMITE_CREDITO")
@Data
@EqualsAndHashCode(of = {"id"})
public class LimiteCreditoDependency {
    
    @Id
    @Column(name="ID_LIMITE_CREDITO")
    private Long id;
    
    @Column(name="VL_LIMITE_CREDITO")
    private BigDecimal limiteCredido;
    
    @Column(name="VL_TITULO_ABERTO")
    private BigDecimal saldoTitulosAbertos;

}
