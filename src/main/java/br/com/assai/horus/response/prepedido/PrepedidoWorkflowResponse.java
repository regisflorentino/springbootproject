package br.com.assai.horus.response.prepedido;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class PrepedidoWorkflowResponse {
	
	private String filial;
	private Long idPrepedido;
	private String  idPedido ;
	private String nome;
	private BigDecimal valor;
	private String nomeVendedor;
	private Date  dtAlteracao;
	private String dataAlteracao;
	private String nomeUsuarioAlteracao;
	private String etapa;
	private String status;
	private Long detalhewf;
	private long idUsuarioCadastro;
	private Date dtCadastro;
	private long idTransacao;

}
