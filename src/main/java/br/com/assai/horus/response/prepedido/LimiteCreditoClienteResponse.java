package br.com.assai.horus.response.prepedido;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
public class LimiteCreditoClienteResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = -5644474835327487551L;
    
    private Long id;
    private Long idPrepedido;
    private Long idPessoa;
    private Date dataConsultaLimite;
    
    private Long idAssociacao;
    private String associacao;
    private String situacao;
    private BigDecimal valorLimiteCredito;
    private BigDecimal valorTituloAberto;
    private BigDecimal valorFaturamentoDia;
    private BigDecimal valorDisponivel;
    private Long prazoMaximo;
    
    private Date dataCadastro;
    private Long idUsuarioCadastro;
    private Date dataAlteracao;  
    private Long idUsuarioAlteracao;
    private Long idTransacao;


}
