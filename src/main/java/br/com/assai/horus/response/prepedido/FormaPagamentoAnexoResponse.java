package br.com.assai.horus.response.prepedido;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormaPagamentoAnexoResponse implements HorusDataResponse{

	private static final long serialVersionUID = 355995680296497536L;
	
	private Long id;
    private Long idFormaPagamento;
    private byte[] arquivo; 
    private String nomeAnexo; 
    
    private Date dataCadastro;
    private Long idUsuarioCadastro;
    private Date dataAlteracao;  
    private Long idUsuarioAlteracao;
    private Long idTransacao;

}
