package br.com.assai.horus.response;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.annotation.MapperProperty;
import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.IdentificacaoVendaCupomConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ResponseConverter(IdentificacaoVendaCupomConverter.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ControleIntegracaoPedidoResponse implements HorusDataResponse, Comparable<ControleIntegracaoPedidoResponse> {
    
    
	private static final long serialVersionUID = 8232527547071310132L;

	private Long nroPedVenda;
	
	private String origemPedido;
	
	private Date dtaBaseFaturamento;

	@Override
	public int compareTo(ControleIntegracaoPedidoResponse o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
    
   

}
