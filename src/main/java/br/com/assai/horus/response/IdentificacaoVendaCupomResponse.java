package br.com.assai.horus.response;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.annotation.MapperProperty;
import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.IdentificacaoVendaCupomConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ResponseConverter(IdentificacaoVendaCupomConverter.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IdentificacaoVendaCupomResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = 8414723822778243162L;
    
    private Long id;
    private Long idRegional;    
    private Long idFilial;
    private String uf;
    private String nomeRegional;
    private String nomeFilial;    
    private BigDecimal valorMaximoVendaCupom;
    private Date dataIniVigenciaValor;
    private Boolean obrigatorioIdentificacao;
    private BigDecimal valorObrigatorioIdentificacao;
    private Date dataIniVigenciaIdentificacao;
    private Date dataCadastro;

    @MapperProperty("usuarioCadastro.id")
    private Long idUsuarioCadastro;
    
    @MapperProperty("usuarioCadastro.nome")
    private String nomeUsuarioCadastro;
    
    private Date dataAlteracao;
    
    @MapperProperty("usuarioAlteracao.id")
    private Long idUsuarioAlteracao;
    
    @MapperProperty("usuarioAlteracao.nome")
    private String nomeUsuarioAlteracao;
    
    private Date minDate;
}
