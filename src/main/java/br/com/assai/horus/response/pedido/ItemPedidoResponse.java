package br.com.assai.horus.response.pedido;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemPedidoResponse implements HorusDataResponse {

	private static final long serialVersionUID = 4612120095845926867L;
	
    private Long id;
    private Long idPedido;
    private Long idProduto;
    private Long codigoProduto;
    private String nomeProduto;
    private Long idEmbalagem;
    private BigDecimal margem;
    private BigDecimal margemAtacado;
    private BigDecimal margemVarejo;
    private BigDecimal percentualTaxaAdm;
    private BigDecimal percentualTotalImposto;
    private Long idProdutoFilialPreco;
    private Boolean negociacaoPontual;
    private Integer numeroItem;
    private Integer quantidade;
    private BigDecimal precoNegociadoEmb;
    private BigDecimal precoNegociadoUnit;
    private BigDecimal qtdDevolvida;
    private BigDecimal qtdNegociada;
    private BigDecimal qtdRetirada;
    private BigDecimal qtdSeparacao;
    private String statusSeparacao;
    private BigDecimal valorDesconto;
    private BigDecimal valorDespesa;
    private BigDecimal valorImposto;
    private BigDecimal valorPrecoVigente;
    private BigDecimal valorTotalProduto;
    private BigDecimal valorVerba;    
    private Long idTransacao;
         
    private Long estoque;
    private String embalagem;
    private BigDecimal precoEmbalagemProduto;
    private Long paleteLastro;
    private Long paleteAltura; 


}
