package br.com.assai.horus.response.prepedido;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import br.com.assai.horus.dto.HorusGenericEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemPrepedidoResponse implements HorusDataResponse {

	private static final long serialVersionUID = -9096244839002604058L;

	private Long id;
	private HorusGenericEntity produto;
	private HorusGenericEntity embalagem;
	private Long idPrepedido;
	private BigDecimal qtdNegociada;
	private BigDecimal precoNegociadoEmb;
	private BigDecimal precoNegociadoUnit;
	private BigDecimal maxPrecoRange;
	private BigDecimal minPrecoRange;
	private BigDecimal valorTotalProduto;
	private Boolean isPermiteReducao;
	private Boolean isPermiteAcrescimo;
    private Integer numeroItem;
	private BigDecimal margem;
	private boolean pontual;
	private ItemPrePedidoValor valor;

	public Long getProdutoId() {
		return this.getId(this.produto);
	}

	public Long getEmbalagemId() {
		return this.getId(this.embalagem);
	}

	private Long getId(HorusGenericEntity entity) {
		if (entity != null) {
			return entity.getId();
		}
		return null;
	}
	
	public ItemPrePedidoValor getValor() {
		if(this.valor == null) {
			this.valor = ItemPrePedidoValor.builder().build();
		}
		return valor;
	}
	
	public Long getIdProdutoFilialPreco() {
		return this.getValor().getIdProdutoFilialPreco();
	}

	public BigDecimal getPrecoEmbalagemProduto() {
		return this.getValor().getPrecoEmbalagemProduto();
	}

	public BigDecimal getValorPrecoVigente() {
		return this.getValor().getValorPrecoVigente();
	}

	public BigDecimal getValorImposto() {
		return this.getValor().getValorImposto();
	}

	public BigDecimal getValorDespesa() {
		return this.getValor().getValorDespesa();
	}

	public BigDecimal getValorVerba() {
		return this.getValor().getValorVerba();
	}

	public BigDecimal getValorDesconto() {
		return this.getValor().getValorDesconto();
	}

	public String getStatusSeparacao() {
		return this.getValor().getStatusSeparacao();
	}

	public BigDecimal getQtdSeparacao() {
		return this.getValor().getQtdSeparacao();
	}

	public BigDecimal getQtdRetirada() {
		return this.getValor().getQtdRetirada();
	}

	public BigDecimal getQtdDevolvida() {
		return this.getValor().getQtdDevolvida();
	}

	public void setIdProdutoFilialPreco(Long idProdutoFilialPreco) {
		this.getValor().setIdProdutoFilialPreco(idProdutoFilialPreco);
	}

	public void setQtdDevolvida(BigDecimal qtdDevolvida) {
		this.getValor().setQtdDevolvida(qtdDevolvida);
	}

	public void setQtdRetirada(BigDecimal qtdRetirada) {
		this.getValor().setQtdRetirada(qtdRetirada);		
	}

	public void setQtdSeparacao(BigDecimal qtdSeparacao) {
		this.getValor().setQtdSeparacao(qtdSeparacao);		
	}

	public void setStatusSeparacao(String statusSeparacao) {
		this.getValor().setStatusSeparacao(statusSeparacao);		
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.getValor().setValorDesconto(valorDesconto);		
	}

	public void setValorDespesa(BigDecimal valorDespesa) {
		this.getValor().setValorDespesa(valorDespesa);		
	}

	public void setValorImposto(BigDecimal valorImposto) {
		this.getValor().setValorImposto(valorImposto);		
	}

	public void setValorPrecoVigente(BigDecimal valorPrecoVigente) {
		this.getValor().setValorPrecoVigente(valorPrecoVigente);		
	}

	public void setValorVerba(BigDecimal valorVerba) {
		this.getValor().setValorVerba(valorVerba);		
	}

}

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class ItemPrePedidoValor {
	private Long idProdutoFilialPreco;
	private String statusSeparacao;
	private BigDecimal precoEmbalagemProduto;
	private BigDecimal valorPrecoVigente;
	private BigDecimal valorImposto;
	private BigDecimal valorDespesa;
	private BigDecimal valorVerba;
	private BigDecimal valorDesconto;
	private BigDecimal qtdSeparacao;
	private BigDecimal qtdRetirada;
	private BigDecimal qtdDevolvida;
}
