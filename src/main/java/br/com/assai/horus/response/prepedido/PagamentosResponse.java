package br.com.assai.horus.response.prepedido;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.FormaPagamentoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
public class PagamentosResponse implements HorusDataResponse{
    
	
	private static final long serialVersionUID = -5132168292799675433L;
	private Long id;
	private String formaPagamento;
	private Long documento;
	private Long noBanco;
	private Long noAgencia;
	private Long noCC;
	private Long noCheque;
    private BigDecimal valor;
 
  
}
