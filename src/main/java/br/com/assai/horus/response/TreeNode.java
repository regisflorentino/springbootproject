package br.com.assai.horus.response;

import java.util.ArrayList;
import java.util.List;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.ToString;

@ToString()
public class TreeNode<T extends HorusDataResponse> {

    private T data;
    private List<TreeNode<T>> children;
    private boolean leaf;

    
    public boolean getExpanded() {
        return getChildren() != null && !getChildren().isEmpty();
    }
    
    public void addChild(TreeNode<T> element) {
        if (getChildren() == null) {
            setChildren(new ArrayList<>());
        }
        getChildren().add(element);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode<T>> children) {
        this.children = children;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }
    
}
