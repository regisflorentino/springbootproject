package br.com.assai.horus.response.prepedido;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.FormaPagamentoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
@ResponseConverter(FormaPagamentoConverter.class)
public class FormaPagamentoResponse implements HorusDataResponse{
    
	private static final long serialVersionUID = -6512395214137765321L;
	private Long id;
    private Long idPrepedido;
    private Long idPedido;
    private Long codigo;
    private String nome;
    private String numeroDeposito;
    private PrazosPagamentoBoletoResponse prazo;
    private List<FormaPagamentoAnexoResponse> anexos; 

    private Date dataCadastro;
    private Long idUsuarioCadastro;
    private Date dataAlteracao;  
    private Long idUsuarioAlteracao;
    private Long idTransacao;
    
    
}
