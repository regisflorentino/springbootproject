package br.com.assai.horus.response.prepedido;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.PrepedidoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import br.com.assai.horus.entity.constant.TipoVendaPrePedido;
import br.com.assai.horus.rule.IdentifiableEntity;
import lombok.Data;

@Data
@ResponseConverter(PrepedidoConverter.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrepedidoResponse implements HorusDataResponse, IdentifiableEntity {

	private static final long serialVersionUID = 5134535419095690147L;

	private String labelCliente;
	
	private Long id;
    private Long idFilial;
    private Long idOperacao;
    private String tipoFaturamento;
    private Long tipoVenda;
    private TipoVendaPrePedido tipoVendaEnum;
    private String tipoOrigem;
    private Long idStatus;
    private Long idCliente;
    
    private boolean clienteNaoIdentificado;
    private boolean cpfCnpjCupom;
    private String numeroCpfCnpjCupom;
    private String nomeVendedor;
    private Long numeroPedidoCliente;

    private Long formaRetirada;
    private Date dataPrevistaRetirada;
    private String nomeMotorista;
    private String nomeVeiculo;
    private Long idTransportadora;
    private String placa;
    
    private int separacao;
    private Long idEndereco;
    private Long idPedido;
    private String idPedidoCliente;
    private Long idVendedor;

    private List<FormaPagamentoResponse> formasPagamento;
    private LimiteCreditoClienteResponse limiteCredito;

    private Date dataGeracaoPedido;

    private List<ItemPrepedidoResponse> itens;

    private Long numeroPrazoPagamento;
    private String numeroRgCpfRetirada;
    
    private String observacaoNota;
    private String observacaoPedido;
    
    private String nomeOperacao;
    private BigDecimal percentutalTotalMargem;
    
    private Long qtdTotalItens;
    private String telefoneMotorista;

    private BigDecimal valorTotalDesconto;
    private BigDecimal valorTotalPrepedido;
    private Long versao;
    
    private String nomeUsuarioCadastro;
    private String dataCadastro;
    private Date dtCadastro;
//    private boolean flAcPrazoPagtoMaiorQCadastrado;
//    private boolean flAcTotalComprasMaiorLimCredit;
//    private boolean flAcDepositoIdentificado;
    private boolean flAcItensForaLimiteParamPreco;
    private Long detalheId;

}
