package br.com.assai.horus.response.dependency;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormaPagamentoDependencyResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = 1107162777594953619L;
    
    public static Long ID_CHEQUE = 2L;
    public static Long ID_BOLETO = 3L;
    
    private Long id;
    private String nome;

}
