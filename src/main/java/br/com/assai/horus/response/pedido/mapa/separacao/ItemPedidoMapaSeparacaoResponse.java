package br.com.assai.horus.response.pedido.mapa.separacao;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemPedidoMapaSeparacaoResponse implements HorusDataResponse {

	private static final long serialVersionUID = -6701423975416658102L;
	private Long idPedido;
	private Long idProduto;
	private String nomeProduto;
	private Integer quantidade;
	private BigDecimal qtdNegociada;
	private BigDecimal qtdSeparacao;
	private String statusSeparacao;
	private String embalagem;
	private String estruturaMercadologica; // categoria
	private String endereco;
	private String cliente;
	private String vendedor;
	private String telefone;
	private String inscricaoEstadual;
	private String cpfCnpj;
	private boolean clienteNidentificado;
	private String observacaoPedido;

}
