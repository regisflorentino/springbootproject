package br.com.assai.horus.response.prepedido;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.StatusPrepedidoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@ResponseConverter(StatusPrepedidoConverter.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusPrepedidoResponse implements HorusDataResponse {
	

	private static final long serialVersionUID = 3926195080265674650L;
	
	private Long id;
	private String status;
	private Date dataCadastro;
    private Long idUsuarioCadastro;
    private Date dataAlteracao;  
    private Long idUsuarioAlteracao;
    private Long idTransacao;


}
