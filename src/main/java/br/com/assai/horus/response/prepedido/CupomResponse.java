package br.com.assai.horus.response.prepedido;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.FormaPagamentoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@JsonIgnoreProperties(ignoreUnknown = true)
@ResponseConverter(FormaPagamentoConverter.class)
public class CupomResponse implements HorusDataResponse{
    
	private static final long serialVersionUID = -3160154466248303400L;
	private Long id;
    private Long codigoCupom;
    private Long numeroPdv;
    private String operador;
    private List<PagamentosResponse> pagamentosList;
    
    
    
}
