package br.com.assai.horus.response.dependency;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class PessoaDependencyResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = 618963458130695669L;
    
    private Long id;
    private String descricao;
    private String cpfCnpj;
    private String nomeRazaoSocial;
    private String nomeFantasia;
    private String sexo;
    private Date fundacaoNascimento;
    private String numeroInscricaoEstadual;
    private String numeroRG;
    private String orgaoExpedidor;
    private String uforgaoExpedidor;
    private String situacaoTributaria;
    private String cnae;
    private String emailNfe;
    private Boolean habilitadoSintegra;
    private Boolean orgaoPublico;
    private Boolean produtorRural;
    private Boolean contribuinteIcms;
    private Boolean suspensoPisCofins;
    private Boolean microEmpresa;
    private Boolean substitutoTributario;
    private Boolean contribuinteIpi;
    private String numeroSuframa;
    private String atividade;
    private Boolean cliente;
    private Boolean fornecedor;
    private Boolean transportadora;
    private Boolean motorista;
    private Date dataCadastro;
    private Long idUsuarioCadastro;
    private String nomeUsuarioCadastro;
    private Date dataAlteracao;
    private Long idUsuarioAlteracao;
    private String nomeUsuarioAlteracao;
    private Long idTransacao;
    
    
    private PessoaEnderecoDependencyResponse endereco;
    private FormaPagamentoDependencyResponse formaPagamento;
 
}
