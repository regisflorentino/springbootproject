package br.com.assai.horus.response.parametropreco;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.ParametroPrecoProdutoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ResponseConverter(ParametroPrecoProdutoConverter.class)
public class ParametroPrecoProdutoResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = -6509141344782047138L;
    
    private Long id;
    
    private Long idParamPrecoFilial;
    
    private Long idNivel;
    
    private Long idProduto;
            
    private BigDecimal precoReducao;
    
    private Boolean semReducao;
    
    private BigDecimal precoAcrescimo;
    
    private Boolean semAcrescimo;
}


