package br.com.assai.horus.response.dependency;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.annotation.MapperProperty;
import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.EstruturaMercadologicaConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ResponseConverter(EstruturaMercadologicaConverter.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EstruturaMercadologicaDependencyResponse implements HorusDataResponse {

    private static final long serialVersionUID = 3451452722496872731L;
    
    private Long id;
    private String codigo;
    private String nome;
    private String descricao;
    private Integer ordem;
    
    @MapperProperty("nivelPai.id")
    private Long idNivelPai;

}
