package br.com.assai.horus.response.parametropreco;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.ParametroPrecoFilialConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ResponseConverter(ParametroPrecoFilialConverter.class)
public class ParametroPrecoFilialResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = -5003244843165022765L;

    @Nullable
    private Long id;

    private Long idParametroPreco;
    
    private Long idFilial;
    
    private String nome;

    private BigDecimal precoReducao;

    private Boolean semReducao;

    private BigDecimal precoAcrescimo;
    
    private Boolean semAcrescimo;

    private List<ParametroPrecoProdutoResponse> produtos;
    
    private Boolean excluir;

}
