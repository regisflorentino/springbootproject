package br.com.assai.horus.response.pedido;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.converter.ResponseConverter;
import br.com.assai.horus.converter.response.PedidoConverter;
import br.com.assai.horus.dto.HorusDataResponse;
import br.com.assai.horus.entity.constant.FormaRetirada;
import br.com.assai.horus.entity.constant.TipoFaturamento;
import br.com.assai.horus.entity.constant.TipoVendaPrePedido;
import br.com.assai.horus.response.prepedido.FormaPagamentoResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString()
@ResponseConverter(PedidoConverter.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PedidoResponse implements HorusDataResponse {
	
	private static final long serialVersionUID = -1717739002731350489L;
	
	
    private Long id;
    private Long idOperacao;
    private String nomeOperacao;
    private TipoVendaPrePedido tipoVenda;
    private TipoFaturamento tipoFaturamento;
    private Boolean clienteNaoIdentificado;
    private Boolean cpfCnpjCupom;
    private String numeroCpfCnpjCupom;
    private Long idVendedor;
    public String nomeVendedor;
    private Long idCliente;
    private String cliente;
    private Long idEndereco;
    private Long idFilial;
    private Long idPrepedido;
    private String idPedidoCliente;
    private Long idStatus;
    private Long numeroPrazoPagamento;
    private Long formaRetirada;
    private Date dataPrevistaRetirada;
    private String idTransportadora;
    private String numeroRgCpfRetirada;
    private String nomeMotorista;
    private String telefoneMotorista;
    private String nomeVeiculo;
    private String placa;
    private Date dataGeracaoPedido;
    private Long qtdTotalItens;
    private BigDecimal valorTotalDesconto;
    private BigDecimal percentutalTotalMargem;
    private BigDecimal valorTotalPedido;
    private String observacaoPedido;
    private String observacaoNota;
    private String  dataCadastro;
   private Date dtCadastro; 
    private Long idUsuarioCadastro;
    private Date dataAlteracao;  
    private Long idUsuarioAlteracao;
    private Long idTransacao;
    private Long versao;
    private List<ItemPedidoResponse> itens;
    private List<FormaPagamentoResponse> formasPagamento;


    private String nomeFilial;
    private String nomeUsuarioCadastro;
    private String status;
    private Long numeroNota;
    private Long codigoCupom;
    private String uf;
    private Long idStatusSeparacao;
}
