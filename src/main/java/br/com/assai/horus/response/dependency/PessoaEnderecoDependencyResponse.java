package br.com.assai.horus.response.dependency;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class PessoaEnderecoDependencyResponse implements HorusDataResponse {
    
    private static final long serialVersionUID = -1144100767662901633L;
    
    private Long id;
    private Long idPessoa;
    private String tipoEndereco;
    private String logradouro;
    private String numeroEndereco;
    private String descricaoComplemento;
    private String nomeBairro;
    private String nomeCidade;
    private String nomePais;
    private String siglaUF;
    private String codigoIbge;
    private String numeroCep;
    private String numeroInscricaoMunicipal;  
    private Date dataCadastro;
    private Long idUsuarioCadastro;
    private String nomeUsuarioCadastro;
    private Date dataAlteracao;
    private Long idUsuarioAlteracao;
    private String nomeUsuarioAlteracao;
    private Long idTransacao;
    
    public PessoaEnderecoDependencyResponse() {
    	super();
    }
    
    public PessoaEnderecoDependencyResponse(Long id, Long idPessoa) {
        super();
        this.id =id;
        this.idPessoa = idPessoa;
    }
    
    public String getEnderecoCompleto() {
        StringBuilder sb = new StringBuilder(logradouro);
        if (numeroEndereco != null) {
           sb.append(", ").append(numeroEndereco);
        }
        if (descricaoComplemento != null) {
            sb.append(" ").append(descricaoComplemento);
        }
        if (nomeBairro != null) {
            sb.append(", ").append(nomeBairro);
        }
        if (nomeCidade != null) {
            sb.append(", ").append(nomeCidade);
        }
        if (siglaUF != null) {
            sb.append(" - ").append(siglaUF);
        }
        if (numeroCep != null) {
            sb.append(" CEP: ").append(numeroCep);
        }        
        return sb.toString();
    }

}
