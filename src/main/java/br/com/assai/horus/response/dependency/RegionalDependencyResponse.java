package br.com.assai.horus.response.dependency;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.assai.horus.dto.HorusDataResponse;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegionalDependencyResponse implements HorusDataResponse{

	private static final long serialVersionUID = 9124500548406412312L;

	private Long id;
    private String nome;  
    private Long codigo;
    private Long ordem;
    private String idAndNome;
    private List<FilialDependencyResponse> filiais;

    
}
