package br.com.assai.horus.service.formapagamento;

import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;
import br.com.assai.horus.repository.formapagamento.FormaPagamentoAnexoRepository;
import br.com.assai.horus.service.GeneralService;

public interface FormaPagamentoAnexoBusiness extends GeneralService<FormaPagamentoAnexo, FormaPagamentoAnexoRepository>{
}
