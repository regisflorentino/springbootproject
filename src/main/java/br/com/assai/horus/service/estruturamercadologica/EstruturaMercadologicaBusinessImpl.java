package br.com.assai.horus.service.estruturamercadologica;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency;
import br.com.assai.horus.repository.dependencies.EstruturaMercadologicaRepository;
import br.com.assai.horus.service.AbstractService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EstruturaMercadologicaBusinessImpl extends AbstractService<EstruturaMercadologicaDependency, EstruturaMercadologicaRepository>  implements EstruturaMercadologicaBusiness {

}
