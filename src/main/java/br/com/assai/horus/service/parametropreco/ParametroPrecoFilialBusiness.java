package br.com.assai.horus.service.parametropreco;

import java.util.List;

import br.com.assai.horus.entity.parametropreco.ParametroPrecoFilial;
import br.com.assai.horus.repository.parametropreco.ParametroPrecoFilialRepository;
import br.com.assai.horus.response.parametropreco.ParametroPrecoFilialResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoResponse;
import br.com.assai.horus.service.GeneralService;

public interface ParametroPrecoFilialBusiness extends GeneralService<ParametroPrecoFilial, ParametroPrecoFilialRepository> {
    
    List<ParametroPrecoFilialResponse> listAll();
    
    List<ParametroPrecoFilialResponse> listByParametroPreco(Long idParametroPreco);
    
    ParametroPrecoFilialResponse save(ParametroPrecoFilialResponse response);

    void removeByParametroPreco(Long idParametroPreco);

	void removeFiliais(List <ParametroPrecoFilialResponse> filiais);

}
