package br.com.assai.horus.service.prepedido;

import java.math.BigDecimal;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.ibm.icu.text.SimpleDateFormat;

import br.com.assai.horus.context.domain.annotation.TransactionalContext;
import br.com.assai.horus.contextdomain.entity.TransacaoContextDomainParentComponent;
import br.com.assai.horus.converter.response.FormaPagamentoAnexoConverter;
import br.com.assai.horus.converter.response.FormaPagamentoConverter;
import br.com.assai.horus.converter.response.ItemPrepedidoConverterMapper;
import br.com.assai.horus.converter.response.PrepedidoConverter;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.constant.TipoOrigemEnum;
import br.com.assai.horus.entity.pedido.ItemPedido;
import br.com.assai.horus.entity.pedido.Pedido;
import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;
import br.com.assai.horus.entity.prepedido.ItemPrepedido;
import br.com.assai.horus.entity.prepedido.Prepedido;
import br.com.assai.horus.entity.prepedido.StatusTelevendas;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.formapagamento.FormaPagamentoAnexoRepository;
import br.com.assai.horus.repository.formapagamento.FormaPagamentoRepository;
import br.com.assai.horus.repository.mapper.PrepedidoJdbcRepository;
import br.com.assai.horus.repository.parametropreco.ParametroPrecoRepository;
import br.com.assai.horus.repository.pedido.PedidoRepository;
import br.com.assai.horus.repository.prepedido.ItemPrepedidoRespository;
import br.com.assai.horus.repository.prepedido.PrepedidoRepository;
import br.com.assai.horus.response.prepedido.CupomResponse;
import br.com.assai.horus.response.prepedido.FormaPagamentoAnexoResponse;
import br.com.assai.horus.response.prepedido.FormaPagamentoResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;
import br.com.assai.horus.response.prepedido.LimiteCreditoClienteResponse;
import br.com.assai.horus.response.prepedido.NotaResponse;
import br.com.assai.horus.response.prepedido.PagamentosResponse;
import br.com.assai.horus.response.prepedido.PrazosPagamentoBoletoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoWorkflowResponse;
import br.com.assai.horus.rule.IdentifiableEntity;
import br.com.assai.horus.security.SecurityDataUsuarioLogado;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrepedidoBusinessImpl extends AbstractService<Prepedido, PrepedidoRepository> implements PrepedidoBusiness {
	//    private static final String CODIGODEPOSITOANTECIPADO = "4";

	private static final Long STATUS_ANDAMENTO = 1L;
	@Autowired private MybatisService mybatisService;
	@Autowired private PrepedidoConverter prepedidoConverter; 
	@Autowired private ItemPrepedidoConverterMapper itemPrepedidoConverter; 
    @Autowired private FormaPagamentoConverter formaPagamentoConverter;
    @Autowired private FormaPagamentoAnexoConverter  formaPagamentoAnexoConverter;
    @Autowired private SecurityDataUsuarioLogado securityDataUsuarioLogado;
    @Autowired private FormaPagamentoAnexoRepository formaPagamentoAxexoRepository;
    @Autowired private FormaPagamentoRepository formaPagamentoRepository;
    @Autowired private PedidoRepository pedidoRepository;
    @Autowired private ParametroPrecoRepository precoRepo;
    @Autowired private PrepedidoRepository prepedidoRepo;

    @Autowired private ItemPrepedidoRespository itemPrepedidoRespository;
    
    BigDecimal menorMargem = null;
	BigDecimal maiorMargem = null;
	Boolean semAcrescimo = null;
	Boolean semReducao = null;
    
    /**
     * Obtem id de pre-pedido
     */
    @Override
    public Long getNextId() {
        return mybatisService.getRepository(PrepedidoJdbcRepository.class).getNextId();
    }

    /* Obtem registros de pre-pedidos conforme filtro
     * @param idFilial Identificador da filial
     * @param dataInicial Data inicio
     * @param dataFinal Data fim
     * @param produtos Range de identificadores de produtos
     * @param flgExcluidos Registros excluidos ou nao
     * @return
     */
    @Override
    public List<PrepedidoResponse> findByFilter(Long idFilial, Date dataInicial, Date dataFinal, List<Long> produtos, boolean flgExcluidos) {
    	java.sql.Timestamp dataInicialTS=null;
    	java.sql.Timestamp dataFinalTS=null;
    	if(dataInicial != null) {
    		 dataInicialTS = new java.sql.Timestamp(dataInicial.getTime());
    	}
    	if(dataFinal != null) {
       	 dataFinalTS = new java.sql.Timestamp(dataFinal.getTime());
   	}
        List<PrepedidoResponse> responses = mybatisService.getRepository(PrepedidoJdbcRepository.class).findByFilter(idFilial, dataInicialTS, dataFinalTS, produtos, flgExcluidos);
        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        if(CollectionUtils.isNotEmpty(responses)) {
        	responses.forEach(index -> {
        		if(index.getTipoVendaEnum() != null) {
        			index.setTipoVenda(index.getTipoVendaEnum().getId());
        		}
        		if(index.getDtCadastro() != null) {
        			index.setDataCadastro(formatter.format(index.getDtCadastro()));
        		}
        	});
        }
        return responses;
    }
    
    @TransactionalContext
    @Override
    public PrepedidoResponse merge(PrepedidoResponse prepedidoResponse) throws MapperRegistryException {
		try {
		log.info("PrepedidoBusinessImpl  merge() -  iniciando.");
		log.info("PrepedidoResponse {} " , prepedidoResponse);
    	Optional<Prepedido> optional = getRepository().findById(prepedidoResponse.getId());
		Prepedido prepedido = prepedidoConverter.copyPropertiesResponseToModel(prepedidoResponse, optional.orElse(new Prepedido()));
		log.info("PrepedidoResponse {} " ,prepedido );

		prepedido.setFormasPagamento(null);
		setDadosContextDomain(prepedido, false);
		if (prepedido.getIdStatus() != StatusTelevendas.FINALIZADO) {
			if (prepedido.getTipoOrigem() == null) {
				prepedido.setTipoOrigem(TipoOrigemEnum.E);
			}
    	   prepedido = getRepository().save(prepedido);
    	} else {
    		
    		Long idPedido = 0l; 
    		log.info("PrepedidoBusinessImpl - merge() : Verificando prepedido ja possui IdPedido  :  "  + prepedido.getIdPedido());
    		if(prepedido.getIdPedido() == null) {
    			 idPedido = getAllocatedIdPedido();
    			prepedido.setIdPedido(idPedido);
    		}else {
    			idPedido = prepedido.getIdPedido();
    		}
    		
			if(prepedido.getDataGeracaoPedido() == null) {
				prepedido.setDataGeracaoPedido(DateTime.now().toDate());
			}
			prepedido = getRepository().save(prepedido);
			log.info("PrepedidoResponse {} new   Pedido" );
			Pedido pedido = new Pedido(idPedido, prepedido);
			log.info("PrepedidoResponse {} add itens no  Pedido" );
			pedido.setItens(convertItemPrepedidoInItemPedido(prepedidoResponse, idPedido));
			log.info("PrepedidoResponse {} save Pedido" , pedido );
			pedidoRepository.save(pedido);
			log.info("PrepedidoResponse {} set formas pagamentos" , pedido );
            for (FormaPagamentoResponse formaPagamento : prepedidoResponse.getFormasPagamento()) {
            	formaPagamento.setIdPedido(idPedido);
			}
    	}
		PrepedidoResponse savedResponseConverter = this.prepedidoConverter.convertToResponse(prepedido);
		log.info("PrepedidoResponse {} mergeItens " );
		mergeItens(prepedidoResponse, prepedido, savedResponseConverter);
		log.info("PrepedidoResponse {} mergeItens sucesso" );
		mergeFormaPagamento(prepedidoResponse, prepedido);
		log.info("PrepedidoResponse {} mergeFormaPagamento sucesso" );
		log.info("PrepedidoBusinessImpl  merge() -  finalizado com sucesso.");
		
		return savedResponseConverter;
		}catch (Exception e) {
			log.error("Error durante o  merge = " +  e.getMessage());
			throw new MapperRegistryException("Error durante o  merge "  +  e.getMessage());
		}
    }

	private void setDadosContextDomain(Prepedido prepedido, boolean isClone) {
		if(prepedido.getTransacao() == null) {
			prepedido.setTransacao(new TransacaoContextDomainParentComponent());
		}
		
		if(prepedido.getTransacao().getDataCadastro() == null) {
			prepedido.getTransacao().setDataCadastro(new Date());
		}
		
		Usuario usuario = this.securityDataUsuarioLogado.findUsuarioLogado();
		if(prepedido.getTransacao().getUsuarioCadastroId() == null || isClone) {
			Long usuarioCadastroId = usuario.getId();
			prepedido.getTransacao().setUsuarioCadastroId(usuarioCadastroId);
		}
		prepedido.getTransacao().setDataAlteracao(new Date());
		prepedido.getTransacao().setUsuarioAlteracaoId(usuario.getId());
		
	}
	
//	private Date extrairData(){
//	    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//	    Date date = new Date(sdf.format(new Date()));
//	    return date;
//	}
	
	private List<ItemPedido> convertItemPrepedidoInItemPedido(PrepedidoResponse prepedidoResponse, Long idPedido) {
		BigDecimal valorTotalPrepedido = BigDecimal.ZERO;
		BigDecimal valorTotalDesconto = BigDecimal.ZERO;
		List<ItemPedido> listItens = new ArrayList<>();
		
		if(CollectionUtils.isNotEmpty(prepedidoResponse.getItens())) {
			for (int i = 0; i < prepedidoResponse.getItens().size() ; i++) {
				ItemPrepedido itemPrepedido = this.itemPrepedidoConverter.responseToModel(prepedidoResponse.getItens().get(i));
				itemPrepedido.setNumeroItem(prepedidoResponse.getItens().size() - i);
				if(itemPrepedido.getValorTotalProduto() != null) {
					valorTotalPrepedido = valorTotalPrepedido.add(itemPrepedido.getValorTotalProduto());
				}
				if(itemPrepedido.getValorDesconto() != null) {
					valorTotalDesconto = valorTotalDesconto.add(itemPrepedido.getValorDesconto());
				}
				ItemPedido itemPedido = new ItemPedido(idPedido, itemPrepedido); 
				listItens.add(itemPedido);		
				
			}
		}
		return listItens;
	}
	
	private void mergeItens(PrepedidoResponse prepedidoResponse, Prepedido saved, PrepedidoResponse savedResponseConverter) {
		List<Long> itensId = new ArrayList<Long>();

		BigDecimal valorTotalPrepedido = BigDecimal.ZERO;
		BigDecimal valorTotalDesconto = BigDecimal.ZERO;

		if(CollectionUtils.isNotEmpty(prepedidoResponse.getItens())) {
			for (int i = 0; i < prepedidoResponse.getItens().size() ; i++) {
				ItemPrepedido itemPrepedido = this.itemPrepedidoConverter.responseToModel(prepedidoResponse.getItens().get(i));
				itemPrepedido.setNumeroItem(prepedidoResponse.getItens().size() - i);
				itemPrepedido.setIdPrepedido(saved.getId());
				if(itemPrepedido.getValorTotalProduto() != null) {
					valorTotalPrepedido = valorTotalPrepedido.add(itemPrepedido.getValorTotalProduto());
				}
//				if(itemPrepedido.getValorDesconto() != null) {
//					valorTotalDesconto = valorTotalDesconto.add(itemPrepedido.getValorDesconto());
//				}
				itensId.add(this.itemPrepedidoRespository.save(itemPrepedido).getId());
			}
		}
		
		
		List<ItemPrepedido> itens = clearDependencyModelByExampleAndValidEntityIds(this.itemPrepedidoRespository, itensId, ItemPrepedido.builder().idPrepedido(saved.getId()).build());
		if(CollectionUtils.isNotEmpty(itens)) {
			this.itemPrepedidoRespository.deleteAll(itens);
		}
		
		saved.setValorTotalPrepedido(valorTotalPrepedido);
		//saved.setValorTotalDesconto(valorTotalDesconto);
		saved.setQtdTotalItens(Long.valueOf(itensId.size()));
		this.repository.save(saved);
		
	}

	private void mergeFormaPagamento(PrepedidoResponse prepedidoResponse, Prepedido prePedido) {
		List<Long> validIds = new ArrayList<Long>();
		for(FormaPagamentoResponse index : prepedidoResponse.getFormasPagamento()) {
			Optional<FormaPagamento> optional = this.formaPagamentoRepository.findById(index.getId());
			FormaPagamento formaPagamento = optional.orElse(new FormaPagamento());
			index.setIdPrepedido(prePedido.getId());
			this.formaPagamentoConverter.copyPropertiesResponseToModel(formaPagamento, index);
			validIds.add(this.formaPagamentoRepository.save(formaPagamento).getId());
		}
		List<FormaPagamento> removidos = clearDependencyModelByExampleAndValidEntityIds(this.formaPagamentoRepository, validIds, FormaPagamento.builder().idPrepedido(prePedido.getId()).build());
		if(CollectionUtils.isNotEmpty(removidos)) {
			for (FormaPagamento formaPagamento : removidos) {
				Example<FormaPagamentoAnexo> example = Example.of(FormaPagamentoAnexo.builder().formaPagamento(formaPagamento).build(), ExampleMatcher.matchingAll());
				this.formaPagamentoAxexoRepository.deleteAll(this.formaPagamentoAxexoRepository.findAll(example));
			}
			this.formaPagamentoRepository.deleteAll(removidos);
		}
	}
	

	

	private <T extends IdentifiableEntity> List<T> clearDependencyModelByExampleAndValidEntityIds(JpaRepository<T, Long> jpaRepo, List<Long> validIds, T entity) {
		Example<T> example = Example.of(entity, ExampleMatcher.matchingAll());
		List<T> removed = jpaRepo.findAll(example);
		if(CollectionUtils.isNotEmpty(removed)) {
			removed.removeIf(index -> validIds.contains(index.getId()));
		}
		return removed;
	}
	
    
//    public Prepedido clonar(Prepedido prepedido) {
//        Usuario usuario = securityDataUsuarioLogado.findUsuarioLogado();
//        Prepedido prepedidoAtualizado = atualizarDados(prepedido);
//    	return null;
//    }
    
//    private Prepedido atualiza	

	@Override
    public void remove(Prepedido prepedido) {
    	Usuario usuario = securityDataUsuarioLogado.findUsuarioLogado();
    	if(StatusTelevendas.permiteExclusaoPrepedido(prepedido.getIdStatus())) {
    		prepedido.setIdStatus(StatusTelevendas.CANCELADO); 
    		prepedido.setDataExclusao(Calendar.getInstance().getTime());
            prepedido.setUsuarioExclusao(usuario);
    		super.merge(prepedido);
    	} else {
    		throw new RuntimeException("Status do Pré-pedido não permite remoção!");
    	}
    }
	
    public byte[] getFileInPDF(Long idFormaPagamento) {   	
    	FormaPagamentoAnexo anexo = formaPagamentoAxexoRepository.findById(idFormaPagamento).orElse(null);
    	byte[] result = null;
    	if (anexo != null) {
    		result = anexo.getArquivo();
    	}
    	return result;
    }

    @Override
    public List<FormaPagamentoResponse> getFormasPagamentoByIdPredido(Long idPrepedido) throws MapperRegistryException {
    	return formaPagamentoConverter.convertListModelToListResponse(getRepository().getFormasPagamentoByIdPedido(idPrepedido));
    }

    @Override
    public LimiteCreditoClienteResponse getLimiteCredidoIdPredido(Long idPrepedido) {
    	return mybatisService.getRepository(PrepedidoJdbcRepository.class).getLimiteCreditoByIdPrepedido(idPrepedido);
//		return limiteCredidoClienteConverter.convertListModelToListResponse(getRepository().getLimiteCredidoIdPredido(idPrepedido));
    }

    @Override
    public List<ItemPrepedidoResponse> getItensPrepedidoByIdPredido(Long idPrepedido) {
    	
        List<ItemPrepedido> itensPrepedidoByIdPredido = getRepository().getItensPrepedidoByIdPredido(idPrepedido);
        List<ItemPrepedidoResponse> response = new ArrayList<ItemPrepedidoResponse>();

        for (ItemPrepedido item : itensPrepedidoByIdPredido) {
			response.add(itemPrepedidoConverter.modelToResponse(item));
		}
        
		return response;
    }

	@Override
	public FormaPagamentoAnexoResponse saveFormaPagamentoAnexo(Long idFormaPagamento, String nomeArquivo, byte[] conteudo) throws MapperRegistryException {
		Usuario usuario = securityDataUsuarioLogado.findUsuarioLogado();
		FormaPagamento formaPagamento = formaPagamentoRepository.findById(idFormaPagamento).orElse(formaPagamentoRepository.save(FormaPagamento.builder().id(idFormaPagamento).build()));
		FormaPagamentoAnexo formaPagamentoAnexo = FormaPagamentoAnexo.builder().nomeAnexo(nomeArquivo).arquivo(conteudo).formaPagamento(formaPagamento).usuarioCadastro(usuario).build(); 
		return formaPagamentoAnexoConverter.convertToResponse(this.formaPagamentoAxexoRepository.save(formaPagamentoAnexo));
	}

	@Override
	public List<FormaPagamentoAnexoResponse> deleteFormaPagamentoAnexo(Long idFormaPagamento, List<Long> idsSelected) throws MapperRegistryException {
		FormaPagamento formaPagamento = formaPagamentoRepository.getOne(idFormaPagamento);
		
		Example<FormaPagamentoAnexo> example = Example.of(FormaPagamentoAnexo.builder().formaPagamento(formaPagamento).build(), ExampleMatcher.matchingAll());
		List<FormaPagamentoAnexo> formasPagamento = this.formaPagamentoAxexoRepository.findAll(example);
		formasPagamento.forEach(f -> {
			formaPagamentoAxexoRepository.delete(f);
		});
		return formaPagamentoAnexoConverter.convertListModelToListResponse(formasPagamento);
	}


	@Override
	public ItemPrepedidoResponse verificaRangeMargem(Long idFilial, ItemPrepedidoResponse itemPrepedido) {
			this.menorMargem = null;
			this.maiorMargem = null;
			this.semAcrescimo = null;
			this.semReducao = null;
			String pathProd = mybatisService.getRepository(PrepedidoJdbcRepository.class).getPathProd(itemPrepedido.getProduto().getId());
				pathProd = pathProd.substring(1);
				pathProd = pathProd + "@" + itemPrepedido.getProduto().getId();
				String[] splited = pathProd.split("@");
				List<String> asList = Arrays.asList(splited);
				asList.sort(Comparator.reverseOrder());
				for (String id : asList) {
					ParametroPrecoRepository.DadosPreco paramPreco = precoRepo.findByIdProdORIdEM(Long.parseLong(id), idFilial);
					if(paramPreco != null) {
						populaDadosDesconto(paramPreco);
					}
				}
				populaDadosDesconto(precoRepo.findParanFilialByIdFilial(idFilial));
				itemPrepedido.setMaxPrecoRange(maiorMargem);
				itemPrepedido.setMinPrecoRange(menorMargem);
				itemPrepedido.setIsPermiteAcrescimo(semAcrescimo);
				itemPrepedido.setIsPermiteReducao(semReducao);
			return itemPrepedido;
	}
	
	private void populaDadosDesconto( ParametroPrecoRepository.DadosPreco paramPreco) {
		if(paramPreco != null) {
			if (this.menorMargem == null && paramPreco.getPrecoReducao() != null && this.semReducao == null) {
				this.menorMargem  = paramPreco.getPrecoReducao();
				System.out.println(this.menorMargem);
			}
			if (this.maiorMargem == null && paramPreco.getPrecoAcrescimo() != null && this.semAcrescimo == null) {
				this.maiorMargem  = paramPreco.getPrecoAcrescimo();
				System.out.println(this.maiorMargem);
			}
			if (this.semAcrescimo == null && paramPreco.getSemReducao() != null) {
				this.semAcrescimo = paramPreco.getSemReducao();
				System.out.println(this.semAcrescimo);
			}
			if (this.semReducao == null && paramPreco.getSemAcrescimo() != null) {
				this.semReducao = paramPreco.getSemAcrescimo();
				System.out.println(this.semReducao);
			}
		}
	}
	
	@Override
	public List<FormaPagamentoAnexo> getFormasPagamentoAnexoByIdFormaPagamento(long id) {
		return getRepository().getFormasPagamentoAnexoByIdFormaPagamento(id);
	}

	@Override
	public Long getItemNextId() {
        return mybatisService.getRepository(PrepedidoJdbcRepository.class).getItemNextId();

	}
    
	@Override
    public Long countByFilter(Long idFilial, Date dataInicial, Date dataFinal, List<Long> produtos, boolean flgExcluidos) {
        return mybatisService.getRepository(PrepedidoJdbcRepository.class).countByFilter(idFilial, dataInicial, dataFinal, produtos, flgExcluidos);
    }

	@Override
	public Prepedido clonePrePedido(Long id, Long vendedorId) {
		Optional<Prepedido> optionalPrePedido = this.findById(id);
		Prepedido prepedido = this.detach(optionalPrePedido.orElseThrow(() -> new RuntimeException("Erro ao obter Prepedido")));
		
		List<ItemPrepedido> itens = this.itemPrepedidoRespository.findAll(Example.of(ItemPrepedido.builder().idPrepedido(prepedido.getId()).build(), ExampleMatcher.matchingAll()));
		
		prepedido.setId(this.getNextId());
		prepedido.setIdVendedor(vendedorId);
		prepedido.setObservacaoNota(null);
		prepedido.setObservacaoPedido(null);
		prepedido.setDataGeracaoPedido(null);
		prepedido.getTransacao().setDataCadastro(new Date());
		prepedido.setIdStatus(STATUS_ANDAMENTO);
		setDadosContextDomain(prepedido, true);
		prepedido.setDetalhe(null);
		
		Prepedido saved = this.repository.save(prepedido);
		
		for (ItemPrepedido itemPrepedido : itens) {
			this.itemPrepedidoRespository.save(ItemPrepedido.builder()
				.id(this.getItemNextId())
				.idPrepedido(prepedido.getId())
				.idEmbalagem(itemPrepedido.getIdEmbalagem())
				.idProduto(itemPrepedido.getIdProduto())
				.qtdNegociada(itemPrepedido.getQtdNegociada())
				.idProdutoFilialPreco(itemPrepedido.getIdProdutoFilialPreco()).build());
		}
		
		return saved;
	}

	@Override
	public Long getBloqueioPrecoGerente(Long idFilial, Long idProd) {
		return mybatisService.getRepository(PrepedidoJdbcRepository.class).getBloqueioPrecoGerente(idFilial, idProd);
	}

	@Override
	public void removeAllPrepedidos(List<Long> ids) {
		List<Prepedido> list = this.findByIds(ids);
		if(list.size() < 1) {return;} 
		if(CollectionUtils.isNotEmpty(list)) {
			for(int i =0 ; i < list.size(); i++) {
				if(StatusTelevendas.FINALIZADO.equals(list.get(i))) {
					list.remove(i);
				}
				List<FormaPagamento> formaPgto = formaPagamentoRepository.findByIdPrepedido(list.get(i).getId());
				List<ItemPrepedido> itens = itemPrepedidoRespository.findByidPrepedido(list.get(i).getId());
				deleteFormasPagamento(formaPgto);
				deleteItemPrepedido(itens);
			}
			this.removeAll(list);
		}
	}
	
	@Override
	public void cancelAllPrepedidos(List<Long> ids) {
		List<Prepedido> list = this.findByIds(ids);
		if(list.size() < 1) {return;} 
		if(CollectionUtils.isNotEmpty(list)) {
			for(int i =0 ; i < list.size(); i++) {
				if(!StatusTelevendas.FINALIZADO.equals(list.get(i))) {
					Optional<Prepedido> optional = this.findById(list.get(i).getId());
			    	optional.get().setIdStatus(2L);
			    	optional.get().setDataExclusao(new Date());
			    	this.merge(optional.get());
				}
			}
		}
	}
	
	private void  deleteFormasPagamento(List<FormaPagamento> list) {
		if (list.size() < 1) {return;}
		list.forEach(f -> {
			List<FormaPagamentoAnexo> findByFormaPagamento = formaPagamentoAxexoRepository.findByFormaPagamento(f);
			if(findByFormaPagamento.size() > 0) {
				findByFormaPagamento.forEach(fa -> {
					formaPagamentoAxexoRepository.delete(fa);
				});
				formaPagamentoRepository.delete(f);
			}
		});
	}
	
	private void deleteItemPrepedido(List<ItemPrepedido> list) {
		if (list.size() > 0) {
			list.forEach(i -> {
				itemPrepedidoRespository.delete(i);
			});
		}
	}

	@Override
	public List<CupomResponse> findFaturamentosCupom(Long idPedidoCapa) {
		List<CupomResponse> cupomResponse =  mybatisService.getRepository(PrepedidoJdbcRepository.class).findFaturamentosCupom(idPedidoCapa);
		if(cupomResponse.size() > 0 ) {
			cupomResponse.forEach( c -> {
				c.setPagamentosList(findPagamentos(idPedidoCapa)); 
			});
			return cupomResponse;
		} else {
			return null;
		}
	}
	
	@Override
	public NotaResponse  findFaturamentosNota(Long idPedidoCapa) {
		NotaResponse notaResponse  = mybatisService.getRepository(PrepedidoJdbcRepository.class).findFaturamentosNota(idPedidoCapa);
		if(notaResponse != null) {
			notaResponse.setPagamentosList(findPagamentos(idPedidoCapa));
			return notaResponse;
		} else {
			return null;
		}
	}
	
	@Override
	public List<PagamentosResponse> findPagamentos(Long idPedidoCapa) {
		List<PagamentosResponse> findPagamentos = mybatisService.getRepository(PrepedidoJdbcRepository.class).findPagamentos(idPedidoCapa);
		if(findPagamentos.size() > 0) {
			return findPagamentos; 
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<PrazosPagamentoBoletoResponse> getPrazosPagamentoBoleto() {
		List<PrazosPagamentoBoletoResponse> prazoPagamentos = mybatisService.getRepository(PrepedidoJdbcRepository.class).getPrazosPagamentoBoleto();
		if(prazoPagamentos.size() > 0) {
			return prazoPagamentos; 
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public void flush() {
	}
	
	@Override
	public void cancelarPrepedido(Long id) {
		getRepository().cancelarPrepedido(id);
	}
	
	@Override
	public void reprovarPrepedido(Long id, Long idStatus) {
		getRepository().reprovarPrepedido(id, idStatus);
	}

	@Override
	public List<PrepedidoWorkflowResponse> getPrepedidoWorkflows(String status) {
		List<PrepedidoWorkflowResponse> prepedidoWorkflowsList =  mybatisService.getRepository(PrepedidoJdbcRepository.class).getPrepedidoWorkflows(status);
		Format formatter = new SimpleDateFormat("dd/MM/yyyy");
		for (PrepedidoWorkflowResponse prepedidoWorkflowResponse : prepedidoWorkflowsList) {
			if(prepedidoWorkflowResponse.getDtAlteracao() !=  null) {
				prepedidoWorkflowResponse.setDataAlteracao(formatter.format(prepedidoWorkflowResponse.getDtAlteracao()));
			 }	
			if(prepedidoWorkflowResponse.getDetalhewf() == null) {
				List<Long>  idsWorkflowDetalhe = this.getIdWkfDetalhePrepedido(prepedidoWorkflowResponse.getIdPrepedido());
				this.atualizaPrepedidoComIdWkfDetalhe(prepedidoWorkflowResponse.getIdPrepedido(), idsWorkflowDetalhe.get(idsWorkflowDetalhe.size()-1));
				prepedidoWorkflowResponse.setDetalhewf(idsWorkflowDetalhe.get(idsWorkflowDetalhe.size()-1));
			}
			if(prepedidoWorkflowResponse.getStatus().equalsIgnoreCase("ANDAMENTO")) {
				prepedidoWorkflowResponse.setIdPedido(null);
			}
		}
		return prepedidoWorkflowsList;
	}

	@Override
	public List<PrepedidoRepository.ResumoPedido> getPedidosByIdCliente(long idCliente) {
		return getRepository().getPedidosByIdCliente(idCliente);
	}
	
	public Long getAllocatedIdPedido() {
			Date date = DateTime.now().toDate();
			mybatisService.getRepository(PrepedidoJdbcRepository.class).alloc(date);
			Long sequenceValue = mybatisService.getRepository(PrepedidoJdbcRepository.class).getSequenceValue(date);
			return sequenceValue;
		}
	
	public void  deleteAllocatedIdPedido(long idPedido) {
		 mybatisService.getRepository(PrepedidoJdbcRepository.class).deleteSequenceValue(idPedido);	
	}
	   
	public void  atualizaPrepedidoComIdWkfDetalhe(long IdPrePedido , long idWorkflowDetalhe) {
		Optional<Prepedido> prepedido = prepedidoRepo.findById(IdPrePedido);
		prepedido.get().setDetalhe(idWorkflowDetalhe);
		prepedidoRepo.save(prepedido.get());
	}
	
	public List<Long> getIdWkfDetalhePrepedido(long idPrepedido) {
		return mybatisService.getRepository(PrepedidoJdbcRepository.class).getIdWkfDetalhePrepedido(idPrepedido);
	}

	@Override
	public String getNomeVendedor(Long idVendedor) {
		return mybatisService.getRepository(PrepedidoJdbcRepository.class).getNomeVendedor(idVendedor);
	}
	
    @Override
    public   void updateStatusWorkflow(Long idWorkflow, Long idStatus) {
    	String status = mybatisService.getRepository(PrepedidoJdbcRepository.class).getStatusTelevendasById(idStatus);
    	mybatisService.getRepository(PrepedidoJdbcRepository.class).updateStatusWorkflow(idWorkflow  , status );
    }

	@Override
	public void updatePrepedido(Long idPrepedido, Long idPedido) {
    	mybatisService.getRepository(PrepedidoJdbcRepository.class).updatePrepedido(idPrepedido  , idPedido );
	}
	
	@Override
	public Long getIdPedidoByIdPrepedido(Long idPrepedido) {
        return mybatisService.getRepository(PrepedidoJdbcRepository.class).getIdPedidoByIdPrepedido(idPrepedido);

	}

	@Override
	public int getPeriodoValidadePedidoParam() {
		final int  VALIDADE_PEDIDO_TVD_PARAM  = 626;
		return mybatisService.getRepository(PrepedidoJdbcRepository.class).getPeriodoValidadePedidoParam(VALIDADE_PEDIDO_TVD_PARAM);
	}

	
	 /* Obtem registros de pre-pedidos para atualizacao da lista de prepedido
     * @param idFilial Identificador da filial
     * @param idPrepedidoLast  ultimo idprepedido exibido para o usuario
     * @return
     */
    @Override
    public List<PrepedidoResponse> prepedidosRefresh(Long idFilial,  Long idPrepedidoLast) {
        List<PrepedidoResponse> responses = mybatisService.getRepository(PrepedidoJdbcRepository.class).prepedidosRefresh(idFilial , idPrepedidoLast);
        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        if(CollectionUtils.isNotEmpty(responses)) {
        	responses.forEach(index -> {
        		if(index.getTipoVendaEnum() != null) {
        			index.setTipoVenda(index.getTipoVendaEnum().getId());
        		}
        		if(index.getDtCadastro() != null) {
        			index.setDataCadastro(formatter.format(index.getDtCadastro()));
        		}
        	});
        }
        return responses;
    }
}