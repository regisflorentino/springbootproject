package br.com.assai.horus.service.formapagamento;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;
import br.com.assai.horus.repository.formapagamento.FormaPagamentoAnexoRepository;
import br.com.assai.horus.service.AbstractService;

@Service(value="FormaPagamentoBusiness")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FormaPagamentoAnexoBusinessImpl extends AbstractService<FormaPagamentoAnexo, FormaPagamentoAnexoRepository> implements FormaPagamentoAnexoBusiness {
	
}
