package br.com.assai.horus.service.formapagamento;

import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.repository.formapagamento.FormaPagamentoRepository;
import br.com.assai.horus.service.GeneralService;

public interface FormaPagamentoBusiness extends GeneralService<FormaPagamento, FormaPagamentoRepository>{

	Long nextValSequence();
    
}
