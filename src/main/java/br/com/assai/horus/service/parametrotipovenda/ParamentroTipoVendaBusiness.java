package br.com.assai.horus.service.parametrotipovenda;

import java.util.List;

import br.com.assai.horus.entity.ParamentroTipoVenda;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.parametrotipovenda.ParamentroTipoVendaRepository;
import br.com.assai.horus.response.ParamentroTipoVendaResponse;
import br.com.assai.horus.service.GeneralService;

public interface ParamentroTipoVendaBusiness extends GeneralService<ParamentroTipoVenda, ParamentroTipoVendaRepository> {
	
	List<ParamentroTipoVendaResponse> listAll(); 
	void saveAll(List<ParamentroTipoVendaResponse> list) throws MapperRegistryException ;
	ParamentroTipoVendaResponse findByIdFilial(Long idFilial) throws MapperRegistryException;
}
