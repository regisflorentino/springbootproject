package br.com.assai.horus.service.prepedido;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;
import br.com.assai.horus.entity.prepedido.Prepedido;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.prepedido.PrepedidoRepository;
import br.com.assai.horus.response.prepedido.CupomResponse;
import br.com.assai.horus.response.prepedido.FormaPagamentoAnexoResponse;
import br.com.assai.horus.response.prepedido.FormaPagamentoResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;
import br.com.assai.horus.response.prepedido.LimiteCreditoClienteResponse;
import br.com.assai.horus.response.prepedido.NotaResponse;
import br.com.assai.horus.response.prepedido.PagamentosResponse;
import br.com.assai.horus.response.prepedido.PrazosPagamentoBoletoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoWorkflowResponse;
import br.com.assai.horus.service.GeneralService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PrepedidoBusiness extends GeneralService<Prepedido, PrepedidoRepository> {
 
    Long getNextId();
    public List<PrepedidoResponse> findByFilter(Long idFilial, Date dataInicial, Date dataFinal, List<Long> produtos, boolean flgExcluidos);
    ItemPrepedidoResponse verificaRangeMargem(Long idFilial, ItemPrepedidoResponse itemPrepedido);
    List<FormaPagamentoResponse> getFormasPagamentoByIdPredido(Long idPrepedido) throws MapperRegistryException;
    List<FormaPagamentoAnexo> getFormasPagamentoAnexoByIdFormaPagamento(@Param("id") long id);
    LimiteCreditoClienteResponse getLimiteCredidoIdPredido(Long idPrepedido);
    List<ItemPrepedidoResponse> getItensPrepedidoByIdPredido(Long idPrepedido);
    List<PrepedidoRepository.ResumoPedido> getPedidosByIdCliente(long idCliente);
    byte[] getFileInPDF(Long idFormaPagamento);
    FormaPagamentoAnexoResponse saveFormaPagamentoAnexo(Long idFormaPagamento, String nomeArquivo, byte[] conteudo) throws MapperRegistryException;
    void remove(Prepedido prepedido);
//    Prepedido clonar(Prepedido prepedido);
    List<FormaPagamentoAnexoResponse> deleteFormaPagamentoAnexo(Long idFormaPagamento, List<Long> idsSelected) throws MapperRegistryException;
	PrepedidoResponse merge(PrepedidoResponse prepedidoResponse) throws MapperRegistryException;
    Long getItemNextId();
    Long countByFilter(Long idFilial, Date dataInicial, Date dataFinal, List<Long> produtos, boolean flgExcluidos);
    Prepedido clonePrePedido(Long id, Long vendedorId);
    Long getBloqueioPrecoGerente(Long idFilial, Long idProd);
	void removeAllPrepedidos(List<Long> ids);
	List<CupomResponse> findFaturamentosCupom(Long idPedidoCapa);
	NotaResponse  findFaturamentosNota(Long idPedidoCapa);
	List<PagamentosResponse> findPagamentos(Long idPedidoCapa);
	List<PrazosPagamentoBoletoResponse> getPrazosPagamentoBoleto();
	List<PrepedidoWorkflowResponse> getPrepedidoWorkflows(String status);
	void flush();
	void cancelarPrepedido(Long id);
	void reprovarPrepedido(Long id, Long idStatus);
	void cancelAllPrepedidos(List<Long> ids);
	String getNomeVendedor(Long idVendedor);
	 Long getAllocatedIdPedido();
	void updateStatusWorkflow(Long idWorkflow, Long idStatus); 
	void updatePrepedido(Long idPrepedido, Long idPedido);
	Long getIdPedidoByIdPrepedido(Long idPrepedido);
	int getPeriodoValidadePedidoParam();
	List<PrepedidoResponse> prepedidosRefresh(Long idFilial,  Long idPrepedidoLast);
	void deleteAllocatedIdPedido(long idPedido); 
}
