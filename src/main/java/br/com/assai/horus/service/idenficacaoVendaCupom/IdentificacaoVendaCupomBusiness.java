package br.com.assai.horus.service.idenficacaoVendaCupom;

import java.util.List;

import br.com.assai.horus.entity.IdentificacaoVendaCupom;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.identificacaoVendaCupom.IdentificacaoVendaCupomRepository;
import br.com.assai.horus.response.IdentificacaoVendaCupomResponse;
import br.com.assai.horus.service.GeneralService;

public interface IdentificacaoVendaCupomBusiness extends GeneralService<IdentificacaoVendaCupom, IdentificacaoVendaCupomRepository> {
    
    List<IdentificacaoVendaCupomResponse> listAll();
    IdentificacaoVendaCupomResponse findByFilial(Long idFilial);
    void mergeList(List<IdentificacaoVendaCupomResponse> values) throws MapperRegistryException;

}
