package br.com.assai.horus.service.parametropreco;

import java.util.List;

import br.com.assai.horus.entity.parametropreco.ParametroPrecoProduto;
import br.com.assai.horus.repository.parametropreco.ParametroPrecoProdutoRepository;
import br.com.assai.horus.response.parametropreco.ParametroPrecoProdutoResponse;
import br.com.assai.horus.service.GeneralService;

public interface ParametroPrecoProdutoBusiness extends GeneralService<ParametroPrecoProduto, ParametroPrecoProdutoRepository> {
    
    List<ParametroPrecoProdutoResponse> listByParametroPrecoFilial(Long idParametroPrecoFilial);
    
    ParametroPrecoProdutoResponse save(ParametroPrecoProdutoResponse response);

    void removeByParametroPrecoFilial(Long idParametroPrecoFilial);
}
