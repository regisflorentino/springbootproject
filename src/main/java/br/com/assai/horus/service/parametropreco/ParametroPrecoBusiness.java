package br.com.assai.horus.service.parametropreco;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.assai.horus.entity.parametropreco.ParametroPreco;
import br.com.assai.horus.repository.parametropreco.ParametroPrecoRepository;
import br.com.assai.horus.response.parametropreco.ParametroPrecoFilialResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoProdutoResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoResponse;
import br.com.assai.horus.service.GeneralService;

public interface ParametroPrecoBusiness extends GeneralService<ParametroPreco, ParametroPrecoRepository> {
    
    List<ParametroPrecoResponse> listAll();
    List<ParametroPrecoFilialResponse> listParametroPrecoFiliais(Long idParametroPreco);
    List<ParametroPrecoFilialResponse> verifyParametroPrecoFilial(Long idFilial);
    List<ParametroPrecoProdutoResponse> listParametroPrecoProdutos(Long idParametroPrecoFilial);
    List<ParametroPrecoProdutoResponse> listParametroPrecoProdutosByIdProd(Long idFilial, Long idProd);
    void delete(List<Long> ids);
	ParametroPrecoResponse save(ParametroPrecoResponse entity);
	public ParametroPrecoRepository.DadosPreco  findByIdProdORIdEM(@Param("idProd") Long idProd, Long idFilial);
}
