package br.com.assai.horus.service.parametrotipovenda;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.response.ParamentroTipoVendaConverter;
import br.com.assai.horus.entity.ParamentroTipoVenda;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.mapper.ParamentroTipoVendaJdbcRepository;
import br.com.assai.horus.repository.parametrotipovenda.ParamentroTipoVendaRepository;
import br.com.assai.horus.response.ParamentroTipoVendaResponse;
import br.com.assai.horus.security.SecurityDataUsuarioLogado;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParamentroTipoVendaBusinessImpl extends AbstractService<ParamentroTipoVenda, ParamentroTipoVendaRepository> implements ParamentroTipoVendaBusiness{

	@Autowired 
	private SecurityDataUsuarioLogado securityDataUsuarioLogado;
	
	@Autowired
	private ParamentroTipoVendaConverter converter;
	
	@Autowired
	private MybatisService mybatisService;
	
	@Autowired
	private ParamentroTipoVendaRepository paramentroRepository;
	
	@Override
	public List<ParamentroTipoVendaResponse> listAll() {
		return mybatisService.getRepository(ParamentroTipoVendaJdbcRepository.class).listAll();
	}
	
	@Override
	public void saveAll(List<ParamentroTipoVendaResponse> list) throws MapperRegistryException {
		List<ParamentroTipoVenda> listaModel = new ArrayList<>();
		Usuario usuario = securityDataUsuarioLogado.findUsuarioLogado();
		for (ParamentroTipoVendaResponse response : list) {
			ParamentroTipoVenda model = converter.convertToModel(response);
	        if (model.isNew()) {
	        	model.setDataCadastro(Calendar.getInstance().getTime());
	        	model.setUsuarioCadastro(usuario);
	        }
	        model.setDataAlteracao(Calendar.getInstance().getTime());
	        model.setUsuarioAlteracao(usuario);
			listaModel.add(model);
			
		}
		super.mergeAll(listaModel);
	}
	
	@Override
    public ParamentroTipoVenda merge(ParamentroTipoVenda entity){
        Usuario usuario = securityDataUsuarioLogado.findUsuarioLogado();
        if (entity.isNew()) {
            entity.setDataCadastro(Calendar.getInstance().getTime());
            entity.setUsuarioCadastro(usuario);
        }
        entity.setDataAlteracao(Calendar.getInstance().getTime());
        entity.setUsuarioAlteracao(usuario);
        return super.merge(entity);
    }

	@Override
	public ParamentroTipoVendaResponse findByIdFilial(Long idFilial) throws MapperRegistryException {
		return converter.convertToResponse(getRepository().findByIdFilial(idFilial));
	}
	
}
