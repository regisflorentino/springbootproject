package br.com.assai.horus.service.parametropreco;

import java.util.List;
import java.util.Optional;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.response.ParametroPrecoProdutoConverter;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.parametropreco.ParametroPrecoProduto;
import br.com.assai.horus.repository.mapper.ParametroPrecoJdbcRepository;
import br.com.assai.horus.repository.parametropreco.ParametroPrecoProdutoRepository;
import br.com.assai.horus.response.parametropreco.ParametroPrecoProdutoResponse;
import br.com.assai.horus.security.SecurityDataUsuarioLogado;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParametroPrecoProdutoBusinessImpl extends AbstractService<ParametroPrecoProduto, ParametroPrecoProdutoRepository>  implements ParametroPrecoProdutoBusiness {

    @Autowired 
	private SecurityDataUsuarioLogado securityDataUsuarioLogado;

    @Autowired 
    private MybatisService mybatisService; 

    @Autowired
    private ParametroPrecoProdutoConverter parametroPrecoProdutoConverter;

    /**
     * Lista todos parametros de preco produto de um parametro preco filial
     * @param idParametroPrecoFilial Identificador do parametro de preco filial
     * @return Lista de parametro de preco produto
     */
    public List<ParametroPrecoProdutoResponse> listByParametroPrecoFilial(Long idParametroPrecoFilial) {
    	return mybatisService.getRepository(ParametroPrecoJdbcRepository.class).listParametroPrecoProdutos(idParametroPrecoFilial);
    }


    /**
     * Salva um parametro de preco produto
     * @param response Parametro preco produto response
     */
    @Override
	public ParametroPrecoProdutoResponse save(ParametroPrecoProdutoResponse response) {
        ParametroPrecoProduto parametroPrecoProduto = parametroPrecoProdutoConverter.convertToModel(response);

        return parametroPrecoProdutoConverter.convertToResponse(super.merge(parametroPrecoProduto));
    }
    
    /**
     * Remove todos parametros de preco produto de um parametro preco filial
     * @param idParametroPrecoFilial Identificador do parametro preco da filial
     */
    public void removeByParametroPrecoFilial(Long idParametroPrecoFilial) {
        List<ParametroPrecoProdutoResponse> parametrosPrecoProdutos = this.listByParametroPrecoFilial(idParametroPrecoFilial);

        if (parametrosPrecoProdutos != null) {
            for (ParametroPrecoProdutoResponse response : parametrosPrecoProdutos) {
                Optional<ParametroPrecoProduto> parametroPrecoProduto = getRepository().findById(response.getId());
                parametroPrecoProduto.ifPresent(p -> {
                    super.remove(p);
                });
            }
        }
    }
}
