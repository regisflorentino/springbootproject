package br.com.assai.horus.service.formapagamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.repository.formapagamento.FormaPagamentoRepository;
import br.com.assai.horus.repository.mapper.PrepedidoJdbcRepository;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FormaPagamentoBusinessImpl extends AbstractService<FormaPagamento, FormaPagamentoRepository> implements FormaPagamentoBusiness {

	@Autowired
	private MybatisService mybatisService;
	
	@Override
	public Long nextValSequence() {
        return mybatisService.getRepository(PrepedidoJdbcRepository.class).getFormaPagamentoNextId();
	}
	
}
