package br.com.assai.horus.service.parametropreco;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.response.ParametroPrecoConverter;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.parametropreco.ParametroPreco;
import br.com.assai.horus.repository.mapper.ParametroPrecoJdbcRepository;
import br.com.assai.horus.repository.parametropreco.ParametroPrecoRepository;
import br.com.assai.horus.response.parametropreco.ParametroPrecoFilialResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoProdutoResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoResponse;
import br.com.assai.horus.security.SecurityDataUsuarioLogado;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParametroPrecoBusinessImpl extends AbstractService<ParametroPreco, ParametroPrecoRepository>
		implements ParametroPrecoBusiness {

	@Autowired
	private SecurityDataUsuarioLogado securityDataUsuarioLogado;

	@Autowired
	private MybatisService mybatisService;

	@Autowired
	private ParametroPrecoConverter parametroPrecoConverter;

	@Autowired
	private ParametroPrecoFilialBusiness parametroPrecoFilialBusiness;

	/**
	 * Lista todos parametros de preco
	 * 
	 * @return Lista de parametro de preco
	 */
	public List<ParametroPrecoResponse> listAll() {
		List<ParametroPrecoResponse> parametrosPreco = mybatisService.getRepository(ParametroPrecoJdbcRepository.class)
				.listAll();

		if (parametrosPreco != null) {
			for (ParametroPrecoResponse parametroPreco : parametrosPreco) {
				parametroPreco.setFiliais(this.listParametroPrecoFiliais(parametroPreco.getId()));
			}
		}

		return parametrosPreco;
	}

	/**
	 * Lista os parametro preco filial de um parametro de preco
	 * 
	 * @param idParametroPreco Identificador do parametro de preco
	 * @return Lista de parametro de preco filial
	 */
	public List<ParametroPrecoFilialResponse> listParametroPrecoFiliais(Long idParametroPreco) {
		if (idParametroPreco != null) {
			return this.parametroPrecoFilialBusiness.listByParametroPreco(idParametroPreco);
		} else {
			return this.parametroPrecoFilialBusiness.listAll();
		}
	}

	/**
	 * Lista os parametro preco filial de uma filial
	 * 
	 * @param idFilial Identificador da filial
	 * @return Lista de parametro de preco filial
	 */
	public List<ParametroPrecoFilialResponse> verifyParametroPrecoFilial(Long idFilial) {
		return mybatisService.getRepository(ParametroPrecoJdbcRepository.class).verififyParametroPrecoFilial(idFilial);
	}

	/**
	 * Lista os parametro preco produto associados a um parametro preco filial
	 * 
	 * @param idParametroPrecoFilial Identificador do parametro preco filial
	 * @return Lista de parametro preco produto
	 */
	public List<ParametroPrecoProdutoResponse> listParametroPrecoProdutos(Long idParametroPrecoFilial) {
		return mybatisService.getRepository(ParametroPrecoJdbcRepository.class)
				.listParametroPrecoProdutos(idParametroPrecoFilial);
	}
	
	@Override
	public List<ParametroPrecoProdutoResponse> listParametroPrecoProdutosByIdProd(Long idFilial, Long idProd) {
		return mybatisService.getRepository(ParametroPrecoJdbcRepository.class)
				.listParametroPrecoProdutosByIdProdIdFilial(idFilial, idProd);
	}

	@Override
	public void delete(List<Long> ids) {
		if (ids == null || ids.isEmpty())
			return;

		for (Long id : ids) {
			Optional<ParametroPreco> parametroPreco = getRepository().findById(id);

			parametroPreco.ifPresent(p -> {
				parametroPrecoFilialBusiness.removeByParametroPreco(p.getId());
				super.remove(p);
			});
		}
	}

	@Override
	public ParametroPrecoRepository.DadosPreco findByIdProdORIdEM(Long idProd, Long idFilial) {
		return getRepository().findByIdProdORIdEM(idProd, idFilial);
	}
	
	@Override
	public ParametroPrecoResponse save(ParametroPrecoResponse response) {
		ParametroPreco parametroPreco = parametroPrecoConverter.convertToModel(response);

		boolean isNomeValido = isNomeValido(response.getNomeGrupo());
			
		//Edição
		if(nonNull(response.getId()) || (isNull(response.getId()) && isNomeValido)) {
			return persistirParametroPreco(response, parametroPreco);
		}
		
		return null;
	}

	private ParametroPrecoResponse persistirParametroPreco(ParametroPrecoResponse response, ParametroPreco parametroPreco) {
		Usuario usuario = securityDataUsuarioLogado.findUsuarioLogado();
		if (parametroPreco.isNew()) {
			parametroPreco.setDataCadastro(Calendar.getInstance().getTime());
			parametroPreco.setUsuarioCadastro(usuario);
		}

		parametroPreco.setDataAlteracao(Calendar.getInstance().getTime());
		parametroPreco.setUsuarioAlteracao(usuario);
		parametroPreco.setIdTransacao(1L);

		super.merge(parametroPreco);

		// remove as filiais e produtos associados
		this.parametroPrecoFilialBusiness.removeFiliais(response.getFiliais());

		this.salvarParametrosFiliais(parametroPreco.getId(), response.getFiliais());
		return parametroPrecoConverter.convertToResponse(super.merge(parametroPreco));
	}

	/**
	 * Salva parametro preco filiais
	 * 
	 * @param idParametroPreco Identificador do parametro preco
	 * @param filiais          Lista de parametro preco filiais
	 */
	private void salvarParametrosFiliais(Long idParametroPreco, List<ParametroPrecoFilialResponse> filiais) {
		if (filiais == null)
			return;
		
		Iterator<ParametroPrecoFilialResponse> iterator = filiais.iterator();
		while(iterator.hasNext()) {
			if(iterator.next().getExcluir() != null) {
				iterator.remove();
			}
		}
		
		if(filiais != null) {
			filiais.forEach(x -> {
				x.setIdParametroPreco(idParametroPreco);
				x.setId(parametroPrecoFilialBusiness.save(x).getId());
			});
		};
	}

	private boolean isNomeValido(String nomeGrupo ) {
		if (nomeGrupo != null) {
			return !nomeGrupo.equals(mybatisService.getRepository(ParametroPrecoJdbcRepository.class).checkNomeGrupo(nomeGrupo));
		} else {
			return false;
		}
	}

}
