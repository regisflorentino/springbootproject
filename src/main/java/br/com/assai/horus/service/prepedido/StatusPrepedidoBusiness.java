package br.com.assai.horus.service.prepedido;

import br.com.assai.horus.entity.prepedido.StatusTelevendas;
import br.com.assai.horus.repository.prepedido.StatusPrepedidoRepository;
import br.com.assai.horus.service.GeneralService;

public interface StatusPrepedidoBusiness extends GeneralService<StatusTelevendas, StatusPrepedidoRepository> {

}
