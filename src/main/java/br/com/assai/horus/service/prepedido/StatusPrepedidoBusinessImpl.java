package br.com.assai.horus.service.prepedido;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.prepedido.StatusTelevendas;
import br.com.assai.horus.repository.prepedido.StatusPrepedidoRepository;
import br.com.assai.horus.service.AbstractService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StatusPrepedidoBusinessImpl extends AbstractService<StatusTelevendas, StatusPrepedidoRepository>  implements StatusPrepedidoBusiness {

}
