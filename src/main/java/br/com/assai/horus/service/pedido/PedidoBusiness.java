package br.com.assai.horus.service.pedido;

import java.util.Date;
import java.util.List;

import br.com.assai.horus.entity.pedido.Pedido;
import br.com.assai.horus.entity.prepedido.Prepedido;
import br.com.assai.horus.repository.pedido.PedidoRepository;
import br.com.assai.horus.response.pedido.PedidoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.service.GeneralService;

public interface PedidoBusiness extends GeneralService<Pedido, PedidoRepository> {
	
	List<PedidoResponse> findByFilter(Long idFilial, Date dataInicial, Date dataFinal, List<Long> produtos, boolean flgExcluidos);

	Pedido createPedido(Prepedido entity, PrepedidoResponse response);

}
