package br.com.assai.horus.service.idenficacaoVendaCupom;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.response.IdentificacaoVendaCupomConverter;
import br.com.assai.horus.entity.IdentificacaoVendaCupom;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.identificacaoVendaCupom.IdentificacaoVendaCupomRepository;
import br.com.assai.horus.repository.mapper.IdentificacaoVendaCupomJdbcRepository;
import br.com.assai.horus.response.IdentificacaoVendaCupomResponse;
import br.com.assai.horus.security.SecurityDataUsuarioLogado;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IdentificacaoVendaCupomBusinessImpl 
                extends AbstractService<IdentificacaoVendaCupom, IdentificacaoVendaCupomRepository> 
                implements IdentificacaoVendaCupomBusiness {
    
    @Autowired 
    private SecurityDataUsuarioLogado securityDataUsuarioLogado;
    
    @Autowired 
    private IdentificacaoVendaCupomRepository identificacaoVendaCupomRepository; 
    
    @Autowired
    private IdentificacaoVendaCupomConverter identificacaoVendaCupomConverter;
    
    @Autowired
    private MybatisService myBatisService;
    
    public List<IdentificacaoVendaCupomResponse> listAll() {
    	return myBatisService.getRepository(IdentificacaoVendaCupomJdbcRepository.class).listAll();
    }

    public IdentificacaoVendaCupomResponse findByFilial(Long idFilial) {
    	return myBatisService.getRepository(IdentificacaoVendaCupomJdbcRepository.class).findByFilial(idFilial);
    }

    @Override
    public void mergeList(List<IdentificacaoVendaCupomResponse> responses) throws MapperRegistryException {
        List<IdentificacaoVendaCupom> entities = new ArrayList<>();
        Usuario usuario = securityDataUsuarioLogado.findUsuarioLogado();       
        
        for (IdentificacaoVendaCupomResponse response:  responses) {
            IdentificacaoVendaCupom entity = identificacaoVendaCupomConverter.convertToModel(response);
            entities.add(linkUsuarioLogado(entity, usuario));
        }
        
        identificacaoVendaCupomRepository.saveAll(entities);
    }

    
    private IdentificacaoVendaCupom linkUsuarioLogado(final IdentificacaoVendaCupom entity, Usuario usuario) {
        if (entity.isNew()) {
            entity.setDataCadastro(Calendar.getInstance().getTime());
            entity.setUsuarioCadastro(usuario);
        }
        entity.setDataAlteracao(Calendar.getInstance().getTime());
        entity.setUsuarioAlteracao(usuario);
        return entity;
    }
        
}

