package br.com.assai.horus.service.parametropreco;

import java.util.List;
import java.util.Optional;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.response.ParametroPrecoFilialConverter;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.parametropreco.ParametroPrecoFilial;
import br.com.assai.horus.repository.mapper.ParametroPrecoJdbcRepository;
import br.com.assai.horus.repository.parametropreco.ParametroPrecoFilialRepository;
import br.com.assai.horus.response.parametropreco.ParametroPrecoFilialResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoProdutoResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoResponse;
import br.com.assai.horus.security.SecurityDataUsuarioLogado;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParametroPrecoFilialBusinessImpl extends AbstractService<ParametroPrecoFilial, ParametroPrecoFilialRepository>  implements ParametroPrecoFilialBusiness {

    @Autowired 
	private SecurityDataUsuarioLogado securityDataUsuarioLogado;

    @Autowired 
    private MybatisService mybatisService; 

    @Autowired
    private ParametroPrecoFilialConverter parametroPrecoFilialConverter;

    @Autowired
    private ParametroPrecoProdutoBusiness parametroPrecoProdutoBusiness;

    /**
     * Lista todos os parametros de preco filial
     * @return Lista de parametro de preco filial
     */
    public List<ParametroPrecoFilialResponse> listAll() {
    	return mybatisService.getRepository(ParametroPrecoJdbcRepository.class).listParametroPrecoFiliais(null);
    }

    /**
     * Lista todos parametros de preco filial de um parametro preco
     * @param idParametroPreco Identificador do parametro de preco
     * @return Lista de parametro de preco filial
     */
    public List<ParametroPrecoFilialResponse> listByParametroPreco(Long idParametroPreco) {
    	return mybatisService.getRepository(ParametroPrecoJdbcRepository.class).listParametroPrecoFiliais(idParametroPreco);
    }


    /**
     * Salva um parametro de preco filial
     * @param response Parametro de preco filial response
     */
    @Override
	public ParametroPrecoFilialResponse save(ParametroPrecoFilialResponse response) {
        ParametroPrecoFilial parametroPrecoFilial = super.merge(parametroPrecoFilialConverter.convertToModel(response));        
        
        this.salvarParametrosProdutos(parametroPrecoFilial.getId(), response.getProdutos());

        return parametroPrecoFilialConverter.convertToResponse(parametroPrecoFilial);
    }

    /**
     * Remove filiais parametro preco de um parametro preco
     * @param idParametroPreco Identificador parametro de preco
     */
    public void removeByParametroPreco(Long idParametroPreco) {

        List<ParametroPrecoFilialResponse> parametrosPrecoFiliais = this.listByParametroPreco(idParametroPreco);

        removeFiliais(parametrosPrecoFiliais);
    }
    
    public void removeFiliais(List<ParametroPrecoFilialResponse> filiais) {

    	if(filiais != null) {
    		for (ParametroPrecoFilialResponse parametroPrecoFilialResponse : filiais) {
    			if(parametroPrecoFilialResponse.getId() != null) {
    				Optional<ParametroPrecoFilial> parametroPrecoFilial = getRepository().findById(parametroPrecoFilialResponse.getId());
    				parametroPrecoFilial.ifPresent(p -> { 
    					parametroPrecoProdutoBusiness.removeByParametroPrecoFilial(p.getId());
    					super.remove(p); 
    				});
    			}
			}
    	}
    	
    }
    
    /**
     * Salva parametro preco produtos
     * @param idParametroPrecoFilial Identificador parametro preco filial
     * @param produtos Lista de parametro preco produtos
     */
    private void salvarParametrosProdutos(Long idParametroPrecoFilial, List<ParametroPrecoProdutoResponse> produtos) {
        if (produtos == null) return;

        // parametroPrecoProdutoRepository.deleteByIdParamPrecoFilial(idParametroPrecoFilial);

        produtos.forEach(x -> {
            x.setIdParamPrecoFilial(idParametroPrecoFilial);
            x = parametroPrecoProdutoBusiness.save(x);
        });
    }
}
