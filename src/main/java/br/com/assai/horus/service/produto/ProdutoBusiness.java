package br.com.assai.horus.service.produto;

import java.util.List;

import br.com.assai.horus.entity.dependencies.ProdutoDependency;
import br.com.assai.horus.repository.dependencies.produto.ProdutoRepository;
import br.com.assai.horus.response.dependency.ProdutoDependencyResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;
import br.com.assai.horus.service.GeneralService;

@Deprecated
public interface ProdutoBusiness extends GeneralService<ProdutoDependency, ProdutoRepository> {

    
    List<ProdutoDependencyResponse> findValidProdutosToTelevendasByDescricaoCompleta(String descricaoCompleta, Long idFilial);
    List<ItemPrepedidoResponse> findDadosItemPrePedidoDeProdutoValidoComPrecoAndEstoqueInFilial(Long idProduto, Long idFilial, Long idPromocao);
    
}
