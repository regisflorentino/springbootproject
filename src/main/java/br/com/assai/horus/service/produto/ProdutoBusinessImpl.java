package br.com.assai.horus.service.produto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.dependencies.ProdutoDependency;
import br.com.assai.horus.repository.dependencies.produto.ProdutoRepository;
import br.com.assai.horus.repository.mapper.ProdutoJdbcRepository;
import br.com.assai.horus.response.dependency.ProdutoDependencyResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProdutoBusinessImpl extends AbstractService<ProdutoDependency, ProdutoRepository> implements ProdutoBusiness {

    @Autowired private MybatisService mybatisService;
    
    public List<ProdutoDependencyResponse> findValidProdutosToTelevendasByDescricaoCompleta(String descricaoCompleta, Long idFilial) {
        List<ProdutoDependencyResponse> result = new ArrayList<>();
        if (idFilial != null && idFilial > 0L) {
            result = mybatisService.getRepository(ProdutoJdbcRepository.class)
                            .findValidProdutoToTelevendasByDescricaoCompletaAndFilial(descricaoCompleta, idFilial, NumberUtils.isNumber(descricaoCompleta));
        } else {
            result = mybatisService.getRepository(ProdutoJdbcRepository.class)
                            .findValidProdutoToTelevendasByDescricaoCompletaAndFilial(descricaoCompleta, null, NumberUtils.isNumber(descricaoCompleta));
        }
        return result;
    }

    @Override
    public List<ItemPrepedidoResponse> findDadosItemPrePedidoDeProdutoValidoComPrecoAndEstoqueInFilial(Long idProduto, Long idFilial, Long idPromocao) {
//        List<ItemPrepedidoResponse> result = mybatisService.getRepository(ProdutoJdbcRepository.class).getProdutoValidoComPrecoAndEstoqueInFilial(idProduto, idFilial);
//        return result.stream().filter(i -> i.getEstoque() > 0 && i.getPrecoEmbalagemProduto().doubleValue() > 0D).collect(Collectors.toList());
        return mybatisService.getRepository(ProdutoJdbcRepository.class).findDadosItemPrePedidoDeProdutoValidoComPrecoAndEstoqueInFilial(idProduto, idFilial);
    }

}
