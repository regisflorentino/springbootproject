package br.com.assai.horus.service.pedido;

import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;



import br.com.assai.horus.converter.response.ItemPrepedidoConverterMapper;
import br.com.assai.horus.entity.pedido.ItemPedido;
import br.com.assai.horus.entity.pedido.Pedido;
import br.com.assai.horus.entity.prepedido.ItemPrepedido;
import br.com.assai.horus.entity.prepedido.Prepedido;
import br.com.assai.horus.iis.integration.dto.RestRequest;
import br.com.assai.horus.iis.integration.dto.TransacaoDetalheIntegracaoDTO;
import br.com.assai.horus.iis.integration.dto.TransacaoIntegracaoDTO;
import br.com.assai.horus.integracao.PedidoTVDIntegracao;
import br.com.assai.horus.repository.mapper.PedidoJdbcRepository;
import br.com.assai.horus.repository.mapper.PrepedidoJdbcRepository;
import br.com.assai.horus.repository.pedido.PedidoRepository;
import br.com.assai.horus.repository.prepedido.PrepedidoRepository;
import br.com.assai.horus.response.pedido.ItemPedidoResponse;
import br.com.assai.horus.response.pedido.PedidoResponse;
import br.com.assai.horus.response.pedido.mapa.separacao.ItemPedidoMapaSeparacaoResponse;
import br.com.assai.horus.response.prepedido.FormaPagamentoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.service.AbstractService;
import br.com.assai.horus.service.MybatisService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PedidoBusinessImpl extends AbstractService<Pedido, PedidoRepository> implements PedidoBusiness {

	@Autowired private MybatisService mybatisService;
	@Autowired private ItemPrepedidoConverterMapper itemPrepedidoConverter; 
	@Autowired private PedidoRepository pedidoRepo;
	@Autowired private PrepedidoRepository prepedidoRepo;
	@Autowired private PedidoTVDIntegracao pedidoTVDIntegracao;
	
	@Override
	public List<PedidoResponse> findByFilter(Long idFilial, Date dataInicial, Date dataFinal, List<Long> produtos, boolean flgExcluidos) {
		List<PedidoResponse> responses =  mybatisService.getRepository(PedidoJdbcRepository.class).findByFilter(idFilial, dataInicial, dataFinal, produtos, flgExcluidos);
		Format formatter = new SimpleDateFormat("dd/MM/yyyy");
		if(CollectionUtils.isNotEmpty(responses)) {
        	responses.forEach(index -> {
        		if(index.getDtCadastro() != null) {
        			index.setDataCadastro(formatter.format(index.getDtCadastro()));
        		}
        	});
        }
        return responses;
	}
	

	@Override
	public Pedido createPedido(Prepedido entity, PrepedidoResponse prepedidoResponse) { 
		
		Long idPedido =  getAllocatedIdPedido();
		entity.setIdPedido(idPedido);
		Pedido pedido = new Pedido(idPedido, entity);
		pedido.setItens(convertItemPrepedidoInItemPedido(prepedidoResponse, idPedido));
		Pedido saved = pedidoRepo.save(pedido);
		List<Long> idsFormas = prepedidoResponse.getFormasPagamento().stream().map(FormaPagamentoResponse::getId).collect(Collectors.toList());
		pedidoRepo.atualizaFormaPagamentoComIdPedido(saved.getId(), entity.getId(), idsFormas);
		pedidoRepo.atualizaPrepedidoComIdPedido(saved.getId(), entity.getId());
		List<Long>  idsWorkflowDetalhe = this.getIdWkfDetalhePrepedido(prepedidoResponse.getId());
		this.atualizaPrepedidoComIdWkfDetalhe(prepedidoResponse.getId(), idsWorkflowDetalhe.get(idsWorkflowDetalhe.size()-1));
        return saved;

	}
	
	private List<ItemPedido> convertItemPrepedidoInItemPedido(PrepedidoResponse prepedidoResponse, Long idPedido) {
		BigDecimal valorTotalPrepedido = BigDecimal.ZERO;
		BigDecimal valorTotalDesconto = BigDecimal.ZERO;
		List<ItemPedido> listItens = new ArrayList<>();
		
		if(CollectionUtils.isNotEmpty(prepedidoResponse.getItens())) {
			for (int i = 0; i < prepedidoResponse.getItens().size() ; i++) {
				ItemPrepedido itemPrepedido = this.itemPrepedidoConverter.responseToModel(prepedidoResponse.getItens().get(i));
				itemPrepedido.setNumeroItem(prepedidoResponse.getItens().size() - i);
				if(itemPrepedido.getValorTotalProduto() != null) {
					valorTotalPrepedido = valorTotalPrepedido.add(itemPrepedido.getValorTotalProduto());
				}
				if(itemPrepedido.getValorDesconto() != null) {
					valorTotalDesconto = valorTotalDesconto.add(itemPrepedido.getValorDesconto());
				}
				ItemPedido itemPedido = new ItemPedido(idPedido, itemPrepedido); 
				listItens.add(itemPedido);		
				
			}
		}
		return listItens;
	}
	
	public Long getAllocatedIdPedido() {
		Date date = DateTime.now().toDate();
		mybatisService.getRepository(PrepedidoJdbcRepository.class).alloc(date);
		Long sequenceValue = mybatisService.getRepository(PrepedidoJdbcRepository.class).getSequenceValue(date);
		return sequenceValue;
	}
	public void  atualizaPrepedidoComIdWkfDetalhe(long IdPrePedido , long idWorkflowDetalhe) {
		Optional<Prepedido> prepedido = prepedidoRepo.findById(IdPrePedido);
		prepedido.get().setDetalhe(idWorkflowDetalhe);
		prepedidoRepo.save(prepedido.get());
	}
	
	public List<Long> getIdWkfDetalhePrepedido(long idPrepedido) {
		return mybatisService.getRepository(PrepedidoJdbcRepository.class).getIdWkfDetalhePrepedido(idPrepedido);
	}
	
	 public  boolean enviarRequisicao( long idPedido) {
		 
		   PedidoResponse response =  mybatisService.getRepository(PedidoJdbcRepository.class).findByIdPedidoCapa(idPedido);
		  
		   String URI =  pedidoTVDIntegracao.integracaoEmpURI();
		   
		   TransacaoIntegracaoDTO transacaoDTO = buildTranasacaoDTO(response);
	         
	         RestRequest request = new RestRequest();
	         
	         request.setTransacao(transacaoDTO);
		 
	        boolean ret = true;

	        try {
	            RestTemplate restTemplate = new RestTemplate();
	             if(restTemplate.postForEntity(URI, request, ResponseEntity.class).getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR) == true)
	            	ret = false;
	         } catch (Exception e) {
	            ret = false;
	        }

	        return ret;
	    }
	
	 
	 private TransacaoIntegracaoDTO buildTranasacaoDTO(PedidoResponse response) {
		 
		 TransacaoIntegracaoDTO transacaoDTO = new TransacaoIntegracaoDTO();
         
		 transacaoDTO.setIdTransacao(response.getIdTransacao());
		 
		 transacaoDTO.setDataTransacao(response.getDataGeracaoPedido());
		 
		 transacaoDTO.setUsuarioId(response.getIdUsuarioCadastro());
         
         TransacaoDetalheIntegracaoDTO detalheDTO =  new TransacaoDetalheIntegracaoDTO();

         detalheDTO.setCodigoItem(response.getId());   
         
         detalheDTO.setCodigoFilial(response.getIdFilial());
         
         detalheDTO.setContexto("INT_PEDIDO_TVD_EMPORIO");
         
         List<TransacaoDetalheIntegracaoDTO> detalhes = Lists.newArrayList();
         
         detalhes.add(detalheDTO);
		
         transacaoDTO.setDetalhes(detalhes);
         
         return transacaoDTO;
		 
	 }
	
	 public   List<ItemPedidoMapaSeparacaoResponse> mapaSeparacao( long idPedido) {
		 List<ItemPedidoMapaSeparacaoResponse> responseItem =  mybatisService.getRepository(PedidoJdbcRepository.class).findMapaSeparacao(idPedido);
		 return responseItem ;
	 }


	public List<ItemPedidoMapaSeparacaoResponse> termoRetirada(Long idPedido) {
		List<ItemPedidoMapaSeparacaoResponse> responseItem =  mybatisService.getRepository(PedidoJdbcRepository.class).termoRetirada(idPedido);
		 return responseItem ;
	}
	
	public void cancelarPedido(List<Long> idsPedidos) {
	   for (Long idPedido : idsPedidos) {
		   mybatisService.getRepository(PedidoJdbcRepository.class).cancelarPedido( idPedido  , new Date ());
	}
    	
	}
	
	 public   String  getNomeVendedor( long idVendedor) {
		return  mybatisService.getRepository(PedidoJdbcRepository.class).getNomeVendedor(idVendedor);
	 }
}
