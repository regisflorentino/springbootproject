package br.com.assai.horus.service.estruturamercadologica;

import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency;
import br.com.assai.horus.repository.dependencies.EstruturaMercadologicaRepository;
import br.com.assai.horus.service.GeneralService;

public interface EstruturaMercadologicaBusiness  extends GeneralService<EstruturaMercadologicaDependency, EstruturaMercadologicaRepository> { 
    

}
