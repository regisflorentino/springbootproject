package br.com.assai.horus.resolver;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.assai.horus.context.domain.rule.ContextDomainResolver;
import br.com.assai.horus.context.domain.transaction.EntityTransactionDetail;
import br.com.assai.horus.message.constant.HorusQueue;
import br.com.assai.horus.message.constant.MessageServiceBean;
import br.com.assai.horus.message.content.MessageContent;
import br.com.assai.horus.message.content.QueueState;
import br.com.assai.horus.message.entity.HorusSqsEntity;
import br.com.assai.horus.message.request.MessageRequest;
import br.com.assai.horus.message.request.MessageRequest.MessageRequestBuilder;
import br.com.assai.horus.message.service.HorusSqsService;
import br.com.assai.horus.utils.JsonNodeBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TelevendasContextDomainResolver implements ContextDomainResolver {

	@Autowired
	private HorusSqsService service;

	private HorusQueue contextDomain = HorusQueue.CONTEXT_DOMAIN;
	private HorusSqsEntity entity;

	@PostConstruct
	public void postConstructor() {
		this.entity = this.service.findQueueByStateAndKey(contextDomain, QueueState.IN);
	}

	
	public String getLocal() {
		return "TELEVENDAS";
	}
	
	@Override
	public void resolve(EntityTransactionDetail element) throws IOException {
		this.initLog(log, element, this.entity.getAddress());
		MessageRequestBuilder builder = MessageRequest.builder();
		MessageServiceBean bean = null;
		switch (element.getTransactionType()) {
		case EXCLUSAO:
			bean = MessageServiceBean.CONTEXT_DOMAIN_DELETE;
			break;
		case INSERCAO:
			bean = MessageServiceBean.CONTEXT_DOMAIN_INSERT;
			break;
		default:
			bean = MessageServiceBean.CONTEXT_DOMAIN_UPDATE;
			break;
		}
		builder.serviceName(bean);
		builder.queue(this.entity);
		MessageContent messageContent = new MessageContent();
		messageContent.setLogin(element.getLogin());
		messageContent.setToken(element.getToken());
		messageContent.setCredentialToken(element.getCredentialToken());
		messageContent.setBean(bean);

		messageContent.setContent(JsonNodeBuilder.toJson(element));

		builder.messageContent(messageContent);
		this.service.send(builder.build());
	}

}
