package br.com.assai.horus.converter.response;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.repository.UsuarioRepository;

@Service
public class MapperUtil {
    
    @Autowired private UsuarioRepository userRepository;
    
    public Usuario getUsuario(Long idUsuario) {
        Optional<Usuario> result = Optional.empty();
        if (idUsuario != null) {
            result = userRepository.findById(idUsuario);
        }
        return result.orElse(null);
    } 

}
