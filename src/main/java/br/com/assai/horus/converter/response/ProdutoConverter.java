package br.com.assai.horus.converter.response;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.dependencies.ProdutoDependency;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.mapper.HorusMapper;
import br.com.assai.horus.response.dependency.ProdutoDependencyResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProdutoConverter implements ConverterModelResponse<ProdutoDependency, ProdutoDependencyResponse>{

    @Autowired 
    private HorusMapper<ProdutoConverter> mapper;
    
	@Override
	public ProdutoDependencyResponse convertToResponse(ProdutoDependency entity) throws MapperRegistryException {
		try {
			return this.mapper.registryMapper(this).convertToResponse(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ProdutoDependency convertToModel(ProdutoDependencyResponse entity) throws MapperRegistryException {
		try {
			return this.mapper.registryMapper(this).convertToModel(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
    
}
