package br.com.assai.horus.converter.response;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.dto.HorusGenericEntity;
import br.com.assai.horus.entity.prepedido.ItemPrepedido;
import br.com.assai.horus.entity.prepedido.ItemPrepedido.ItemPrepedidoBuilder;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ItemPrepedidoConverterMapper {

	public ItemPrepedidoResponse modelToResponse(ItemPrepedido model) {
		if (model == null) {
			return null;
		}
		ItemPrepedidoResponse itemPrepedidoResponse = new ItemPrepedidoResponse();

        itemPrepedidoResponse.setId(model.getId());
        itemPrepedidoResponse.setIdPrepedido(model.getIdPrepedido());
        if(model.getNegociacaoPontual() != null) {
        	itemPrepedidoResponse.setPontual(model.getNegociacaoPontual());
        }
        itemPrepedidoResponse.setProduto(HorusGenericEntity.builder().id(model.getIdProduto()).build());
        itemPrepedidoResponse.setEmbalagem(HorusGenericEntity.builder().id(model.getIdEmbalagem()).build());
        itemPrepedidoResponse.setMargem(model.getMargem());
        itemPrepedidoResponse.setPrecoNegociadoUnit(model.getPrecoNegociadoUnit());
        itemPrepedidoResponse.setPrecoNegociadoEmb(model.getPrecoNegociadoEmb());
        itemPrepedidoResponse.setQtdNegociada(model.getQtdNegociada());
        itemPrepedidoResponse.setValorTotalProduto(model.getValorTotalProduto());
        
        itemPrepedidoResponse.setNumeroItem(model.getNumeroItem());

        itemPrepedidoResponse.setIdProdutoFilialPreco(model.getIdProdutoFilialPreco());
        itemPrepedidoResponse.setQtdDevolvida(model.getQtdDevolvida());
        itemPrepedidoResponse.setQtdRetirada(model.getQtdRetirada());
        itemPrepedidoResponse.setQtdSeparacao(model.getQtdSeparacao());
        itemPrepedidoResponse.setStatusSeparacao(model.getStatusSeparacao());
        itemPrepedidoResponse.setValorDesconto(model.getValorDesconto());
        itemPrepedidoResponse.setValorDespesa(model.getValorDespesa());
        itemPrepedidoResponse.setValorImposto(model.getValorImposto());
        itemPrepedidoResponse.setValorPrecoVigente(model.getValorPrecoVigente());
        itemPrepedidoResponse.setValorVerba(model.getValorVerba());

		return itemPrepedidoResponse;
	}

	public ItemPrepedido responseToModel(ItemPrepedidoResponse response) {
		if (response == null) {
			return null;
		}

		ItemPrepedidoBuilder itemBuilder = ItemPrepedido.builder();
		itemBuilder.id(response.getId());
		itemBuilder.idProduto(response.getProdutoId());
		itemBuilder.idEmbalagem(response.getEmbalagemId());
		itemBuilder.qtdNegociada(response.getQtdNegociada());
		itemBuilder.precoNegociadoEmb(response.getPrecoNegociadoEmb());
		itemBuilder.precoNegociadoUnit(response.getPrecoNegociadoUnit());
		itemBuilder.valorTotalProduto(response.getValorTotalProduto());
		itemBuilder.negociacaoPontual(response.isPontual());
		itemBuilder.margem(response.getMargem());
		itemBuilder.idProdutoFilialPreco(response.getIdProdutoFilialPreco());

        if (response.getPrecoNegociadoEmb() != null) {
        	itemBuilder.valorPrecoVigente(response.getPrecoNegociadoEmb());
        } else {
        	itemBuilder.valorPrecoVigente(response.getPrecoEmbalagemProduto());
        }
		
        itemBuilder.valorPrecoVigente(response.getValorPrecoVigente());
        itemBuilder.valorImposto(response.getValorImposto());
        itemBuilder.valorDespesa(response.getValorDespesa());
        itemBuilder.valorVerba(response.getValorVerba());
        itemBuilder.valorTotalProduto(response.getValorTotalProduto());
        itemBuilder.valorDesconto(response.getValorDesconto());
        itemBuilder.statusSeparacao(response.getStatusSeparacao());
        itemBuilder.qtdSeparacao(response.getQtdSeparacao());
        itemBuilder.qtdRetirada(response.getQtdRetirada());
        itemBuilder.qtdDevolvida(response.getQtdDevolvida());

		return itemBuilder.build();
	}

}
