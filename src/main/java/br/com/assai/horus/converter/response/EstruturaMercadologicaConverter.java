package br.com.assai.horus.converter.response;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.mapper.HorusMapper;
import br.com.assai.horus.response.dependency.EstruturaMercadologicaDependencyResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EstruturaMercadologicaConverter implements ConverterModelResponse<EstruturaMercadologicaDependency, EstruturaMercadologicaDependencyResponse> {

	@Autowired  
	private HorusMapper<EstruturaMercadologicaConverter> mapper;

	@Override
	public EstruturaMercadologicaDependencyResponse convertToResponse(EstruturaMercadologicaDependency entity) throws MapperRegistryException {
		EstruturaMercadologicaDependencyResponse response = null;
		try {
			response = this.mapper.registryMapper(this).convertToResponse(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public EstruturaMercadologicaDependency convertToModel(EstruturaMercadologicaDependencyResponse entity) throws MapperRegistryException { 
		EstruturaMercadologicaDependency model = null;
		try {
			model = this.mapper.registryMapper(this).convertToModel(entity);
			if(entity.getIdNivelPai() == null) {
				model.setNivelPai(null);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return model;
	} 
	
}
