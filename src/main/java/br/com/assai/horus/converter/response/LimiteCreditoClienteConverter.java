package br.com.assai.horus.converter.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.prepedido.LimiteCreditoCliente;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.limitecredito.LimiteCreditoClienteRepository;
import br.com.assai.horus.response.prepedido.LimiteCreditoClienteResponse;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LimiteCreditoClienteConverter implements ConverterModelResponse<LimiteCreditoCliente, LimiteCreditoClienteResponse> {
    
    @Autowired private MapperUtil mapperUtil;
    @Autowired private LimiteCreditoClienteRepository limiteCreditoClienteRepository;

    public void afterMapping(LimiteCreditoClienteResponse response, LimiteCreditoCliente model) {
          Usuario userAlteracao = mapperUtil.getUsuario(response.getIdUsuarioAlteracao());
          Usuario userCadastro = mapperUtil.getUsuario(response.getIdUsuarioCadastro());
          model.setUsuarioCadastro(userCadastro);
          model.setUsuarioAlteracao(userAlteracao);
    }

    @Override
    public LimiteCreditoClienteResponse convertToResponse(LimiteCreditoCliente model) throws MapperRegistryException {
        if ( model == null ) {
            return null;
        }

        LimiteCreditoClienteResponse limiteCreditoClienteResponse = new LimiteCreditoClienteResponse();

        Long id = modelUsuarioCadastroId( model );
        if ( id != null ) {
            limiteCreditoClienteResponse.setIdUsuarioCadastro( id );
        }
        Long id1 = modelUsuarioAlteracaoId( model );
        if ( id1 != null ) {
            limiteCreditoClienteResponse.setIdUsuarioAlteracao( id1 );
        }
        limiteCreditoClienteResponse.setId( model.getId() );
        limiteCreditoClienteResponse.setIdPessoa(model.getIdPessoa());
        limiteCreditoClienteResponse.setIdPrepedido( model.getIdPrepedido() );
        limiteCreditoClienteResponse.setDataConsultaLimite( model.getDataConsultaLimite() );
        limiteCreditoClienteResponse.setIdAssociacao( model.getIdAssociacao() );
        limiteCreditoClienteResponse.setAssociacao( model.getAssociacao() );
        limiteCreditoClienteResponse.setSituacao( model.getSituacao() );
        limiteCreditoClienteResponse.setValorLimiteCredito( model.getValorLimiteCredito() );
        limiteCreditoClienteResponse.setValorTituloAberto( model.getValorTituloAberto() );
        limiteCreditoClienteResponse.setValorFaturamentoDia( model.getValorFaturamentoDia() );
        limiteCreditoClienteResponse.setValorDisponivel( model.getValorDisponivel() );
        
        limiteCreditoClienteResponse.setPrazoMaximo( model.getPrazoMaximo() );
        limiteCreditoClienteResponse.setDataCadastro( model.getDataCadastro() );
        limiteCreditoClienteResponse.setDataAlteracao( model.getDataAlteracao() );
        limiteCreditoClienteResponse.setIdTransacao( model.getIdTransacao() );

        return limiteCreditoClienteResponse;
    }

    @Override
    public LimiteCreditoCliente convertToModel(LimiteCreditoClienteResponse response) throws MapperRegistryException {
        if ( response == null ) {
            return null;
        }
        LimiteCreditoCliente limiteCreditoCliente = responseToModel(response);
        return limiteCreditoCliente;
    }

	private LimiteCreditoCliente responseToModel(LimiteCreditoClienteResponse response) {
		LimiteCreditoCliente limiteCreditoCliente = null;
		
		if (response != null) {
			limiteCreditoCliente = new LimiteCreditoCliente();
	        limiteCreditoCliente.setId( response.getId() );
	        limiteCreditoCliente.setIdPrepedido( response.getIdPrepedido() );
	        copyData(response, limiteCreditoCliente);
		}
		return limiteCreditoCliente;
	}

	public void copyData(LimiteCreditoClienteResponse response, LimiteCreditoCliente limiteCreditoCliente) {
		limiteCreditoCliente.setIdPessoa(response.getIdPessoa());
		limiteCreditoCliente.setDataConsultaLimite( response.getDataConsultaLimite() );
		limiteCreditoCliente.setIdAssociacao( response.getIdAssociacao() );
		limiteCreditoCliente.setAssociacao( response.getAssociacao() );
		limiteCreditoCliente.setSituacao( response.getSituacao() );
		limiteCreditoCliente.setValorLimiteCredito( response.getValorLimiteCredito() );
		limiteCreditoCliente.setValorTituloAberto( response.getValorTituloAberto() );
		limiteCreditoCliente.setValorFaturamentoDia( response.getValorFaturamentoDia() );
		limiteCreditoCliente.setValorDisponivel( response.getValorDisponivel() );
		limiteCreditoCliente.setPrazoMaximo( response.getPrazoMaximo() );
		limiteCreditoCliente.setDataCadastro( response.getDataCadastro() );
		limiteCreditoCliente.setDataAlteracao( response.getDataAlteracao() );
		limiteCreditoCliente.setIdTransacao( response.getIdTransacao() );
		afterMapping( response, limiteCreditoCliente );
	}

    private Long modelUsuarioCadastroId(LimiteCreditoCliente limiteCreditoCliente) {
        if ( limiteCreditoCliente == null ) {
            return null;
        }
        Usuario usuarioCadastro = limiteCreditoCliente.getUsuarioCadastro();
        if ( usuarioCadastro == null ) {
            return null;
        }
        Long id = usuarioCadastro.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long modelUsuarioAlteracaoId(LimiteCreditoCliente limiteCreditoCliente) {
        if ( limiteCreditoCliente == null ) {
            return null;
        }
        Usuario usuarioAlteracao = limiteCreditoCliente.getUsuarioAlteracao();
        if ( usuarioAlteracao == null ) {
            return null;
        }
        Long id = usuarioAlteracao.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    public List<LimiteCreditoClienteResponse> convertListModelToListResponse(List<LimiteCreditoCliente> models) throws MapperRegistryException {
    	List<LimiteCreditoClienteResponse> result = new ArrayList<>();
    	if (models != null) {
    		for (LimiteCreditoCliente model: models) {
    			result.add(convertToResponse(model));
    		}
    	}
    	return result;
    }
    
    public List<LimiteCreditoCliente> convertListResponseToListModel(List<LimiteCreditoClienteResponse> responses) throws MapperRegistryException {
    	List<LimiteCreditoCliente> result = new ArrayList<>();
    	if (responses != null) {
    		for (LimiteCreditoClienteResponse response: responses) {
    			result.add(convertToModel(response));
    		}
    	}
    	return result;
    }

    
}
