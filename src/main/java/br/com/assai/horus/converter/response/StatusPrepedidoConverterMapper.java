package br.com.assai.horus.converter.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.prepedido.StatusTelevendas;
import br.com.assai.horus.response.prepedido.StatusPrepedidoResponse;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StatusPrepedidoConverterMapper {
	
    @Autowired public MapperUtil mapperUtil;
   
    public StatusPrepedidoResponse modelToResponse(StatusTelevendas model) {
        if ( model == null ) {
            return null;
        }

        StatusPrepedidoResponse statusPrepedidoResponse = new StatusPrepedidoResponse();

        Long id = modelUsuarioCadastroId( model );
        if ( id != null ) {
            statusPrepedidoResponse.setIdUsuarioCadastro( id );
        }
        Long id1 = modelUsuarioAlteracaoId( model );
        if ( id1 != null ) {
            statusPrepedidoResponse.setIdUsuarioAlteracao( id1 );
        }
        statusPrepedidoResponse.setId( model.getId() );
        statusPrepedidoResponse.setStatus( model.getStatus() );
        statusPrepedidoResponse.setDataCadastro( model.getDataCadastro() );
        statusPrepedidoResponse.setDataAlteracao( model.getDataAlteracao() );
        statusPrepedidoResponse.setIdTransacao( model.getIdTransacao() );

        return statusPrepedidoResponse;
    }

    public StatusTelevendas responseToModel(StatusPrepedidoResponse response) {
        if ( response == null ) {
            return null;
        }

        StatusTelevendas statusPrepedido = new StatusTelevendas();

        statusPrepedido.setId( response.getId() );
        statusPrepedido.setStatus( response.getStatus() );
        statusPrepedido.setDataCadastro( response.getDataCadastro() );
        statusPrepedido.setDataAlteracao( response.getDataAlteracao() );
        statusPrepedido.setIdTransacao( response.getIdTransacao() );

        return statusPrepedido;
    }

    private Long modelUsuarioCadastroId(StatusTelevendas statusPrepedido) {
        if ( statusPrepedido == null ) {
            return null;
        }
        Usuario usuarioCadastro = statusPrepedido.getUsuarioCadastro();
        if ( usuarioCadastro == null ) {
            return null;
        }
        Long id = usuarioCadastro.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long modelUsuarioAlteracaoId(StatusTelevendas statusPrepedido) {
        if ( statusPrepedido == null ) {
            return null;
        }
        Usuario usuarioAlteracao = statusPrepedido.getUsuarioAlteracao();
        if ( usuarioAlteracao == null ) {
            return null;
        }
        Long id = usuarioAlteracao.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }    

}
