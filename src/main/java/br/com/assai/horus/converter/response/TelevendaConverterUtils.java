package br.com.assai.horus.converter.response;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.repository.UsuarioRepository;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TelevendaConverterUtils {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public Optional<Usuario> getUsuario(Long usuarioId) {
		if(usuarioId != null) {
			return usuarioRepository.findById(usuarioId);
		}
		return Optional.empty();
	}
	
}
