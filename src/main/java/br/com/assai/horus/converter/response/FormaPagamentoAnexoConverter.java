package br.com.assai.horus.converter.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.response.prepedido.FormaPagamentoAnexoResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FormaPagamentoAnexoConverter implements ConverterModelResponse<FormaPagamentoAnexo, FormaPagamentoAnexoResponse>{
	
	@Autowired private MapperUtil mapperUtil;

	@Override
	public FormaPagamentoAnexo convertToModel(FormaPagamentoAnexoResponse response) throws MapperRegistryException {
        if ( response == null ) {
            return null;
        }

        FormaPagamentoAnexo formaPagamentoAnexo = new FormaPagamentoAnexo();

        formaPagamentoAnexo.setId( response.getId() );
        formaPagamentoAnexo.setNomeAnexo( response.getNomeAnexo() );
        formaPagamentoAnexo.setDataCadastro( response.getDataCadastro() );
        formaPagamentoAnexo.setDataAlteracao( response.getDataAlteracao() );
        formaPagamentoAnexo.setIdTransacao( response.getIdTransacao() );

        Usuario userAlteracao = mapperUtil.getUsuario(response.getIdUsuarioAlteracao());
        Usuario userCadastro = mapperUtil.getUsuario(response.getIdUsuarioCadastro());
        formaPagamentoAnexo.setUsuarioCadastro(userCadastro);
        formaPagamentoAnexo.setUsuarioAlteracao(userAlteracao);

        return formaPagamentoAnexo;
	}

	@Override
	public FormaPagamentoAnexoResponse convertToResponse(FormaPagamentoAnexo model) throws MapperRegistryException {
        if ( model == null ) {
            return null;
        }

        FormaPagamentoAnexoResponse formaPagamentoAnexoResponse = new FormaPagamentoAnexoResponse();

        Long id = modelUsuarioCadastroId( model );
        if ( id != null ) {
            formaPagamentoAnexoResponse.setIdUsuarioCadastro( id );
        }
        Long id1 = modelUsuarioAlteracaoId( model );
        if ( id1 != null ) {
            formaPagamentoAnexoResponse.setIdUsuarioAlteracao( id1 );
        }
        Long id2 = modelFormaPagamentoId( model );
        if ( id2 != null ) {
            formaPagamentoAnexoResponse.setIdFormaPagamento( id2 );
        }
        formaPagamentoAnexoResponse.setId( model.getId() );
        formaPagamentoAnexoResponse.setNomeAnexo( model.getNomeAnexo() );
        formaPagamentoAnexoResponse.setDataCadastro( model.getDataCadastro() );
        formaPagamentoAnexoResponse.setDataAlteracao( model.getDataAlteracao() );
        formaPagamentoAnexoResponse.setIdTransacao( model.getIdTransacao() );

        return formaPagamentoAnexoResponse;
    }
	
	
	public List<FormaPagamentoAnexo> convertListResponseToListModel(List<FormaPagamentoAnexoResponse> responses) throws MapperRegistryException {
		List<FormaPagamentoAnexo> result = new ArrayList<>();
		if (responses != null) {
			for (FormaPagamentoAnexoResponse response : responses) {
				result.add(this.convertToModel(response));
			}
		}
		return result;
		
	}
	
	public List<FormaPagamentoAnexoResponse> convertListModelToListResponse(List<FormaPagamentoAnexo> entities) throws MapperRegistryException {
		List<FormaPagamentoAnexoResponse> result = new ArrayList<>();
		if (entities != null) {
			for (FormaPagamentoAnexo entity : entities) {
				result.add(this.convertToResponse(entity));
			}
		}
		return result;
	}
	
    private Long modelUsuarioCadastroId(FormaPagamentoAnexo formaPagamentoAnexo) {
        if ( formaPagamentoAnexo == null ) {
            return null;
        }
        Usuario usuarioCadastro = formaPagamentoAnexo.getUsuarioCadastro();
        if ( usuarioCadastro == null ) {
            return null;
        }
        return usuarioCadastro.getId();
    }

    private Long modelUsuarioAlteracaoId(FormaPagamentoAnexo formaPagamentoAnexo) {
        if ( formaPagamentoAnexo == null ) {
            return null;
        }
        Usuario usuarioAlteracao = formaPagamentoAnexo.getUsuarioAlteracao();
        if ( usuarioAlteracao == null ) {
            return null;
        }
        return usuarioAlteracao.getId();
    }

    private Long modelFormaPagamentoId(FormaPagamentoAnexo formaPagamentoAnexo) {
        if ( formaPagamentoAnexo == null ) {
            return null;
        }
        FormaPagamento formaPagamento = formaPagamentoAnexo.getFormaPagamento();
        if ( formaPagamento == null ) {
            return null;
        }
        return formaPagamento.getId();
    } 	

}
