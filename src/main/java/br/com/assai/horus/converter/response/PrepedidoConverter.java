package br.com.assai.horus.converter.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import br.com.assai.horus.contextdomain.entity.TransacaoContextDomainParentComponent;
import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.Usuario;
import br.com.assai.horus.entity.constant.FormaRetirada;
import br.com.assai.horus.entity.constant.TipoFaturamento;
import br.com.assai.horus.entity.constant.TipoOrigemEnum;
import br.com.assai.horus.entity.constant.TipoVendaPrePedido;
import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.entity.prepedido.ItemPrepedido;
import br.com.assai.horus.entity.prepedido.Prepedido;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.prepedido.ItemPrepedidoRespository;
import br.com.assai.horus.response.prepedido.FormaPagamentoResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.service.UsuarioBusiness;
import br.com.assai.horus.service.prepedido.PrepedidoBusiness;

@Service
public class PrepedidoConverter implements ConverterModelResponse<Prepedido, PrepedidoResponse> {
    
	@Autowired
	private FormaPagamentoConverter formaPagamentoConverter;
	
	@Autowired
	private ItemPrepedidoRespository itemPrepedidoRespository; 
	
	@Autowired
	private PrepedidoBusiness prepedidoBusiness;

	@Autowired
	private ItemPrepedidoConverterMapper itemPrepedidoConverter; 

	@Autowired
	private UsuarioBusiness usuarioBusiness;
	
    @Override
    public PrepedidoResponse convertToResponse(Prepedido model) throws MapperRegistryException {
        if (model == null ) {
            return null;
        }
        PrepedidoResponse prepedidoResponse = new PrepedidoResponse();
        prepedidoResponse.setId(model.getId());
        prepedidoResponse.setIdOperacao(model.getIdOperacao());
        prepedidoResponse.setTipoVenda(model.getTipoVenda().getId());
        prepedidoResponse.setTipoOrigem(model.getTipoOrigem()  == null ? "ERP" : model.getTipoOrigem().getSigla());
        prepedidoResponse.setTipoFaturamento(model.getTipoFaturamento().toString());
        prepedidoResponse.setIdFilial(model.getIdFilial());
        prepedidoResponse.setIdStatus(model.getIdStatus());
        prepedidoResponse.setSeparacao(model.getSeparacao());
        
        prepedidoResponse.setObservacaoNota(model.getObservacaoNota());
        prepedidoResponse.setObservacaoPedido(model.getObservacaoPedido());
        
        prepedidoResponse.setClienteNaoIdentificado(model.isClienteNaoIdentificado());
        prepedidoResponse.setNumeroCpfCnpjCupom(model.getNumeroCpfCnpjCupom());
        prepedidoResponse.setIdVendedor(model.getIdVendedor());
        prepedidoResponse.setNomeVendedor(prepedidoBusiness.getNomeVendedor(model.getIdVendedor()));
        prepedidoResponse.setIdPedidoCliente(model.getIdPedidoCliente());
        prepedidoResponse.setIdCliente(model.getIdCliente());
        prepedidoResponse.setIdEndereco(model.getIdEndereco());
        prepedidoResponse.setIdPedido(model.getIdPedido());
        prepedidoResponse.setObservacaoPedido(model.getObservacaoPedido());
        prepedidoResponse.setObservacaoNota(model.getObservacaoNota());

        prepedidoResponse.setNumeroPrazoPagamento(model.getNumeroPrazoPagamento());
        if(model.getFormaRetirada() != null) {
        	prepedidoResponse.setFormaRetirada(model.getFormaRetirada().getId());
        }
        prepedidoResponse.setDataPrevistaRetirada(model.getDataPrevistaRetirada());
        
        if (model.getIdTransportadora() != null ) {
            prepedidoResponse.setIdTransportadora(model.getIdTransportadora());
        }
        
        prepedidoResponse.setNumeroRgCpfRetirada(model.getNumeroRgCpfRetirada());
        prepedidoResponse.setNomeMotorista(model.getNomeMotorista());
        prepedidoResponse.setTelefoneMotorista(model.getTelefoneMotorista());
        prepedidoResponse.setNomeVeiculo(model.getNomeVeiculo());
        prepedidoResponse.setPlaca(model.getPlaca());
        prepedidoResponse.setDataGeracaoPedido(model.getDataGeracaoPedido());
        prepedidoResponse.setQtdTotalItens(model.getQtdTotalItens());
        
        prepedidoResponse.setItens(new ArrayList<ItemPrepedidoResponse>());

		Example<ItemPrepedido> example = Example.of(ItemPrepedido.builder().idPrepedido(model.getId()).build(), ExampleMatcher.matchingAll());
        List<ItemPrepedido> itens = itemPrepedidoRespository.findAll(example);
        
        if(CollectionUtils.isNotEmpty(itens)) {
        	for (ItemPrepedido itemPrepedido : itens) {
        		prepedidoResponse.getItens().add(this.itemPrepedidoConverter.modelToResponse(itemPrepedido));		
			}
        }
        
        if(CollectionUtils.isNotEmpty(model.getFormasPagamento())) {
        	prepedidoResponse.setFormasPagamento(new ArrayList<FormaPagamentoResponse>());
        	for (FormaPagamento formaPagamento : model.getFormasPagamento()) {
        		prepedidoResponse.getFormasPagamento().add(this.formaPagamentoConverter.convertToResponse(formaPagamento));
			}
        }
        
        TransacaoContextDomainParentComponent transacao = model.getTransacao();
		if(transacao != null) {
			Optional<Usuario> usuario = usuarioBusiness.findById(transacao.getUsuarioCadastroId());
        	prepedidoResponse.setDtCadastro(transacao.getDataCadastro());
        	if(usuario.isPresent()) {
        		prepedidoResponse.setNomeUsuarioCadastro(usuario.get().getNome());
        	}
        }
        
        prepedidoResponse.setValorTotalDesconto(model.getValorTotalDesconto());
        prepedidoResponse.setPercentutalTotalMargem(model.getPercentutalTotalMargem());
        prepedidoResponse.setValorTotalPrepedido(model.getValorTotalPrepedido());
        
        prepedidoResponse.setVersao(model.getVersao());

        return prepedidoResponse;
    }

    @Override
    public Prepedido convertToModel(PrepedidoResponse response) throws MapperRegistryException {
        if (response == null ) {
            return null;
        }

        return copyPropertiesResponseToModel(response, new Prepedido());

    }

    public Prepedido copyPropertiesResponseToModel(PrepedidoResponse response, Prepedido prepedido) throws MapperRegistryException {
    	if (response != null && prepedido != null) {
	        prepedido.setVersao(response.getVersao());
	        prepedido.setIdVendedor(response.getIdVendedor());
    		prepedido.setId(response.getId());
    		prepedido.setIdOperacao(response.getIdOperacao());
	        prepedido.setIdFilial(response.getIdFilial());
	        prepedido.setTipoFaturamento(TipoFaturamento.valueOf(response.getTipoFaturamento()));
	        prepedido.setTipoVenda(TipoVendaPrePedido.findById(response.getTipoVenda()));
	        prepedido.setTipoOrigem(TipoOrigemEnum.searchBySigla(response.getTipoOrigem()));
	        prepedido.setIdStatus(response.getIdStatus());
    		
	        prepedido.setSeparacao(response.getSeparacao());
    		prepedido.setIdCliente(response.getIdCliente());
    		prepedido.setIdEndereco(response.getIdEndereco());
    		prepedido.setClienteNaoIdentificado(response.isClienteNaoIdentificado());
    		prepedido.setNumeroCpfCnpjCupom(response.getNumeroCpfCnpjCupom());
    		
    		prepedido.setFormaRetirada(FormaRetirada.searchById(response.getFormaRetirada()));
    		prepedido.setDataPrevistaRetirada(response.getDataPrevistaRetirada());
    		prepedido.setIdTransportadora(response.getIdTransportadora());
    		
    		prepedido.setNomeMotorista(response.getNomeMotorista());
    		prepedido.setNomeVeiculo(response.getNomeVeiculo());
    		prepedido.setTelefoneMotorista(response.getTelefoneMotorista());
    		prepedido.setPlaca(response.getPlaca());
    		
    		prepedido.setObservacaoNota(response.getObservacaoNota());
    		prepedido.setObservacaoPedido(response.getObservacaoPedido());
    		
    		
	        List<FormaPagamentoResponse> formasPagamento = response.getFormasPagamento();
	        if(CollectionUtils.isNotEmpty(formasPagamento)) {
				prepedido.setFormasPagamento(new ArrayList<FormaPagamento>());
				for (FormaPagamentoResponse formaPagamentoResponse : formasPagamento) {
					formaPagamentoResponse.setIdPrepedido(prepedido.getId());
					prepedido.getFormasPagamento().add(this.formaPagamentoConverter.convertToModel(formaPagamentoResponse));
				};
	        }
	        prepedido.setNumeroPrazoPagamento(response.getNumeroPrazoPagamento());
	        prepedido.setNumeroRgCpfRetirada(response.getNumeroRgCpfRetirada());
	        prepedido.setDataGeracaoPedido(response.getDataGeracaoPedido());
	        
	        prepedido.setIdPedido(response.getIdPedido());
	        prepedido.setIdPedidoCliente(response.getIdPedidoCliente());
	        prepedido.setQtdTotalItens(response.getQtdTotalItens());
	        prepedido.setValorTotalDesconto(response.getValorTotalDesconto());
	        prepedido.setPercentutalTotalMargem(response.getPercentutalTotalMargem());
	        prepedido.setValorTotalPrepedido(response.getValorTotalPrepedido());
//	        prepedido.setIdPedidoCliente(response.getIdPedidoCliente());
    	}
    	return prepedido;
	}

}
