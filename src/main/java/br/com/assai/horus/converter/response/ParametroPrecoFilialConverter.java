package br.com.assai.horus.converter.response;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.mapper.HorusMapper;
import br.com.assai.horus.response.parametropreco.ParametroPrecoFilialResponse;
import br.com.assai.horus.entity.parametropreco.ParametroPrecoFilial;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParametroPrecoFilialConverter implements ConverterModelResponse<ParametroPrecoFilial, ParametroPrecoFilialResponse> {

    @Autowired 
    private HorusMapper<ParametroPrecoFilialConverter> mapper;
    		
    @Override
    public ParametroPrecoFilialResponse convertToResponse(ParametroPrecoFilial entity) {
         try {
			return this.mapper.registryMapper(this).convertToResponse(entity);
		} catch (MapperRegistryException | IOException e) {
			e.printStackTrace();
		}
         return null;
    }

    @Override
    public ParametroPrecoFilial convertToModel(ParametroPrecoFilialResponse response) {
        try {
        	return this.mapper.registryMapper(this).convertToModel(response);
		} catch (MapperRegistryException | IOException e) {
			e.printStackTrace();
		}
        return null;
    }

}
