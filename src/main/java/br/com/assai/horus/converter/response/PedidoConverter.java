package br.com.assai.horus.converter.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.contextdomain.entity.TransacaoContextDomainParentComponent;
import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.constant.FormaRetirada;
import br.com.assai.horus.entity.pedido.ItemPedido;
import br.com.assai.horus.entity.pedido.Pedido;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.response.pedido.ItemPedidoResponse;
import br.com.assai.horus.response.pedido.PedidoResponse;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PedidoConverter implements ConverterModelResponse<Pedido, PedidoResponse> {
	
    @Autowired public MapperUtil mapperUtil;
//    @Autowired private FormaPagamentoConverter formaPagamentoConverter;
    @Autowired private ItemPedidoConverter itemPedidoConverter;

    public Long idPedido;
    
    @Override
    public PedidoResponse convertToResponse(Pedido model) throws MapperRegistryException {
        if ( model == null ) {
            return null;
        }

        PedidoResponse response = new PedidoResponse();
        response.setId( model.getId() );
        response.setIdOperacao( model.getIdOperacao() );
        response.setTipoVenda( model.getTipoVenda() );
        response.setTipoFaturamento( model.getTipoFaturamento() );
        response.setClienteNaoIdentificado( model.getClienteNaoIdentificado() );
        response.setNumeroCpfCnpjCupom( model.getNumeroCpfCnpjCupom() );
        response.setIdVendedor( model.getIdVendedor() );
        response.setIdCliente( model.getIdCliente() );
        response.setIdEndereco( model.getIdEndereco() );
        response.setIdFilial( model.getIdFilial() );
        response.setIdPrepedido(model.getIdPrepedido() );
        response.setIdStatus( model.getIdStatus() );
        response.setNumeroPrazoPagamento( model.getNumeroPrazoPagamento() );
        response.setIdPedidoCliente( model.getIdPedidoCliente() );
        response.setFormaRetirada( model.getFormaRetirada().getId());
        response.setDataPrevistaRetirada( model.getDataPrevistaRetirada() );
        if ( model.getIdTransportadora() != null ) {
            response.setIdTransportadora( String.valueOf( model.getIdTransportadora() ) );
        }
        response.setNumeroRgCpfRetirada( model.getNumeroRgCpfRetirada() );
        response.setNomeMotorista( model.getNomeMotorista() );
        response.setTelefoneMotorista( model.getTelefoneMotorista() );
        response.setNomeVeiculo( model.getNomeVeiculo() );
        response.setPlaca( model.getPlaca() );
        response.setDataGeracaoPedido( model.getDataGeracaoPedido() );
        response.setQtdTotalItens( model.getQtdTotalItens() );
        response.setValorTotalDesconto( model.getValorTotalDesconto() );
        response.setPercentutalTotalMargem( model.getPercentutalTotalMargem() );
        response.setValorTotalPedido( model.getValorTotalPedido() );
        response.setObservacaoPedido( model.getObservacaoPedido() );
        response.setObservacaoNota( model.getObservacaoNota() );
        if (model.getTransacao() != null) {
	        response.setDtCadastro( model.getTransacao().getDataCadastro() );
	        response.setDataAlteracao( model.getTransacao().getDataAlteracao() );
	        response.setIdTransacao( model.getTransacao().getTransacaoId() );
	        response.setIdUsuarioCadastro( model.getTransacao().getUsuarioCadastroId() );
	        response.setIdUsuarioAlteracao( model.getTransacao().getUsuarioAlteracaoId() );
        }
        response.setVersao( model.getVersao() );

        return response;
    }

    @Override
    public Pedido convertToModel(PedidoResponse response) throws MapperRegistryException {
        if ( response == null ) {
            return null;
        }

        Pedido entity = new Pedido();

        beforeMapping( response, entity );

        entity.setId( response.getId() );
        entity.setIdOperacao( response.getIdOperacao() );
        entity.setTipoVenda( response.getTipoVenda() );
        entity.setTipoFaturamento( response.getTipoFaturamento() );
        entity.setIdCliente( response.getIdCliente() );
        entity.setClienteNaoIdentificado( response.getClienteNaoIdentificado() );
        entity.setNumeroCpfCnpjCupom( response.getNumeroCpfCnpjCupom() );
        entity.setIdEndereco( response.getIdEndereco() );
//        entity.setFormasPagamento( formaPagamentoConverter.convertListResponseToListModel(response.getFormasPagamento()));
        entity.setIdStatus( response.getIdStatus() );
        entity.setNumeroPrazoPagamento( response.getNumeroPrazoPagamento() );
        entity.setIdPedidoCliente( response.getIdPedidoCliente() );
        entity.setFormaRetirada(FormaRetirada.searchById(response.getFormaRetirada()));
        entity.setDataPrevistaRetirada( response.getDataPrevistaRetirada() );
        entity.setNumeroRgCpfRetirada( response.getNumeroRgCpfRetirada() );
        if ( response.getIdTransportadora() != null ) {
            entity.setIdTransportadora( Long.parseLong( response.getIdTransportadora() ) );
        }
        entity.setNomeMotorista( response.getNomeMotorista() );
        entity.setTelefoneMotorista( response.getTelefoneMotorista() );
        entity.setNomeVeiculo( response.getNomeVeiculo() );
        entity.setPlaca( response.getPlaca() );
        entity.setDataGeracaoPedido( response.getDataGeracaoPedido() );
        entity.setIdFilial( response.getIdFilial() );
        entity.setQtdTotalItens( response.getQtdTotalItens() );
        entity.setValorTotalDesconto( response.getValorTotalDesconto() );
        entity.setPercentutalTotalMargem( response.getPercentutalTotalMargem() );
        entity.setValorTotalPedido( response.getValorTotalPedido() );
        entity.setObservacaoPedido( response.getObservacaoPedido() );
        entity.setObservacaoNota( response.getObservacaoNota() );
        entity.setItens( itemPedidoResponseListToItemPedidoList( response.getItens() ) );
        entity.setIdPrepedido( response.getIdPrepedido() );
        entity.setIdPedidoCliente( response.getIdPedidoCliente() );
        
        if (response.getIdUsuarioCadastro() != null) {
	        TransacaoContextDomainParentComponent transacao = new TransacaoContextDomainParentComponent();
	        transacao.setDataCadastro( response.getDtCadastro() );
	        transacao.setDataAlteracao( response.getDataAlteracao() );
	        transacao.setTransacaoId( response.getIdTransacao() );
	        //entity.setTransacao(transacao);
        }

        entity.setIdVendedor( response.getIdVendedor() );
        entity.setVersao( response.getVersao() );

        afterMapping( response, entity );

        return entity;

    }

    
    protected void beforeMapping(PedidoResponse response, Pedido model) {
        this.idPedido = response.getId();
    }
    
    protected  void afterMapping(PedidoResponse response, Pedido model) {
    	if (model.getTransacao() != null) {
    		model.getTransacao().setUsuarioCadastroId(response.getIdUsuarioCadastro());
    		model.getTransacao().setUsuarioAlteracaoId(response.getIdUsuarioAlteracao());
    		
    	}

//    	if (model.getFormasPagamento() != null) {
//    		model.getFormasPagamento().stream().forEach(f -> f.setIdPedido(idPedido));
//    	}

    	if (model.getItens() != null) {
    		model.getItens().stream().forEach(i -> i.setIdPedido(idPedido));
    	}
    }
    


    protected List<ItemPedido> itemPedidoResponseListToItemPedidoList(List<ItemPedidoResponse> list) {
        if ( list == null ) {
            return null;
        }

        List<ItemPedido> list1 = new ArrayList<ItemPedido>( list.size() );
        for ( ItemPedidoResponse itemPedidoResponse : list ) {
            list1.add( itemPedidoConverter.convertToModel( itemPedidoResponse ) );
        }

        return list1;
    }    


}
