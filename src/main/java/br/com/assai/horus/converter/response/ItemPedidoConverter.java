package br.com.assai.horus.converter.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.contextdomain.entity.TransacaoContextDomainChildComponent;
import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.pedido.ItemPedido;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.response.pedido.ItemPedidoResponse;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ItemPedidoConverter implements ConverterModelResponse<ItemPedido, ItemPedidoResponse> {
    
    public ItemPedidoResponse convertToResponse(ItemPedido model) throws MapperRegistryException {
        if ( model == null ) {
            return null;
        }

        ItemPedidoResponse ItemPedidoResponse = new ItemPedidoResponse();
        ItemPedidoResponse.setId( model.getId() );
        ItemPedidoResponse.setIdPedido( model.getIdPedido() );
        ItemPedidoResponse.setNegociacaoPontual(model.getNegociacaoPontual());
        ItemPedidoResponse.setIdProdutoFilialPreco(model.getIdProdutoFilialPreco());
        ItemPedidoResponse.setIdProduto( model.getIdProduto() );
        ItemPedidoResponse.setMargem(model.getMargem());
        ItemPedidoResponse.setIdEmbalagem( model.getIdEmbalagem() );
        ItemPedidoResponse.setNegociacaoPontual( model.getNegociacaoPontual() );
        ItemPedidoResponse.setNumeroItem( model.getNumeroItem() );
        ItemPedidoResponse.setPrecoNegociadoUnit( model.getPrecoNegociadoUnit() );
        ItemPedidoResponse.setPrecoNegociadoEmb( model.getPrecoNegociadoEmb() );
        ItemPedidoResponse.setQtdDevolvida( model.getQtdDevolvida() );
        ItemPedidoResponse.setQtdNegociada( model.getQtdNegociada() );
        ItemPedidoResponse.setQtdRetirada( model.getQtdRetirada() );
        ItemPedidoResponse.setQtdSeparacao( model.getQtdSeparacao() );
        ItemPedidoResponse.setStatusSeparacao( model.getStatusSeparacao() );
        ItemPedidoResponse.setValorDesconto( model.getValorDesconto() );
        ItemPedidoResponse.setValorDespesa( model.getValorDespesa() );
        ItemPedidoResponse.setValorImposto( model.getValorImposto() );
        ItemPedidoResponse.setValorPrecoVigente( model.getValorPrecoVigente() );
        ItemPedidoResponse.setValorTotalProduto( model.getValorTotalProduto() );
        ItemPedidoResponse.setValorVerba( model.getValorVerba() );
        if (model.getTransacao() != null) {
	        ItemPedidoResponse.setIdTransacao( model.getTransacao().getTransacaoId() );
        }

        return ItemPedidoResponse;
    }

    public List<ItemPedidoResponse> listModelToListResponse(List<ItemPedido> ItemPedido) throws MapperRegistryException {
        if ( ItemPedido == null ) {
            return null;
        }

        List<ItemPedidoResponse> list = new ArrayList<ItemPedidoResponse>( ItemPedido.size() );
        for ( ItemPedido ItemPedido1 : ItemPedido ) {
            list.add( this.convertToResponse( ItemPedido1 ) );
        }

        return list;
    }

    public ItemPedido convertToModel(ItemPedidoResponse response) {
        if ( response == null ) {
            return null;
        }

        ItemPedido itemPedido = new ItemPedido();

        itemPedido.setId( response.getId() );
        itemPedido.setIdPedido( response.getIdPedido() );
        itemPedido.setNumeroItem( response.getNumeroItem() );
        itemPedido.setIdProdutoFilialPreco( response.getIdProdutoFilialPreco() );
        itemPedido.setMargem(response.getMargem());
        if (response.getPrecoNegociadoEmb() != null) {
        	itemPedido.setValorPrecoVigente(response.getPrecoNegociadoEmb());
        } else {
        	itemPedido.setValorPrecoVigente(response.getPrecoEmbalagemProduto());
        }
        itemPedido.setIdProduto( response.getIdProduto() );
        itemPedido.setIdEmbalagem( response.getIdEmbalagem() );
        itemPedido.setValorPrecoVigente( response.getValorPrecoVigente() );
        itemPedido.setQtdNegociada( response.getQtdNegociada() );
        itemPedido.setPrecoNegociadoUnit( response.getPrecoNegociadoUnit() );
        itemPedido.setPrecoNegociadoEmb( response.getPrecoNegociadoEmb() );
        itemPedido.setValorImposto( response.getValorImposto() );
        itemPedido.setValorDespesa( response.getValorDespesa() );
        itemPedido.setValorVerba( response.getValorVerba() );
        itemPedido.setValorTotalProduto( response.getValorTotalProduto() );
        itemPedido.setValorDesconto( response.getValorDesconto() );
        itemPedido.setNegociacaoPontual( response.getNegociacaoPontual() );
        itemPedido.setStatusSeparacao( response.getStatusSeparacao() );
        itemPedido.setQtdSeparacao( response.getQtdSeparacao() );
        itemPedido.setQtdRetirada( response.getQtdRetirada() );
        itemPedido.setQtdDevolvida( response.getQtdDevolvida() );
        if (response.getIdTransacao() != null) {
        	TransacaoContextDomainChildComponent transacao = new TransacaoContextDomainChildComponent();
        	transacao.setTransacaoId(response.getIdTransacao());
        	itemPedido.setTransacao(transacao);
        }
        return itemPedido;
    }

}
