package br.com.assai.horus.converter.response;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.dependencies.RegionalDependency;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.mapper.HorusMapper;
import br.com.assai.horus.response.dependency.RegionalDependencyResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RegionalFilialConverter implements ConverterModelResponse<RegionalDependency, RegionalDependencyResponse>{
    
	@Autowired
	private HorusMapper<RegionalFilialConverter> horusMapper;
	
	@Override
	public RegionalDependencyResponse convertToResponse(RegionalDependency entity) throws MapperRegistryException {
		try {
			return horusMapper.registryMapper(this).convertToResponse(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public RegionalDependency convertToModel(RegionalDependencyResponse entity) throws MapperRegistryException {
		try {
			return horusMapper.registryMapper(this).convertToModel(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
