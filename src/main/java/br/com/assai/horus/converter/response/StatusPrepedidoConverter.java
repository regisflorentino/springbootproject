package br.com.assai.horus.converter.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.prepedido.StatusTelevendas;
import br.com.assai.horus.response.prepedido.StatusPrepedidoResponse;

@Service
public class StatusPrepedidoConverter implements ConverterModelResponse<StatusTelevendas, StatusPrepedidoResponse> {

	@Autowired private StatusPrepedidoConverterMapper mapper;

	@Override
	public StatusTelevendas convertToModel(StatusPrepedidoResponse response) {
		return mapper.responseToModel(response);
	}	
	
	@Override
	public StatusPrepedidoResponse convertToResponse(StatusTelevendas entity) {
		return mapper.modelToResponse(entity);
		
		
	}

}