package br.com.assai.horus.converter.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.response.prepedido.FormaPagamentoResponse;
import br.com.assai.horus.response.prepedido.PrazosPagamentoBoletoResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FormaPagamentoConverter implements ConverterModelResponse<FormaPagamento, FormaPagamentoResponse>{
	
	
//	@Autowired private FormaPagamentoAnexoConverter formaPagamentoAnexoConverter;

	@Override
	public FormaPagamento convertToModel(FormaPagamentoResponse response) throws MapperRegistryException {
		FormaPagamento entity = null;
		if (response != null) {
			entity = copyPropertiesResponseToModel(entity, response);
		}
		return entity;	
	}

	public FormaPagamento copyPropertiesResponseToModel(FormaPagamento entity, FormaPagamentoResponse response)  {
		if(entity == null) {
			entity = new FormaPagamento();
		}
		entity.setId(response.getId());
		entity.setNome(response.getNome());
		entity.setNumeroDeposito(response.getNumeroDeposito());
		entity.setCodigo(response.getCodigo());
		entity.setIdPrepedido(response.getIdPrepedido());
		entity.setIdPedido(response.getIdPedido());
		if(response.getPrazo() != null) {
			entity.setPrazo(response.getPrazo().getIdPrazoPagamento());	
		}
//		try {
////			entity.setAnexos(formaPagamentoAnexoConverter.convertListResponseToListModel(response.getAnexos()));
////			for (FormaPagamentoAnexo anexo : entity.getAnexos()) {
////				anexo.setFormaPagamento(entity);
////			}
//		} catch (MapperRegistryException e) {
//			e.printStackTrace();
//		}
		return entity;
	}

	@Override
	public FormaPagamentoResponse convertToResponse(FormaPagamento entity) throws MapperRegistryException {
		FormaPagamentoResponse response = null;
		if (entity != null) {
			response = new FormaPagamentoResponse();
		    copyPropertiesModelToResponse(entity, response);			
		}
		return response;	
	}

	public void copyPropertiesModelToResponse(FormaPagamento entity, FormaPagamentoResponse response) {
		if (response != null && entity != null) {
			response.setId(entity.getId());
			response.setNome(entity.getNome());
			response.setNumeroDeposito(entity.getNumeroDeposito());
			response.setCodigo(entity.getCodigo());
			response.setIdPrepedido(entity.getIdPrepedido());
			response.setIdPedido(entity.getIdPedido());
			if(response.getPrazo() == null) {
				PrazosPagamentoBoletoResponse prazo = new PrazosPagamentoBoletoResponse();
				prazo.setIdPrazoPagamento(entity.getPrazo());
				response.setPrazo(prazo);
			}
		}
	}
	
	public List<FormaPagamento> convertListResponseToListModel(List<FormaPagamentoResponse> responses) throws MapperRegistryException {
		List<FormaPagamento> result = new ArrayList<>();
		if (responses != null) {
			for (FormaPagamentoResponse response : responses) {
				result.add(this.convertToModel(response));
			}
		}
		return result;
	}

	public List<FormaPagamentoResponse> convertListModelToListResponse(List<FormaPagamento> entities) throws MapperRegistryException {
		List<FormaPagamentoResponse> result = new ArrayList<>();
		if (entities != null) {
			for (FormaPagamento entity : entities) {
				result.add(this.convertToResponse(entity));
			}
		}
		return result;
	}	
}
