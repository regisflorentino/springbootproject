package br.com.assai.horus.converter.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.ParamentroTipoVenda;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.response.ParamentroTipoVendaResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParamentroTipoVendaConverter implements ConverterModelResponse<ParamentroTipoVenda, ParamentroTipoVendaResponse> {
    
	@Autowired
	private TelevendaConverterUtils televendaConverterUtils;

	@Override
	public ParamentroTipoVendaResponse convertToResponse(ParamentroTipoVenda entity) throws MapperRegistryException {
		ParamentroTipoVendaResponse response = null;
		if (entity != null) {
			response = new ParamentroTipoVendaResponse();
			
			response.setId                  (entity.getId()                  );
			response.setIdFilial            (entity.getIdFilial()            );
			response.setVendaExportacao     (entity.getVendaExportacao()     );
			response.setVendaExportacaoPdv  (entity.getVendaExportacaoPdv()  );
			response.setVendaInterna        (entity.getVendaInterna()        );
			response.setVendaInternaPdv     (entity.getVendaInternaPdv()     );
			response.setVendaInterstadual   (entity.getVendaInterstadual()   );
			response.setVendaInterstadualPdv(entity.getVendaInterstadualPdv());
			response.setVersao              (entity.getVersao()              );

			
			response.setDataAlteracao(entity.getDataAlteracao());
			response.setDataCadastro(entity.getDataCadastro());
			if (entity.getUsuarioAlteracao() != null) {
				response.setIdUsuarioAlteracao(entity.getUsuarioAlteracao().getId());
				response.setNomeUsuarioAlteracao(entity.getUsuarioAlteracao().getNome());
			}
			if (entity.getUsuarioCadastro() != null) {
				response.setIdUsuarioCadastro(entity.getUsuarioCadastro().getId());
				response.setNomeUsuarioCadastro(entity.getUsuarioCadastro().getNome());
			}
			
		}
		return response;
	}

	@Override
	public ParamentroTipoVenda convertToModel(ParamentroTipoVendaResponse response) throws MapperRegistryException {
		ParamentroTipoVenda entity = null;
		if (response != null) {
			entity = new ParamentroTipoVenda();
			
			entity.setId                  (response.getId()                  );
			entity.setIdFilial            (response.getIdFilial()            );
			entity.setVendaExportacao     (response.getVendaExportacao()     );
			entity.setVendaExportacaoPdv  (response.getVendaExportacaoPdv()  );
			entity.setVendaInterna        (response.getVendaInterna()        );
			entity.setVendaInternaPdv     (response.getVendaInternaPdv()     );
			entity.setVendaInterstadual   (response.getVendaInterstadual()   );
			entity.setVendaInterstadualPdv(response.getVendaInterstadualPdv());
			entity.setVersao              (response.getVersao()              );

			
			entity.setDataAlteracao(response.getDataAlteracao());
			entity.setDataCadastro(response.getDataCadastro());
			entity.setUsuarioAlteracao(televendaConverterUtils.getUsuario(response.getIdUsuarioAlteracao()).orElse(null));
			entity.setUsuarioCadastro(televendaConverterUtils.getUsuario(response.getIdUsuarioCadastro()).orElse(null));
			
		}
		return entity;
	}
	
}
