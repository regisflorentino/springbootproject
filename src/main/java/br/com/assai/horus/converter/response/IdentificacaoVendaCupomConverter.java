package br.com.assai.horus.converter.response;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.IdentificacaoVendaCupom;
import br.com.assai.horus.entity.dependencies.FilialDependency;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.mapper.HorusMapper;
import br.com.assai.horus.repository.dependencies.FilialRepository;
import br.com.assai.horus.response.IdentificacaoVendaCupomResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IdentificacaoVendaCupomConverter  implements ConverterModelResponse<IdentificacaoVendaCupom, IdentificacaoVendaCupomResponse> {
    

	@Autowired
	private HorusMapper<IdentificacaoVendaCupomConverter> mapper;

	@Autowired
	private TelevendaConverterUtils televendaConverterUtils;
		
	@Override
	public IdentificacaoVendaCupomResponse convertToResponse(IdentificacaoVendaCupom entity) throws MapperRegistryException {
		IdentificacaoVendaCupomResponse response = null;
		try {
			response = this.mapper.registryMapper(this).convertToResponse(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public IdentificacaoVendaCupom convertToModel(IdentificacaoVendaCupomResponse response) throws MapperRegistryException {
		IdentificacaoVendaCupom entity = null;
		try {
			entity = this.mapper.registryMapper(this).convertToModel(response);
			entity.setUsuarioCadastro(this.televendaConverterUtils.getUsuario(response.getIdUsuarioCadastro()).orElse(null));
			entity.setUsuarioAlteracao(this.televendaConverterUtils.getUsuario(response.getIdUsuarioAlteracao()).orElse(null));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return entity;
	}
}
