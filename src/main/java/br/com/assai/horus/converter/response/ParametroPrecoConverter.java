package br.com.assai.horus.converter.response;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.assai.horus.converter.ConverterModelResponse;
import br.com.assai.horus.entity.parametropreco.ParametroPreco;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.mapper.HorusMapper;
import br.com.assai.horus.response.parametropreco.ParametroPrecoResponse;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParametroPrecoConverter implements ConverterModelResponse<ParametroPreco, ParametroPrecoResponse> {

    @Autowired 
    private HorusMapper<ParametroPrecoConverter> mapper;
    		
    @Override
    public ParametroPrecoResponse convertToResponse(ParametroPreco entity) {
        ParametroPrecoResponse response = null;
         try {
			response = this.mapper.registryMapper(this).convertToResponse(entity);
		} catch (MapperRegistryException | IOException e) {
			e.printStackTrace();
		}
         return response;
    }

    @Override
    public ParametroPreco convertToModel(ParametroPrecoResponse response) {
        ParametroPreco model = null;
        try {
        	model = this.mapper.registryMapper(this).convertToModel(response);
		} catch (MapperRegistryException | IOException e) {
			e.printStackTrace();
		}
        return model;
    }

}
