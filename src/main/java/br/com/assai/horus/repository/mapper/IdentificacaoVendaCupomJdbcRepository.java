package br.com.assai.horus.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.assai.horus.response.IdentificacaoVendaCupomResponse;

public interface IdentificacaoVendaCupomJdbcRepository {
	
	List<IdentificacaoVendaCupomResponse> listAll();

	IdentificacaoVendaCupomResponse findByFilial(@Param("idFilial") Long idFilial);
}
