package br.com.assai.horus.repository.formapagamento;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.prepedido.FormaPagamento;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface FormaPagamentoRepository extends JpaRepository<FormaPagamento, Long>{
	
	List<FormaPagamento> findByIdPedido(Long idPedido);
	List<FormaPagamento> findByIdPrepedido(Long idPrepedido);

	
}
