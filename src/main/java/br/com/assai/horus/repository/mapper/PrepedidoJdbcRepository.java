package br.com.assai.horus.repository.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.assai.horus.response.prepedido.CupomResponse;
import br.com.assai.horus.response.prepedido.LimiteCreditoClienteResponse;
import br.com.assai.horus.response.prepedido.NotaResponse;
import br.com.assai.horus.response.prepedido.PagamentosResponse;
import br.com.assai.horus.response.prepedido.PrazosPagamentoBoletoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoResponse;
import br.com.assai.horus.response.prepedido.PrepedidoWorkflowResponse;

public interface PrepedidoJdbcRepository {

    Long getNextId();
    
    Long getFormaPagamentoNextId();
    
    String getPathProd(@Param("id") Long id);
    
    LimiteCreditoClienteResponse getLimiteCreditoByIdPrepedido(@Param("id") Long id);

    List<PrepedidoResponse> findByFilter(@Param("idFilial") Long idFilial, 
        @Param("dataInicial") Date dataInicial, @Param("dataFinal") Date dataFinal,
        @Param("produtos") List<Long> produtos, @Param("flgExcluidos") boolean flgExcluidos);    
    
    Long countByFilter(@Param("idFilial") Long idFilial, 
            @Param("dataInicial") Date dataInicial, @Param("dataFinal") Date dataFinal,
            @Param("produtos") List<Long> produtos, @Param("flgExcluidos") boolean flgExcluidos);    
    
    Long getItemNextId();
    
    Long getBloqueioPrecoGerente(@Param("idFilial") Long idFilial,@Param("idProd") Long idProd);

	List<CupomResponse> findFaturamentosCupom(@Param("idPedidoCapa") Long idPedidoCapa);
	
	NotaResponse findFaturamentosNota(@Param("idPedidoCapa") Long idPedidoCapa);

	List<PagamentosResponse> findPagamentos(@Param("idPedidoCapa") Long idPedidoCapa);
	
	List<PrazosPagamentoBoletoResponse> getPrazosPagamentoBoleto();
	
	List<PrepedidoWorkflowResponse> getPrepedidoWorkflows(@Param("status") String status);
	
	void alloc(Date date);
    
	Long getSequenceValue(Date date);

	List<Long> getIdWkfDetalhePrepedido(@Param("idPrepedido") Long idPrepedido);

	PrepedidoWorkflowResponse getPrepedidoWorkflowResponse(@Param("idPrepedido") Long idPrepedido);

	String getNomeVendedor(Long idVendedor);

	void updateStatusWorkflow(@Param("idWorkflow") Long idWorkflow, @Param("status")  String status); 

	String getStatusTelevendasById(@Param("idStatus") Long idStatus); 

	void updatePrepedido(@Param("idPrepedido") Long idPrepedido, @Param("idPedido") Long idPedido);

	Long getIdPedidoByIdPrepedido(@Param("idPrepedido") Long idPrepedido);

	int  getPeriodoValidadePedidoParam(@Param("id_parametro") int id_parametro);

	List<PrepedidoResponse> prepedidosRefresh(@Param("idFilial") Long idFilial , @Param("idPrepedidoLast")  Long idPrepedidoLast);

	Long deleteSequenceValue(@Param("idPedido") long idPedido);
	
}
