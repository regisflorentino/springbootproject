package br.com.assai.horus.repository.prepedido;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;
import br.com.assai.horus.entity.prepedido.ItemPrepedido;
import br.com.assai.horus.entity.prepedido.Prepedido;


@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PrepedidoRepository  extends JpaRepository<Prepedido, Long> {

    @Query(value="SELECT p.formasPagamento FROM Prepedido p WHERE p.id = :id")
    List<FormaPagamento> getFormasPagamentoByIdPedido(@Param("id") long id);
    
    @Query(value="SELECT f FROM FormaPagamentoAnexo f WHERE f.formaPagamento.id = :id")
    List<FormaPagamentoAnexo> getFormasPagamentoAnexoByIdFormaPagamento(@Param("id") long id);
    
    @Query(value="SELECT * FROM ("
    		+ "SELECT DT_CADASTRO AS dataCadastro,ID_PEDIDO_CAPA AS id, VL_TOTAL_PEDIDO AS valorTotalPedido, ID_FILIAL AS idFilial, DT_GERACAO_PEDIDO AS dataGeracaoPedido"
    		+ " FROM TVD_PEDIDO_CAPA p"  
    		+ " JOIN (SELECT DISTINCT ID_PEDIDO_CAPA AS CAPA_ID"  
    		+ "		FROM TVD_PEDIDO_NOTA) pn ON p.ID_PEDIDO_CAPA = pn.CAPA_ID"  
    		+ " WHERE p.ID_CLIENTE = :idCliente"  
    		+ " UNION"  
    		+ " SELECT DT_CADASTRO AS dataCadastro, ID_PEDIDO_CAPA AS id, VL_TOTAL_PEDIDO AS valorTotalPedido, ID_FILIAL AS idFilial, DT_GERACAO_PEDIDO AS dataGeracaoPedido"  
    		+ " FROM TVD_PEDIDO_CAPA p"  
    		+ " JOIN (SELECT DISTINCT ID_PEDIDO_CAPA AS CAPAC_ID"  
    		+ "		FROM TVD_PEDIDO_CUPOM) pc ON p.ID_PEDIDO_CAPA = pc.CAPAC_ID"  
    		+ " WHERE p.ID_CLIENTE = :idCliente  ORDER BY dataGeracaoPedido DESC ) WHERE ROWNUM < 4" , nativeQuery=true)
    List<ResumoPedido> getPedidosByIdCliente(@Param("idCliente") long idCliente);

    @Query(value="SELECT i FROM ItemPrepedido i WHERE i.idPrepedido = :id")
    List<ItemPrepedido> getItensPrepedidoByIdPredido(@Param("id") Long id);
    
    @Query(value="SELECT NO_DIAS_PAGAMENTO AS dias"
    		+ " FROM CAM_PRAZO_PAGAMENTO"
    		+ " WHERE ID_PRAZO_PAGAMENTO = :numeroPrazoPagto", nativeQuery=true)
     Long getPrazoPagamento(@Param("numeroPrazoPagto") Long numeroPrazoPagto);
    
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value="UPDATE TVD_PREPEDIDO_CAPA SET ID_STATUS = 2 WHERE ID_PREPEDIDO_CAPA = :id", nativeQuery=true)
     void cancelarPrepedido(Long id);
    
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value="UPDATE TVD_PREPEDIDO_CAPA SET ID_STATUS = :idStatus, ID_DETALHE_WKF = NULL WHERE ID_PREPEDIDO_CAPA = :id", nativeQuery=true)
     void reprovarPrepedido(Long id, Long idStatus);
    
    
    interface ResumoPedido {
    	long getId();
    	long getIdFilial();
    	String getDataCadastro();
    	String getDataGeracaoPedido();
    	Double getValorTotalPedido();
    }
    
    
}
