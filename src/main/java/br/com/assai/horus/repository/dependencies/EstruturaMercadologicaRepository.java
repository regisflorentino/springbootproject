package br.com.assai.horus.repository.dependencies;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface EstruturaMercadologicaRepository extends JpaRepository<EstruturaMercadologicaDependency, Long>{
    
	List<EstruturaMercadologicaDependency> findByNivelPai(EstruturaMercadologicaDependency nivelPai);

}
