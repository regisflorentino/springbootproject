package br.com.assai.horus.repository.pedido;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.assai.horus.entity.pedido.ItemPedido;

public interface ItemPedidoRespository extends JpaRepository<ItemPedido, Long> {
	
	 List<ItemPedido> findByidPedido(Long idPedido);		

}
