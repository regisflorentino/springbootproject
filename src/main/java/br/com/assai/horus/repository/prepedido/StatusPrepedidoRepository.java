package br.com.assai.horus.repository.prepedido;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.prepedido.StatusTelevendas;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface StatusPrepedidoRepository extends JpaRepository<StatusTelevendas, Long> {

}
