package br.com.assai.horus.repository.dependencies;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.dependencies.FilialDependency;


@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface FilialRepository extends JpaRepository<FilialDependency, Long>{
    
    
    @Query(value="SELECT ID_FILIAL FROM ADM_USUARIO_FILIAL where ID_USUARIO =  :idUsuario", nativeQuery=true)
    public List<Object> getIdFiliaisPermitidasUsuarioPorIdUsuario(@Param("idUsuario") Long idUsuario);
    


}
