package br.com.assai.horus.repository.prepedido;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.assai.horus.entity.prepedido.ItemPrepedido;

public interface ItemPrepedidoRespository extends JpaRepository<ItemPrepedido, Long> {

	List<ItemPrepedido> findByidPrepedido(Long idPrepedido);

}
