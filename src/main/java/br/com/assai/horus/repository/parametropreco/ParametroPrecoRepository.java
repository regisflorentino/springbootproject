package br.com.assai.horus.repository.parametropreco;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.parametropreco.ParametroPreco;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ParametroPrecoRepository extends JpaRepository<ParametroPreco, Long> {
	
	@Query(value="SELECT pp.pc_reducao precoReducao, pp.pc_acrescimo  precoAcrescimo, pp.fg_sem_acrescimo semAcrescimo, pp.fg_sem_reducao semReducao, pp.id_nivel idNivel, pp.id_produto idProduto "
    		+ "FROM TVD_PARAM_PRECO_PRODUTO pp " + 
    		"INNER JOIN  TVD_PARAM_PRECO_FILIAL pf ON pp.id_param_preco_filial = pf.id_param_preco_filial AND pf.id_filial = :idFilial " + 
    		"WHERE pp.id_produto = :idProd or pp.id_nivel = :idProd", nativeQuery=true)
    public DadosPreco findByIdProdORIdEM(@Param("idProd") Long idProd, @Param("idFilial") Long idFilial);
    
    @Query(value="SELECT pf.pc_reducao precoReducao, pf.pc_acrescimo precoAcrescimo, pf.fg_sem_acrescimo semAcrescimo, pf.fg_sem_reducao semReducao "
    		+ "FROM TVD_PARAM_PRECO_FILIAL pf " + 
    		"WHERE pf.id_filial = :idFilial AND pf.pc_reducao is not null AND pf.pc_acrescimo is not null ", nativeQuery=true)
    public DadosPreco findParanFilialByIdFilial(@Param("idFilial") Long idFilial);
    
    interface DadosPreco {
    	BigDecimal getPrecoReducao();
    	BigDecimal getPrecoAcrescimo();
    	@Value("#{target.semReducao == 1 ? true : false}")
    	Boolean getSemAcrescimo();
    	@Value("#{target.semAcrescimo == 1 ? true : false}")
    	Boolean getSemReducao();
    	Long getIdNivel();
    	Long getIdProduto();
    }
    
}