package br.com.assai.horus.repository.parametrotipovenda;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.ParamentroTipoVenda;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ParamentroTipoVendaRepository extends JpaRepository<ParamentroTipoVenda, Long> {
	
	ParamentroTipoVenda findByIdFilial(Long idFilial);
}
