package br.com.assai.horus.repository.pedido;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.assai.horus.entity.pedido.Pedido;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PedidoRepository extends JpaRepository<Pedido, Long>{

	@Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
	@Query(value="UPDATE TVD_PREPEDIDO_CAPA SET ID_PEDIDO = :idPedido, ID_STATUS = 11 WHERE ID_PREPEDIDO_CAPA = :idPrepedido", nativeQuery = true)
	void atualizaPrepedidoComIdPedido(Long idPedido, Long idPrepedido);
	
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
	@Query(value="UPDATE TVD_FORMA_PAGAMENTO SET ID_PEDIDO_CAPA = :idPedido WHERE ID_PREPEDIDO_CAPA = :idPrepedido AND ID_FORMA_PAGAMENTO IN (:idsFormas)", nativeQuery = true)
	void atualizaFormaPagamentoComIdPedido(Long idPedido, Long idPrepedido, List<Long> idsFormas);
	
}
