package br.com.assai.horus.repository.parametropreco;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.parametropreco.ParametroPrecoProduto;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ParametroPrecoProdutoRepository extends JpaRepository<ParametroPrecoProduto, Long> {
    boolean deleteByIdParamPrecoFilial(Long idParamPrecoFilial);
}
