package br.com.assai.horus.repository.limitecredito;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.assai.horus.entity.prepedido.LimiteCreditoCliente;

public interface LimiteCreditoClienteRepository extends JpaRepository<LimiteCreditoCliente, Long> {

}
