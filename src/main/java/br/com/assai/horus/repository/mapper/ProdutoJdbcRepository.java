package br.com.assai.horus.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.assai.horus.response.dependency.ProdutoDependencyResponse;
import br.com.assai.horus.response.prepedido.ItemPrepedidoResponse;

public interface ProdutoJdbcRepository {
    
    List<ProdutoDependencyResponse> findValidProdutoToTelevendasByDescricaoCompletaAndFilial(@Param("produto") String produto, 
                                                                                             @Param("idFilial") Long idFilial,
                                                                                             @Param("isNumber") boolean isNumber);
    
    List<ProdutoDependencyResponse> findValidProdutoToTelevendasByDescricaoCompleta(@Param("produto") String produto,
                                                                                    @Param("isNumber") boolean isNumber);
    
    List<ItemPrepedidoResponse> findDadosItemPrePedidoDeProdutoValidoComPrecoAndEstoqueInFilial(@Param("idProduto") Long idProduto, 
                                                                           @Param("idFilial") Long idFilial);


}
