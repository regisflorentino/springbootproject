package br.com.assai.horus.repository.mapper;

import java.util.List;

import br.com.assai.horus.response.ParamentroTipoVendaResponse;

public interface ParamentroTipoVendaJdbcRepository {
	
	List<ParamentroTipoVendaResponse> listAll();

}
