package br.com.assai.horus.repository.identificacaoVendaCupom;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.IdentificacaoVendaCupom;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface IdentificacaoVendaCupomRepository extends JpaRepository<IdentificacaoVendaCupom, Long> {

}
