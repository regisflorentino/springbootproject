package br.com.assai.horus.repository.dependencies.produto;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.dependencies.ProdutoDependency;


@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ProdutoRepository extends JpaRepository<ProdutoDependency, Long> {

}
