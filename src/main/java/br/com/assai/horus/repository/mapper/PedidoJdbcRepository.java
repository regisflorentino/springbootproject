package br.com.assai.horus.repository.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.assai.horus.response.pedido.ItemPedidoResponse;
import br.com.assai.horus.response.pedido.PedidoResponse;
import br.com.assai.horus.response.pedido.mapa.separacao.ItemPedidoMapaSeparacaoResponse;

public interface PedidoJdbcRepository {

	Long getNextId();
	List<PedidoResponse> findByFilter(@Param("idFilial") Long idFilial, @Param("dataInicial") Date dataInicial, @Param("dataFinal") Date dataFinal,
	        @Param("idsProduto") List<Long> produtos, @Param("flgExcluidos") boolean flgExcluidos);
	
	PedidoResponse findByIdPedidoCapa(@Param("idPedido") Long idPedido);
	List<ItemPedidoMapaSeparacaoResponse> findMapaSeparacao(@Param("idPedido") Long idPedido);
	List<ItemPedidoMapaSeparacaoResponse> termoRetirada(@Param("idPedido") Long idPedido);
	void cancelarPedido(@Param("idPedido") Long idPedido, @Param("dataCancelamento")  Date dataCancelamento);
	String getNomeVendedor(@Param("idVendedor")  long idVendedor);
	
}
