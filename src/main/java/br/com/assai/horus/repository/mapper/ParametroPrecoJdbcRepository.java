package br.com.assai.horus.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import br.com.assai.horus.response.parametropreco.ParametroPrecoResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoFilialResponse;
import br.com.assai.horus.response.parametropreco.ParametroPrecoProdutoResponse;

public interface ParametroPrecoJdbcRepository {
    
    List<ParametroPrecoResponse> listAll();

    List<ParametroPrecoFilialResponse> listParametroPrecoFiliais(@Param("idParametroPreco") Long idParametroPreco);

    List<ParametroPrecoFilialResponse> verififyParametroPrecoFilial(@Param("idFilial") Long idFilial);
    
    List<ParametroPrecoProdutoResponse> listParametroPrecoProdutos(@Param("idParametroPrecoFilial") Long idParametroPrecoFilial);
    
    List<ParametroPrecoProdutoResponse> listParametroPrecoProdutosByIdProdIdFilial(@Param("idFilial") Long idFilial, @Param("idProd") Long idProd);
    
    String checkNomeGrupo(@Param("nomeGrupo") String nomeGrupo);

}
