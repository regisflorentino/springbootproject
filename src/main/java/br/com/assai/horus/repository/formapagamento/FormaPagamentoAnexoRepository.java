package br.com.assai.horus.repository.formapagamento;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assai.horus.entity.prepedido.FormaPagamento;
import br.com.assai.horus.entity.prepedido.FormaPagamentoAnexo;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface FormaPagamentoAnexoRepository extends JpaRepository<FormaPagamentoAnexo, Long>{
	
	List<FormaPagamentoAnexo> findByFormaPagamento(FormaPagamento forma);

}
