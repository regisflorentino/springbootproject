package br.com.assai.horus.iis;

import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.assai.horus.application.TelevendasStartApplication;
import br.com.assai.horus.integracao.PedidoTVDIntegracao;

//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = TelevendasStartApplication.class)
public class PedidoTVDIntegracaoBuildUrlTest {

	@Autowired
	private PedidoTVDIntegracao service;
	
//	@Test
//	public void testUriIntegracao() {
//		validaURIs("integracaoURI", "localhost:9098/IIS/integracao/pedidotvd/integrar");
//	}
//
//	@Test
//	public void testUriValidacao() {
//		validaURIs("validacaoURI", "localhost:9098/IIS/integracao/pedidotvd/validar");
//	}
	
	private void validaURIs(String nomeMetodoProtegido, String urlEsperada) {
		try {
			Method integracaoURI = PedidoTVDIntegracao.class.getDeclaredMethod(nomeMetodoProtegido);
			integracaoURI.setAccessible(true);
			Object retornoDoMetodo = integracaoURI.invoke(this.service);
			Assert.assertEquals(urlEsperada, retornoDoMetodo.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
}
