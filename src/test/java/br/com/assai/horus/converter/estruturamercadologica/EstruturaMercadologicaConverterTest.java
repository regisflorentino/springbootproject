package br.com.assai.horus.converter.estruturamercadologica;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.assai.horus.converter.response.EstruturaMercadologicaConverter;
import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency;
import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency.EstruturaMercadologicaDependencyBuilder;
import br.com.assai.horus.entity.dependencies.ProdutoDependency;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.mapper.JsonMapperConveter;
import br.com.assai.horus.response.dependency.EstruturaMercadologicaDependencyResponse;
import br.com.assai.horus.utils.AssertConverterUtils;
import br.com.assai.horus.utils.JsonNodeConverter;
import net.vidageek.mirror.dsl.Mirror;

@RunWith(MockitoJUnitRunner.class)
public class EstruturaMercadologicaConverterTest {

	@InjectMocks
	private JsonNodeConverter jsonNodeConverter;
	
	@InjectMocks
	private EstruturaMercadologicaConverter estruturaMercadologicaConverter;

	@InjectMocks  
	private JsonMapperConveter<EstruturaMercadologicaDependency, EstruturaMercadologicaDependencyResponse> estuturaMercadologicaMapper;

	@Before
	public void before() {
		Mirror mirror = new Mirror();
		mirror.on(this.estuturaMercadologicaMapper).set().field("converter").withValue(jsonNodeConverter);
		mirror.on(this.estruturaMercadologicaConverter).set().field("mapper").withValue(estuturaMercadologicaMapper);
		
	}

	public List<ProdutoDependency> buildProdutos(EstruturaMercadologicaDependency estruturaMercadologica, Long ... idsProduto) {
		List<ProdutoDependency> list = new ArrayList<>();
		for (Long long1 : idsProduto) {
			list.add(
					ProdutoDependency.builder().id(long1)
					.codigo(long1).descricaoBasica("DESCRICAO" + long1)
					.descricaoReduzida("Reduzida" + long1)
					.descricaoBasica("basica" + long1)
					.estruturaMercadologica(estruturaMercadologica).build());
		}
		return list;
	}
	
	public EstruturaMercadologicaDependencyBuilder builder(Long id, EstruturaMercadologicaDependency pai, List<EstruturaMercadologicaDependency> subniveis) {
		return EstruturaMercadologicaDependency.builder()
				.id(id).codigo("CODIGO").nome("Nome")
				.descricao("DESCRICAO").nivelPai(pai)
				.subniveis(subniveis).ordem(id.intValue())
				.ativo(true).produtos(null);
		
	}

	private EstruturaMercadologicaDependencyResponse buildResponseWithSubniveis(EstruturaMercadologicaDependency model)
			throws MapperRegistryException {
		EstruturaMercadologicaDependency filho01 = this.builder(2L, model, null).build();
		filho01.setProdutos(this.buildProdutos(filho01, 100L, 101L, 102L));
		
		EstruturaMercadologicaDependency filho02 = this.builder(3L, model, null).build();
		filho02.setProdutos(this.buildProdutos(filho02, 103L, 104L, 105L, 200L));
		
		EstruturaMercadologicaDependency filho03 = this.builder(4L, model, null).build();
		filho03.setProdutos(this.buildProdutos(filho03, 106L, 107L, 108L, 109L, 110L));
		model.getSubniveis().add(filho01);
		model.getSubniveis().add(filho02);
		model.getSubniveis().add(filho03);
		EstruturaMercadologicaDependencyResponse response = this.estruturaMercadologicaConverter.convertToResponse(model);
		return response;
	}
	

	private EstruturaMercadologicaDependencyResponse buildResponseWithSubniveisAndProduto(EstruturaMercadologicaDependency model)
			throws MapperRegistryException {
		model.setProdutos(this.buildProdutos(model, 106L, 107L, 108L, 109L, 110L, 1002L, 123L, 800L));
		EstruturaMercadologicaDependency filho01 = this.builder(2L, model, null).build();
		filho01.setProdutos(this.buildProdutos(filho01, 100L, 101L, 102L));
		
		EstruturaMercadologicaDependency filho02 = this.builder(3L, model, null).build();
		filho02.setProdutos(this.buildProdutos(filho02, 103L, 104L, 105L, 200L));
		
		EstruturaMercadologicaDependency filho03 = this.builder(4L, model, null).build();
		model.getSubniveis().add(filho01);
		model.getSubniveis().add(filho02);
		model.getSubniveis().add(filho03);
		EstruturaMercadologicaDependencyResponse response = this.estruturaMercadologicaConverter.convertToResponse(model);
		return response;
	}

	
	private EstruturaMercadologicaDependencyResponse buildResponseWithSubiniveisAndProdutoAndParent(EstruturaMercadologicaDependency model,
			EstruturaMercadologicaDependency filho01) throws MapperRegistryException {
		filho01.setProdutos(this.buildProdutos(filho01, 100L, 101L, 102L));
		
		EstruturaMercadologicaDependency neto01 = this.builder(3L, filho01, new ArrayList<EstruturaMercadologicaDependency>()).build();
		neto01.setProdutos(this.buildProdutos(neto01, 103L, 104L, 105L, 200L));
		
		EstruturaMercadologicaDependency neto02 = this.builder(4L, filho01, new ArrayList<EstruturaMercadologicaDependency>()).build();
		
		model.getSubniveis().add(filho01);
		filho01.getSubniveis().add(neto01);
		filho01.getSubniveis().add(neto02);
		
		EstruturaMercadologicaDependencyResponse response = this.estruturaMercadologicaConverter.convertToResponse(filho01);
		return response;
	}

	@Test
	public void testConvertModelToResponseWithSubniveis() throws MapperRegistryException {
		EstruturaMercadologicaDependency model = this.builder(1L, null, new ArrayList<EstruturaMercadologicaDependency>()).build();
		EstruturaMercadologicaDependencyResponse response = buildResponseWithSubniveis(model);
		AssertConverterUtils.assertEstruturaMercadologica(model, response);
	}
	
	@Test
	public void testConvertModelToResponseWithProdutos() throws MapperRegistryException {
		EstruturaMercadologicaDependency model = this.builder(1L, null, new ArrayList<EstruturaMercadologicaDependency>()).build();
		EstruturaMercadologicaDependencyResponse response = buildResponseWithSubniveisAndProduto(model);
		AssertConverterUtils.assertEstruturaMercadologica(model, response);
	}

	@Test
	public void testConvertModelToResponseWithSubniveisProdutoEParent() throws MapperRegistryException {
		EstruturaMercadologicaDependency model = this.builder(1L, null, new ArrayList<EstruturaMercadologicaDependency>()).build();
		EstruturaMercadologicaDependency filho01 = this.builder(2L, model, new ArrayList<EstruturaMercadologicaDependency>()).build();
		EstruturaMercadologicaDependencyResponse response = buildResponseWithSubiniveisAndProdutoAndParent(model, filho01);
		AssertConverterUtils.assertEstruturaMercadologica(filho01, response);
	}
	
	@Test
	public void testConvertModelToModelWithSubniveis() throws MapperRegistryException {
		EstruturaMercadologicaDependencyResponse response = buildResponseWithSubniveis(this.builder(1L, null, new ArrayList<EstruturaMercadologicaDependency>()).build());
		AssertConverterUtils.assertEstruturaMercadologica(this.estruturaMercadologicaConverter.convertToModel(response), response);
	}
	
	@Test
	public void testConvertModelToModelWithProdutos() throws MapperRegistryException {
		EstruturaMercadologicaDependencyResponse response = buildResponseWithSubniveisAndProduto(this.builder(1L, null, new ArrayList<EstruturaMercadologicaDependency>()).build());
		AssertConverterUtils.assertEstruturaMercadologica(this.estruturaMercadologicaConverter.convertToModel(response), response);
	}

	@Test
	public void testConvertModelToModelWithSubniveisProdutoEParent() throws MapperRegistryException {
		EstruturaMercadologicaDependency model = this.builder(1L, null, new ArrayList<EstruturaMercadologicaDependency>()).build();
		EstruturaMercadologicaDependency filho01 = this.builder(2L, model, new ArrayList<EstruturaMercadologicaDependency>()).build();
		EstruturaMercadologicaDependencyResponse response = buildResponseWithSubiniveisAndProdutoAndParent(model, filho01);
		AssertConverterUtils.assertEstruturaMercadologica(this.estruturaMercadologicaConverter.convertToModel(response), response);
	}
	
	
	
}
