package br.com.assai.horus.converter.estruturamercadologica;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.assai.horus.application.TelevendasStartApplication;
import br.com.assai.horus.configuration.DataBaseConfiguration;
import br.com.assai.horus.converter.response.EstruturaMercadologicaConverter;
import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency;
import br.com.assai.horus.exception.MapperRegistryException;
import br.com.assai.horus.repository.dependencies.EstruturaMercadologicaRepository;
import br.com.assai.horus.response.dependency.EstruturaMercadologicaDependencyResponse;
import br.com.assai.horus.utils.AssertConverterUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelevendasStartApplication.class)
@Import(DataBaseConfiguration.class)
public class EstruturaMercadologicaConverterIntegrationTest {

	@Autowired
	private EstruturaMercadologicaRepository repository;
	
	@Autowired
	private EstruturaMercadologicaConverter converter;
	
	@Test
	@Ignore
	public void testConvertModelToModelWithSubniveis() throws MapperRegistryException {
		List<EstruturaMercadologicaDependency> parentList = this.repository.findByNivelPai(null);
		
		if(CollectionUtils.isNotEmpty(parentList)) {
			for (EstruturaMercadologicaDependency index : parentList) {
				assertValuesAndReturnResponse(index);
			}
		}
	}

	private void assertValuesAndReturnResponse(EstruturaMercadologicaDependency index) throws MapperRegistryException {
		EstruturaMercadologicaDependencyResponse response = this.converter.convertToResponse(index);
		AssertConverterUtils.assertEstruturaMercadologica(index, response);
		AssertConverterUtils.assertEstruturaMercadologica(this.converter.convertToModel(response), response);
	}
	
}
