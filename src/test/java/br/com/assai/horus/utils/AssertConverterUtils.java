package br.com.assai.horus.utils;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;

import br.com.assai.horus.entity.dependencies.EstruturaMercadologicaDependency;
import br.com.assai.horus.entity.dependencies.ProdutoDependency;
import br.com.assai.horus.response.dependency.EstruturaMercadologicaDependencyResponse;
import br.com.assai.horus.response.dependency.ProdutoDependencyResponse;

public class AssertConverterUtils {

	public static void assertProduto(ProdutoDependency model, ProdutoDependencyResponse response) {
		Assert.assertEquals(model.getId(), response.getId());
		Assert.assertEquals(model.getCodigo(), response.getCodigo());
		Assert.assertEquals(model.getDescricaoCompleta(), response.getDescricaoCompleta());
		Assert.assertEquals(model.getDescricaoReduzida(), response.getDescricaoReduzida());
		Assert.assertEquals(model.getDescricaoBasica(), response.getDescricaoBasica());
		
		if(model.getEstruturaMercadologica() == null) {
			Assert.assertNull(response.getIdEstruturaMercadologica());
		} else {
			Assert.assertEquals(model.getEstruturaMercadologica().getId(), response.getIdEstruturaMercadologica());
		}
	}
	
	public static void assertEstruturaMercadologica(EstruturaMercadologicaDependency model, EstruturaMercadologicaDependencyResponse response) {
		
		Assert.assertEquals(model.getId(), response.getId());
		Assert.assertEquals(model.getCodigo(), response.getCodigo());
		Assert.assertEquals(model.getNome(), response.getNome());
		Assert.assertEquals(model.getDescricao(), response.getDescricao());
		
		if(response.getIdNivelPai() == null) {
			Assert.assertNull(model.getNivelPai());
		}
		if(model.getNivelPai() == null) {
			Assert.assertNull(response.getIdNivelPai());
		} else {
			Assert.assertEquals(model.getNivelPai().getId(), response.getIdNivelPai());
		}
		Assert.assertEquals(model.getOrdem(), response.getOrdem());
	}
	

	public static void assertDates(Date date01, Date date02) {
		if(date01 != null && date02 != null) {
			Assert.assertTrue(DateUtils.truncatedCompareTo(date01, date02, Calendar.YEAR) == 0);
			Assert.assertTrue(DateUtils.truncatedCompareTo(date01, date02, Calendar.MONTH) == 0);
			Assert.assertTrue(DateUtils.truncatedCompareTo(date01, date02, Calendar.DAY_OF_MONTH) == 0);
			Assert.assertTrue(DateUtils.truncatedCompareTo(date01, date02, Calendar.HOUR) == 0);
			Assert.assertTrue(DateUtils.truncatedCompareTo(date01, date02, Calendar.MINUTE) == 0);
			Assert.assertTrue(DateUtils.truncatedCompareTo(date01, date02, Calendar.SECOND) == 0);
		}
	}
	
}
